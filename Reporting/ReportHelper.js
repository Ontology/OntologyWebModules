﻿var dataSource;



function init() {


    output = document.getElementById("output");
    testWebSocket();
    initControls();
}

function initControls() {
    $('#refreshReport').jqxButton({ width: 30, height: 30 });
    $('#refreshReport').on('click', function () {
        SendStringCommandWithoutParameter("RefreshReport");
    });

    $('#syncData').jqxButton({ width: 30, height: 30 });
    $('#syncData').on('click', function () {
        SendStringCommandWithoutParameter("SyncData");
    });

    $('#applyItem').jqxButton({ width: 30, height: 30 });
    $('#applyItem').on('click', function () {
        SendStringCommandWithoutParameter("ApplyItem");
    });

    $('#openUrl').jqxToggleButton({ width: 30, height: 30 });
    $('#openUrl').on('click', function () {
        SendPropertyChangeWithValue('openUrl', 'Checked', 'ToggleButton', $("#isListen").jqxToggleButton('toggled'));
    });

    $('#isListen').jqxToggleButton({ width: 30, height: 30 });
    $('#isListen').on('click', function () {
        SendPropertyChangeWithValue('isListen', 'Other', 'Other', $("#isListen").jqxToggleButton('toggled'));
    });

    $("#jqxLoader").jqxLoader({ isModal: true, width: 100, height: 60, imagePosition: 'top' });
}

function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);
 
        if (viewItem.ViewItemId == 'jqxLoader') {
            if (viewItem.LastValue == true) {
                $('#' + viewItem.ViewItemId).jqxLoader('open');
            }
            else {
                $('#' + viewItem.ViewItemId).jqxLoader('close');
            }
        }
        else if (viewItem.ViewItemType == 'GridConfig' && viewItem.ViewItemClass == 'Grid') {
            console.info(viewItem.LastValue);
            //createGrid(viewItem.LastValue);
            kendo.culture("de-DE");
            if (dataSource != undefined) {
                $("#grid").data("kendoGrid").destroy(); // destroy the Grid

                $("#grid").empty(); // empty the Grid content (inner HTML)
            }
            dataSource = new kendo.data.DataSource(viewItem.LastValue.dataSource);
            viewItem.LastValue.dataSource = dataSource;
            $.extend(viewItem.LastValue, {
                change: function () {

                    var grid = $('#grid').data("kendoGrid");
                    var selectedItem = grid.dataItem(this.select().closest("tr"));
                    
                    var cell = this.select();
                    var cellIndex = cell[0].cellIndex;
                    var column = this.columns[cellIndex];
                    var dataItem = this.dataItem(cell.closest("tr"));

                    var sendData = JSON.stringify({ "Id": selectedItem.IdRow, "ColumnName": column.field, "RowId": this.select().closest("tr")[0].rowIndex });
                    SendPropertyChangeWithValue(viewItem.ViewItemId, "SelectedIndex", "Grid", sendData);


                }, toolbar: ["excel"],
                excel: {
                    proxyURL: "/save"
                }
            })
            $("#grid").kendoGrid(viewItem.LastValue);
            setTimeout(function () {
                dataSource.read();
            }, 500);
                
            $('#grid').on('cellselect', function (event) {
                // event arguments.
                var args = event.args;
                // column data field.
                var datafield = event.args.datafield;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
                // new cell value.
                var value = args.newvalue;
                // old cell value.
                var oldvalue = args.oldvalue;

                var data = $('#' + viewItem.ViewItemId).jqxGrid('getrowdata', rowBoundIndex);

                var sendData = JSON.stringify({ "Id": data.IdRow, "ColumnName": datafield, "RowId": rowBoundIndex });
                SendPropertyChangeWithValue(viewItem.ViewItemId, "SelectedIndex", "Grid", sendData);
            });

            $('#' + viewItem.ViewItemId).on('cellvaluechanged', function (event) {
                // event arguments.
                var args = event.args;
                // column data field.
                var datafield = event.args.datafield;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
                // new cell value.
                var value = args.newvalue;
                // old cell value.
                var oldvalue = args.oldvalue;

                var data = $('#' + viewItem.ViewItemId).jqxGrid('getrowdata', rowBoundIndex);

                if (datafield == 'Apply') {
                    if (value == true) {
                        SendStringCommand("PreAppliedRow", "IdRow", data.IdRow);
                    }
                    else {
                        SendStringCommand("UnPreAppliedRow", "IdRow", data.IdRow);
                    }
                }
                ontoLog(args);
            });
            

        }
        else if (viewItem.ViewItemId == 'isListen') {
            if (viewItem.LastValue) {
                $('#isListen').jqxToggleButton('check');
            }
            else {
                $('#isListen').jqxToggleButton('unCheck');
            }
        }
        else if (viewItem.ViewItemType == 'ColumnConfig' && viewItem.ViewItemClass == 'Grid') {
            columnConfig = JSON.parse(JSON.stringify(viewItem.LastValue.ColumnList))
        }
        else if (viewItem.ViewItemId == 'reloadGrid') {
            dataSource.read();

        }
        else if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemId == 'NextRows') {
            dataSource.read();
        }
        else if (viewItem.ViewItemId == 'DestroyGrid') {
            //dataSource = null;
            //dataAdapter = null;
            //columnConfig = null;
            
            //$('#grid').jqxGrid('destroy');
            //$('#gridContainer').html('<div id="grid" style="display:none;width:100%;height:100%"></div>');
        }
        else if (viewItem.ViewItemType == 'Checked') {
            setViewItemChecked(viewItem);
        }
        else if (viewItem.ViewItemClass == 'Url' && viewItem.ViewItemType == 'Content') {
            window.open(viewItem.LastValue, "_blank");
        }
        else if (viewItem.ViewItemId == 'viewText') {
            document.title = viewItem.LastValue;
        }
    }

    //websocket.close();
}


function onOpen(evt) {
    ontoLog("CONNECTED");

   
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}