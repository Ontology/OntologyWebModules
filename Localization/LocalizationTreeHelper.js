﻿


function init() {

    initControls();
    testWebSocket();

}

var nodes;
var edges;
var network;

function initControls() {


}

function onMessage(evt) {

    var objJSON = eval("(function(){return " + evt.data + ";} )()");
    ontoLog(objJSON);

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemType == 'JsonUrl') {
            jQuery.getJSON(viewItem.LastValue, function (data) {
                $('#' + viewItem.ViewItemId).jqxTree({ source: data, width: '100%' });

                $('#' + viewItem.ViewItemId).on('select', function (event) {
                    var args = event.args;
                    var item = $('#' + viewItem.ViewItemId).jqxTree('getItem', args.element);
                    SendPropertyChangeWithValue("selectedNodeId", "Content", "Other", item.id);
                });
            })




        }
        else if (viewItem.ViewItemId == 'IsSuccessful_Login') {
            if (!viewItem.LastValue) {

                return;
            }

        }
        else if (viewItem.ViewItemClass == 'TreeNode' && viewItem.ViewItemType == 'Change') {
            var treeNodeHtmlItem = $('#' + viewItem.LastValue.Id);
            ontoLog(treeNodeHtmlItem)
            $('#' + viewItem.ViewItemId).jqxTree('updateItem', treeNodeHtmlItem[0], { label: viewItem.LastValue.NewLabel });
        }


    }



    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams();

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}



window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}