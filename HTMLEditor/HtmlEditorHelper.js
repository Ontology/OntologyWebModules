﻿var saveButton;
var timeoutId;
var toolsCount = 0;
var nextStreamIsHtml = false;
var fillViewer = false;

function init() {

    $('body').openPageLoader();
    $('#viewerArea').hide();
    $('#editorArea').hide();
    initTinyMCE();
    testWebSocket();
    

    $("#slide-in-handle").click(function (e) {
        if (visible) {
            slide.reverse();
        } else {
            slide.play();
        }
        visible = !visible;
        e.preventDefault();
    });

    
}

function openNav() {
    document.getElementById("mySidenav").style.width = "200px";
    $('#isListen').show();
    $('#sendSelect').show();
    $('#sendApply').show();
    $('.panel-default').show();
}

/* Set the width of the side navigation to 0 */
function closeNav() {
    $('#isListen').hide();
    $('#sendSelect').hide();
    $('#sendApply').hide();
    $('.panel-default').hide();
    document.getElementById("mySidenav").style.width = "0";

}

function initTinyMCE() {
   

    $('.k-button').kendoButton();

    $("#insertType").kendoMobileButtonGroup({
        select: function (e) {
            if (e.index == 0) {
                SendStringCommandWithoutParameter('clickedNoInsert');
            }
            else if (e.index == 1) {
                SendStringCommandWithoutParameter('clickedToggleLink');
            }
            else if (e.index == 2) {
                SendStringCommandWithoutParameter('clickedToggleAnchor');
            }
        },
        index: 0
    });

    $('#isListen').click(function () {
        SendStringCommand('clicked.isListen');
    });

    $('#sendSelect').click(function () {
        SendStringCommand('clicked.sendSelect');
    });

    $('#sendApply').click(function () {
        SendStringCommand('clicked.sendApply');
    });
}

var max_pages = 100;
var page_count = 0;

function snipMe() {
    page_count++;
    if (page_count > max_pages) {
        return;
    }
    var long = $(this)[0].scrollHeight - Math.ceil($(this).innerHeight());
    var children = $(this).children().toArray();
    var removed = [];
    while (long > 0 && children.length > 0) {
        var child = children.pop();
        $(child).detach();
        removed.push(child);
        long = $(this)[0].scrollHeight - Math.ceil($(this).innerHeight());
    }
    if (removed.length > 0) {
        var a4 = $('<div class="A4"></div>');
        a4.append(removed);
        $(this).after(a4);
        snipMe.call(a4[0]);
    }
}

function onMessage(evt) {

    if (nextStreamIsHtml) {
        if (fillViewer) {
            $('#viewerArea').html(evt.data);
            //$('.A4').each(function () {
            //    snipMe.call(this);
            //});
            $('img').css('display', 'block');
            $('img').css('margin-right', 'auto');
            $('img').css('margin-left', 'auto');

        }
        else {
            tinyMCE.get('mytextarea').setContent(evt.data);
        }
        
        nextStreamIsHtml = false;
    } else {
        var objJSON = eval("(function(){return " + evt.data + ";} )()");



        for (var ix in objJSON) {
            var viewItem = objJSON[ix];
            console.log(viewItem);
            if (viewItem.ViewItemId == 'IsSuccessful_Login') {
                if (viewItem.LastValue) {
                    $('body').closePageLoader();
                       
                    
                }
                else {
                    $('body').closePageLoader();
                    
                }

            }
            else if (viewItem.ViewItemId == 'IsToggled_NextDataIsHtmlCode') {
                if (nextStreamIsHtml != viewItem.LastValue) {
                    nextStreamIsHtml = viewItem.LastValue;

                    if (nextStreamIsHtml) {
                        SendStringCommandWithoutParameter('SendContent');
                    }

                }

            }
            else if (viewItem.ViewItemId == 'DataText_Name') {
                $('#nameDoc').val(viewItem.LastValue);
            }
            else if (viewItem.ViewItemId == 'IsEnabled_HtmlEditor') {
                tinyMCE.get('mytextarea').getBody().setAttribute('contenteditable', viewItem.LastValue);
            }
            else if (viewItem.ViewItemId == 'Label_Name') {
                $('#labelName').html(viewItem.Label_Name);
                tinyMCE.get('mytextarea').getBody().setAttribute('contenteditable', viewItem.LastValue);
            }
            else if (viewItem.ViewItemId == 'Label_Name') {
                $('#nameDoc').change(function () {
                    var name = $('#nameDoc').val();
                    SendStringPropertyChangeWithValue('DataText_Name', name);
                });
            }
            else if (viewItem.ViewItemId == 'viewText') {
                document.title = viewItem.LastValue;
            }
            else if (viewItem.ViewItemId == 'isListen') {
                if (viewItem.LastValue) {
                    $('#isListen').addClass('k-state-active');
                }
                else {
                    $('#isListen').removeClass('k-state-active');
                }
            }
            else if (viewItem.ViewItemId == 'textInsert') {
                tinymce.activeEditor.selection.setContent(viewItem.LastValue, { format: 'raw' });
            }
            else if (viewItem.ViewItemId == 'tglNoInsert') {
                var buttongroup = $("#insertType").data("kendoMobileButtonGroup");

                // selects by index
                buttongroup.select(0);
            }
            else if (viewItem.ViewItemId == 'tglInsertLink' && viewItem.LastValue == true) {
                var buttongroup = $("#insertType").data("kendoMobileButtonGroup");

                // selects by index
                buttongroup.select(1);
            }
            else if (viewItem.ViewItemId == 'tglInsertAnchor' && viewItem.LastValue == true) {
                var buttongroup = $("#insertType").data("kendoMobileButtonGroup");

                // selects by index
                buttongroup.select(2);
            }
            else if (viewItem.ViewItemId == 'idAnchor') {
                var content = tinymce.activeEditor.selection.getContent();
                content = '<a name="' + viewItem.LastValue + '"></a><span name="' + viewItem.LastValue + '">' + content + '</span>';
                tinymce.activeEditor.selection.setContent(content, { format: 'raw' });
            }
            else if (viewItem.ViewItemId == 'content.saved') {
                if (timeoutId != undefined)
                    clearTimeout(timeoutId);
                var ed = tinyMCE.get('mytextarea');
                ed.isNotDirty = true;
                saveButton.disabled(true);
                
            }
            else if (viewItem.ViewItemId == 'showViewerArea') {
                if (viewItem.LastValue) {
                    $('#editorArea').hide();
                    $('#viewerArea').show();
                    fillViewer = true;
                } else {
                    $('#editorArea').show();
                    $('#viewerArea').hide();
                    fillViewer = false;
                }
            }
        }

    }



    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");
    tinymce.init({
        selector: "#mytextarea",
        plugins: "spellchecker lists print preview hr pagebreak searchreplace code fullscreen insertdatetime table textcolor colorpicker link image imagetools visualblocks media contextmenu anchor",
        toolbar: "saveContent | btnListen | undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | print preview | forecolor backcolor | link anchor | image",
        image_caption: true,
        browser_spellcheck: true,
        save_enablewhendirty: true,
        save_onsavecallback: saveContent,
        setup: function (ed) {
            ed.addButton('saveContent', {
                icon: 'save',
                text: 'save',
                onclick: function () {
                    saveContent();
                },
                onPostRender: function() {
                    saveButton = this;
                    saveButton.disabled(true);
                }
            });
            ed.on('NodeChange', function (e) {
                SendPropertyChangeWithValue('selectedText', 'Content', 'Selection', tinymce.activeEditor.selection.getContent());

            });
            ed.on('change', function (ed, l) {
                var viewItem = {
                    ViewItemId: 'exitMessage',
                    LastValue: ''
                }
                if (tinymce.activeEditor.isDirty()) {
                    if (timeoutId != undefined)
                        clearTimeout(timeoutId);
                    timeoutId = setTimeout(function () {
                        saveContent();
                    }, 5000);
                    viewItem.LastValue = 'dirty';
                    saveButton.disabled(false);
                }
                else {
                    saveButton.disabled(true);
                    if (timeoutId != undefined)
                        clearTimeout(timeoutId);
                }

                SetCloseMessage(viewItem);
            });
            ed.on('click', function (e) {
                if (e.target.nodeName == 'A') {
                    SendPropertyChangeWithValue('selectedItem', 'Other', 'Other', e.target.name);
                }
            });
        }
    });

    //myTree.load("tree.xml");
}

function saveContent() {
    
    var content = tinyMCE.get('mytextarea').getContent();
    SendStringCommandWithoutParameter('ReceiveContent');
    websocket.send(content);
    
}

function onClose(evt) {
    $('body').closePageLoader();
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}