﻿function init() {
    testWebSocket();
    initToolbar();
}

function initToolbar() {
    $('#toolbar').kendoToolBar({
        items: [
                { type: "button", id: "listen", spriteCssClass: "fa fa-assistive-listening-systems", togglable: true },
        ],
        toggle: function (e) {
            console.log("toggle", e.target.text(), e.checked);
            var id = e.target.attr('id');

            if (id == 'listen') {
                SendStringCommandWithoutParameter('clicked.listen');
            }
            
        }
    });

    
}

function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);

        
        if (viewItem.ViewItemId == 'viewText') {
            document.title = viewItem.LastValue;
        }
        else if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemClass == 'Input' && viewItem.ViewItemType == 'Content') {
            setViewItemContent(viewItem);
        }
        else if (viewItem.ViewItemClass == 'NumericInput' && viewItem.ViewItemType == 'Content') {
            var numerictextbox = $("#" + viewItem.ViewItemId).data("kendoNumericTextBox");

            numerictextbox.value(viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == "tree" && viewItem.ViewItemType == 'Config') {
            $('#tree').createKendoTreeList(viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'isListen') {
            var toolbar = $("#toolbar").data("kendoToolBar");
            toolbar.toggle("#listen", viewItem.LastValue);
        }
        
    }

    //websocket.close();
}

function setButtonIcon(viewItem) {
    if (viewItem.LastValue == true) {
        $('#' + viewItem.ViewItemId).html('<i class="fa fa-assistive-listening-systems" aria-hidden="true"></i>')
        $('#' + viewItem.ViewItemId).removeClass('k-default');
        $('#' + viewItem.ViewItemId).addClass('k-primary');
    }
    else {
        $('#' + viewItem.ViewItemId).html('<i class="fa fa-circle-thin" aria-hidden="true"></i>')
        $('#' + viewItem.ViewItemId).removeClass('k-primary');
        $('#' + viewItem.ViewItemId).addClass('k-default');
        
    }
    
}

function onOpen(evt) {
    ontoLog("CONNECTED");

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}