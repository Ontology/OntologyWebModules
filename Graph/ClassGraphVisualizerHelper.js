﻿


function init() {

    initControls();
    testWebSocket();

}

var nodes;
var edges;
var network;

function initControls() {
    $("#graphToolbar").jqxToolBar({
        width: '100%', height: 35, tools: 'toggleButton toggleButton',
        initTools: function (type, index, tool, menuToolIninitialization) {
            if (type == "toggleButton") {
                var icon = $("<div class='jqx-editor-toolbar-icon jqx-editor-toolbar-icon-" + theme + " buttonIcon'></div>");
            }
            switch (index) {
                case 0:
                    tool.attr("id", "classButton")
                    tool.jqxToggleButton({ width: 80 });
                    tool.text("Classes");
                    tool.on("click", function () {
                        var toggled = tool.jqxToggleButton("toggled");
                        SendPropertyChangeWithValue("classButton", "Checked", "ToggleButton", toggled)
                    });
                    break;
                case 1:
                    tool.attr("id", "objectButton")
                    tool.jqxToggleButton({ width: 80 });
                    tool.text("Objects");
                    tool.on("click", function () {
                        var toggled = tool.jqxToggleButton("toggled");
                        SendPropertyChangeWithValue("objectButton", "Checked", "ToggleButton", toggled)
                    });
                    break;
            }
        }
    });

}

function onMessage(evt) {

    var objJSON = eval("(function(){return " + evt.data + ";} )()");
    ontoLog(objJSON);

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemId == 'Text_View') {
            document.title = viewItem.LastValue;
        }
        else if (viewItem.ViewItemType == "Content") {
            $('#' + viewItem.ViewItemId).text(viewItem.LastValue);

        }
        else if (viewItem.ViewItemType == "Enable") {
            var tools = $("#graphToolbar").jqxToolBar("getTools");

            for (var i = 0; i < tools.length; i++) {
                var currentTool = tools[i];
                if (currentTool.tool[0].id == viewItem.ViewItemId) {
                    $("#graphToolbar").jqxToolBar("disableTool", i, !viewItem.LastValue)
                }

            }


        }
        else if (viewItem.ViewItemType == "Checked") {
            var tools = $("#graphToolbar").jqxToolBar("getTools");

            for (var i = 0; i < tools.length; i++) {
                var currentTool = tools[i];
                if (currentTool.tool[0].id == viewItem.ViewItemId) {
                    currentTool.tool.jqxToggleButton({ toggled: viewItem.LastValue });
                }

            }


        }
        else if (viewItem.ViewItemType == 'UrlList') {
            if (viewItem.LastValue != undefined) {
                var nodesUrl = viewItem.LastValue[0];
                var edgesUrl = viewItem.LastValue[1];

                jQuery.getJSON(nodesUrl, function (data) {
                    nodes = new vis.DataSet(data);
                    SendStringCommandWithoutParameter("NodesPrepared");
                });

                jQuery.getJSON(edgesUrl, function (data) {
                    edges = new vis.DataSet(data);
                    SendStringCommandWithoutParameter("EdgesPrepared");
                });


            }
        }
        else if (viewItem.ViewItemType == 'Command' &&
            viewItem.ViewItemClass == 'VisGraph') {

            if (viewItem.LastValue == 'LoadGraph') {

                var container = document.getElementById(viewItem.ViewItemId);

                var data = {
                    nodes: nodes,
                    edges: edges
                };


                var options = {
                    manipulation: true,
                    edges: {
                        arrows: 'to',
                        scaling: {
                            label: { enabled: true },
                        },
                        "smooth": false
                    },
                    layout: {
                        improvedLayout: true,

                    },
                    "physics": {
                        "barnesHut": {
                            "gravitationalConstant": -10600,
                            "centralGravity": 0.1,
                            "springLength": 230,
                            "springConstant": 0.005,
                            "avoidOverlap": 0
                        },
                        "minVelocity": 0.19,
                        "timestep": 0.5
                    }
                };
                if (network == undefined) {
                    ontoLog('Initialize')
                    network = new vis.Network(container, data, options);
                    network.on("click", function (params) {
                        params.event = "[original event]";
                        var idNode = params.nodes[0];
                        var idEdge = params.edges[0];
                        if (idNode != undefined) {
                            SendStringCommand("selectedNode", "NodeId", idNode);
                        }
                        else if (idEdge != undefined) {
                            SendStringCommand("selectedEdge", "EdgeId", idEdge);
                        }


                    });
                    network.on("stabilizationIterationsDone", function () {
                        network.setOptions({ physics: false });
                    });
                } else {
                    ontoLog('Reload')
                    network.setData({ nodes: nodes, edges: edges });
                }

            }

        }
    }



    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams();

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}



window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}