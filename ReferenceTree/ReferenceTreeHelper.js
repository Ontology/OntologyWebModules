﻿/**
     * Generates a GUID string.
     * @returns {String} The generated GUID.
     * @example af8a8416-6e18-a307-bd9c-f2c947bbb3aa
     * @author Slavik Meltser (slavik@meltser.info).
     * @link http://slavik.meltser.info/?p=142
     */
function guid() {
    function _p8(s) {
        var p = (Math.random().toString(16) + "000000000").substr(2, 8);
        return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
    }
    return _p8() + _p8(true) + _p8(true) + _p8();
}



var wsUri = "wss://192.168.2.107:6401/ClassTreeController.ClassTreeController?Module=79544abe2580486fb82b310e1d31cf93&Controller=efe0368e33b44a23af4ab61e29fe83b1&ViewType=Reference&Endpoint=" + guid() + "&Session=";
var output;
var toolsCount = 0;

function init() {


    CreateToolBar();
    output = document.getElementById("output");
    testWebSocket();

}

function CreateToolBar() {
    $("#jqxToolBar").jqxToolBar({
        width: "100%", height: 35, tools: '',
        initTools: function (type, index, tool, menuToolIninitialization) {
            if (type == "toggleButton") {
                var icon = $("<div class='jqx-editor-toolbar-icon jqx-editor-toolbar-icon-" + theme + " buttonIcon'></div>");
            }
            switch (index) {
                case 0:
                    var button = $("<div>Root</div>");
                    tool.append(button);
                    button.jqxButton({ height: 15 });

                    break;
                case 2:
                    icon.addClass("jqx-editor-toolbar-icon-underline jqx-editor-toolbar-icon-underline-" + theme);
                    icon.attr("title", "Underline");
                    tool.append(icon);
                    break;
            }
        }
    });
}

function treeSelectionChanged(id) {
    SendStringPropertyChangeWithValue("Text_SelectedNodeId", id);

}


function testWebSocket() {
    try {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        var sessionId = getUrlParameter('Session');

        websocket = new WebSocket(wsUri + sessionId);
        websocket.onopen = function (evt) { onOpen(evt) };
        websocket.onclose = function (evt) { onClose(evt) };
        websocket.onmessage = function (evt) { onMessage(evt) };
        websocket.onerror = function (evt) { onError(evt) };
    } catch (e) {

    }


}
function onMessage(evt) {

    console.info(evt.data);
    var objJSON = eval("(function(){return " + evt.data + ";} )()");



    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemId == 'Url_TreeData') {
            var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'id' },
                    { name: 'label' },
                    { name: 'html' }
                ],
                id: 'id',
                url: viewItem.LastValue
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#list").jqxListBox({ source: dataAdapter, displayMember: "label", valueMember: "id", width: "100%", height: "100%" });
            $('#list').on('select', function (event) {
                var args = event.args;
                var item = $('#list').jqxListBox('getItem', args.index);
                if (item != null) {
                    SendStringPropertyChangeWithValue("Text_SelectedNodeId", item.value);
                }
            });
            //jQuery.getJSON(objJSON.Url_TreeData, function (data) {
            //    initTree(data);
            //});



        }
        else if (viewItem.PropertyName == 'IsSuccessful_Login') {
            if (!viewItem.LastValue) {

                return;
            }

        }
        else if (objJSON.PropertyName == 'ViewTreeNodes_PathNodes') {
            console.info('pathNodes: ' + objJSON.ViewTreeNodes_PathNodes);
            var pathNodes = objJSON.ViewTreeNodes_PathNodes;
            configurePath(pathNodes);
        }


    }


    if (objJSON.MessageType == 'Command') {

    }

    if (objJSON.MessageType == 'Model') {



    }


    //websocket.close();
}

/**
* Detects the font of an element from the font-family css attribute by comparing the font widths on the element
* @link http://stackoverflow.com/questions/15664759/jquery-how-to-get-assigned-font-to-element
*/
(function ($) {
    $.fn.detectFont = function () {
        var fontfamily = $(this).css('font-family');
        var fonts = fontfamily.split(',');
        if (fonts.length == 1)
            return fonts[0];

        var element = $(this);
        var detectedFont = null;
        fonts.forEach(function (font) {
            var clone = $('<span>wwwwwwwwwwwwwwwlllllllliiiiii</span>').css({ 'font-family': fontfamily, 'font-size': '70px', 'display': 'inline', 'visibility': 'hidden' }).appendTo('body');
            var dummy = $('<span>wwwwwwwwwwwwwwwlllllllliiiiii</span>').css({ 'font-family': font, 'font-size': '70px', 'display': 'inline', 'visibility': 'hidden' }).appendTo('body');
            //console.log(clone, dummy, fonts, font, clone.width(), dummy.width());
            if (clone.width() == dummy.width())
                detectedFont = font;
            clone.remove();
            dummy.remove();
        });

        return detectedFont;
    }
})(jQuery);

function measureText(text, font) {
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.font = font;
    return ctx.measureText(text).width;
}

function configurePath(pathNodes) {
    for (var tix = toolsCount - 1; tix >= 0; tix--) {
        $('#jqxToolBar').jqxToolBar('destroyTool', tix);
    }
    var font = $('#jqxToolBar').detectFont();

    $('#jqxToolBar').jqxToolBar('addTool', 'input', 'last', true, function (type, tool, menuToolInitialization) {
        tool.jqxInput({ width: 130, placeHolder: "Enter search..." });
        tool.on("change", function () {
            SendStringPropertyChangeWithValue("Text_Search", tool.val());
        });
    });
    for (var ix in pathNodes) {
        var pathNode = pathNodes[ix];
        $('#jqxToolBar').jqxToolBar('addTool', 'button', 'last', true, function (type, tool, menuToolInitialization) {
            var width = measureText(pathNode.Name, font) * 1.8;
            if (menuToolInitialization == true) {
                width = "100%";
            }
            tool.text(pathNode.Name);
            tool.id = pathNode.Id;
            tool.jqxButton({ width: width });
            tool.on("click", function (event) {
                SendStringPropertyChangeWithValue("Text_SelectedNodeId", tool.id);
            });
        });

    }
    toolsCount = pathNodes.length + 1;
}

function initTree(source) {
    console.info(source);



    $('#list').jqxListMenu({ width: '100%', enableScrolling: false, showHeader: true, showBackButton: true, showFilter: false });

    var ul = $('#list');
    for (var ix in source) {
        var treeNode = source[ix];
        var li = document.createElement("li");
        li.id = treeNode.id;
        li.appendChild(document.createTextNode(treeNode.label));

        ul.append(li);

    }

    //$('#jqxTree').jqxTree({ source: source, width: '98%', height: '95vh' });
    //$('#jqxTree').on('select', function (event) {
    //    var args = event.args;
    //    var item = $('#jqxTree').jqxTree('getItem', args.element);
    //    treeSelectionChanged(item.id);
    //});
}

function onOpen(evt) {
    writeToScreen("CONNECTED");


    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    writeToScreen("DISCONNECTED");
}

function onError(evt) {
    writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

function SendStringPropertyChange(propertyName) {
    websocket.send("{ \"MessageType\": \"Change\", \"PropertyName\": \"" + propertyName + "\", \"" + propertyName + "\" : \"" + $('#' + propertyName).val() + "\"}");
}

function SendStringPropertyChangeWithValue(propertyName, value) {
    websocket.send("{ \"MessageType\": \"Change\", \"PropertyName\": \"" + propertyName + "\", \"" + propertyName + "\" : \"" + value + "\"}");
}

function SendStringEvent(eventName) {
    websocket.send("{ \"MessageType\": \"Event\", \"Event\": \"" + eventName + "\"}");
}


function writeToScreen(message) {
    var pre = document.createElement("p");
    pre.style.wordWrap = "break-word";
    pre.innerHTML = message;
    output.appendChild(pre);

}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}