﻿var dhxWns;
var dhxWindow;

function init() {

    ontoLog('init');
    _createWindow();
    var sessionIdParam = getUrlParameter('Session');
    if (sessionIdParam != undefined) {
        sessionId = sessionIdParam;
        createCookie("session", "", -1);
        createCookie("session", sessionIdParam, 1);

    }
    testWebSocket(sessionId);






    ontoLog('open Login');
    openLogin();
    ontoLog('login opened');
}

function createCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
    ontoLog(document.cookie)
}

//Creating the demo window
function _createWindow() {
    var loginWindow = $("#window")
    loginWindow.kendoWindow({
        width: "320px",
        title: "Login",
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize"
        ],
        close: function () {

        }
    }).data("kendoWindow");
};

function openLogin() {

    $('#window').data('kendoWindow').center().open();
    ontoLog('loginOpened')

}

function onMessage(evt) {

    var objJSON = eval("(function(){return " + evt.data + ";} )()");
    ontoLog(evt.data);
    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemType == 'Command') {

        }
        else {
            if (viewItem.ViewItemId == 'Text_Error') {

                $('#error').html(viewItem.LastValue);
                $('#error').show();

            }
            else if (viewItem.ViewItemId == 'Url_Navigation') {
                if (viewItem.LastValue == undefined) return;
                ontoLog('navigate')
                $('#' + viewItem.ViewItemId).attr('src', viewItem.LastValue);
                $('#navigationArea').show();
                $('#window').data("kendoWindow").close();
            }
            else if (viewItem.ViewItemId == 'Text_Cookie') {
                document.cookie = viewItem.LastValue;
            }
        }
    }


    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams();
    sendCookie();

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog(evt.data);
}

function login() {
    $('#error').css('display', 'none');
    ontoLog('login');
    SendStringCommandCredentials();

}

function SendStringCommandCredentials() {
    var jsonCommand = {
        "MessageType": "Command",
        "Command": "Login",
        "Text_UserName": $('#Text_UserName').val(),
        "Text_Group": $('#Text_Group').val(),
        "Text_Password": $('#Text_Password').val(),
        "IsUserSender": $('#isUserSender').is(':checked')
    }
    websocket.send(JSON.stringify(jsonCommand));
}


function errorOccured(error, url, line) {

}

sessionId = getCookie("session");



window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}