﻿function init() {
    testWebSocket();
    initToolbar();
}

function initToolbar() {
    $('#toolbar').kendoToolBar({
        items: [
                { type: "button", id: "listen", spriteCssClass: "fa fa-assistive-listening-systems", togglable: true },
                { type: "button", id: "listenApplied", spriteCssClass: "fa fa-check-square", togglable: true },
                { type: "button", id: "refresh", spriteCssClass: "fa fa-refresh" },
                { type: "separator" },
                { template: "<label>Typed Tag:</label>" },
                { template: '<input id="inpTypedTag" placeholder="Name" style="width:300px"/>' },
                { type: "separator" },
                { template: "<label>Order-Id:</label>" },
                { template: '<input id="orderId" style="width: 80px;" />' },
                { type: "separator" },
                { template: "<label>Next Order-Id:</label>" },
                { template: '<input id="nextOrderId" style="width: 80px;" />' }
        ],
        toggle: function (e) {
            console.log("toggle", e.target.text(), e.checked);
            var id = e.target.attr('id');

            if (id == 'listen') {
                SendStringCommandWithoutParameter('clicked.listen');
            }
            else if (id == 'listenApplied') {
                SendStringCommandWithoutParameter('clicked.listenApplied');
            }
            
        },
        click: function (e) {
            var id = e.target.attr('id');

            if (id == 'refresh') {
                SendStringCommandWithoutParameter('clicked.reload');
            }
        }
    });

    $("#orderId").kendoNumericTextBox({
        format: "n0",
        change: function () {
            var value = this.value();
            SendPropertyChangeWithValue("orderId", "Content", "NumericInput", value);
        }
    });

    $("#nextOrderId").kendoNumericTextBox({
        format: "n0",
        change: function () {
            var value = this.value();
            SendPropertyChangeWithValue("nextOrderId", "Content", "NumericInput", value);
        }
    });
    

    $('#inpTypedTag').on('change', function () {
        var value = $('#inpTypedTag').val();

        SendPropertyChangeWithValue("inpTypedTag", "Content", "Input", value);
    });
}

function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);

        
        if (viewItem.ViewItemId == 'viewText') {
            document.title = viewItem.LastValue;
        }
        else if (viewItem.ViewItemType == 'DataSource' && viewItem.ViewItemClass == 'Grid') {
            console.info(viewItem.LastValue);
            //createGrid(viewItem.LastValue);

            var gridConfig = {
                gridConfig: viewItem.LastValue,
                dataBoundFunc: function (e) {
                    
                    
                },
                changeFunc: function (e) {
                    var cell = this.select(),
                        cellIndex = cell.index(),
                        column = this.columns[cellIndex],
                        dataSource = this.dataSource,
                        dataItem = dataSource.view()[cell.closest("tr").index()];

                    var typedTagSelected = {
                        PropertyName: column.field,
                        Uid: dataItem.uid,
                        TypedTag: dataItem
                    }

                    SendPropertyChangeWithValue("typedTagSelected", "Other", "Other", typedTagSelected);
                    
                },
                dataSourceChnageFunc: function (e) {
                    console.log(e);
                }
            }
            $('#grid').createKendoGrid(gridConfig);

        }
        else if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemClass == 'Input' && viewItem.ViewItemType == 'Content') {
            setViewItemContent(viewItem);
        }
        else if (viewItem.ViewItemClass == 'NumericInput' && viewItem.ViewItemType == 'Content') {
            var numerictextbox = $("#" + viewItem.ViewItemId).data("kendoNumericTextBox");

            numerictextbox.value(viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'isListen') {
            var toolbar = $("#toolbar").data("kendoToolBar");
            toolbar.toggle("#listen", viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'listenApplied') {
            var toolbar = $("#toolbar").data("kendoToolBar");
            toolbar.toggle("#listenApplied", viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'changedTypedTag') {
            var dataItem = $("#grid").data("kendoGrid").dataItem($("#grid").data("kendoGrid").table.find('tr[data-uid="' + viewItem.LastValue.Uid + '"]'))
            if (viewItem.LastValue.PropertyName == 'NameTypedTag') {
                dataItem.NameTypedTag = viewItem.LastValue.TypedTag.NameTypedTag;
            }
            else if (viewItem.LastValue.PropertyName == 'OrderId') {
                dataItem.OrderId = viewItem.LastValue.TypedTag.OrderId;
            }
            
            $("#grid").data("kendoGrid").refresh();
        }
        else if (viewItem.ViewItemId == 'addedTypedTag') {
            for (var ix in viewItem.LastValue) {
                $("#grid").data("kendoGrid").dataSource.add(viewItem.LastValue[ix]);
                
            }
            $("#grid").data("kendoGrid").refresh();
        }
    }

    //websocket.close();
}

function setButtonIcon(viewItem) {
    if (viewItem.LastValue == true) {
        $('#' + viewItem.ViewItemId).html('<i class="fa fa-assistive-listening-systems" aria-hidden="true"></i>')
        $('#' + viewItem.ViewItemId).removeClass('k-default');
        $('#' + viewItem.ViewItemId).addClass('k-primary');
    }
    else {
        $('#' + viewItem.ViewItemId).html('<i class="fa fa-circle-thin" aria-hidden="true"></i>')
        $('#' + viewItem.ViewItemId).removeClass('k-primary');
        $('#' + viewItem.ViewItemId).addClass('k-default');
        
    }
    
}

function onOpen(evt) {
    ontoLog("CONNECTED");

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}