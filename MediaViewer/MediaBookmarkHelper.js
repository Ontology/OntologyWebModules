﻿

var images;
var toolsCount = 0;
var mimeType;

function init() {
    createOntologyToolbar();
    $("#jqxLoader").jqxLoader({ width: 100, height: 60, imagePosition: 'top' });
    testWebSocket();

}

function createOntologyToolbar() {
    $("#listen").jqxToggleButton({ width: '32px', height: '32px', toggled: false, value: '<i class="fa fa-globe" aria-hidden="true"></i>' });

    $('#listen').on('click', function () {
        SendStringPropertyChangeWithValue('IsToggled_Listen', $("#listen").jqxToggleButton('toggled'));
    });

    $("#menu").mouseenter(function () {
        $("#oToolBar").show(400);
    });
    $("#menu").click(function () {
        $("#oToolBar").hide(400);
    });

    $("#refresh").jqxButton({ width: '32px', height: '32px', value: '<i class="fa fa-refresh" aria-hidden="true"></i>' });
    $('#refresh').on('click', function () {
        SendStringCommandWithoutParameter('Refresh');
    });
}

function onMessage(evt) {

    ontoLog('<span style="color: blue;">Data: ' + evt.data + '</span>');
    var objJSON = eval("(function(){return " + evt.data + ";} )()");



    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.info(viewItem.LastValue);
        if (viewItem.ViewItemId == 'Url_DateBookmarkItems') {
            jQuery.getJSON(viewItem.LastValue, function (data) {
                initLayout(data);
            });
        }
        else if (viewItem.ViewItemId == 'IsLoading_Bookmarks') {
            if (viewItem.LastValue) {
                $('#jqxLoader').jqxLoader('open');
            }
            else {
                $('#jqxLoader').jqxLoader('close');
            }
        }
        else if (viewItem.ViewItemId == 'IsToggled_Listen') {
            $("#listen").jqxToggleButton({ toggled: viewItem.LastValue });
        }

    }




    //websocket.close();
}


function initLayout(data) {
    console.log(data);

    var source = {
        datatype: "json",
        datafields: [
            { name: 'IdItem' },
            { name: 'IdParent' },
            { name: 'LabelItem' }
        ],
        id: 'IdItem',
        localdata: data
    };
    var dataAdapter = new $.jqx.dataAdapter(source);
    dataAdapter.dataBind();
    var records = dataAdapter.getRecordsHierarchy('IdItem', 'IdParent', 'items', [{ name: 'LabelItem', map: 'label' }, { name: 'IdItem', map: 'id' }]);
    $('#bookMarkTree').jqxTree({ source: records, width: '100%' });

    $('#bookMarkTree').on('select', function (event) {
        var args = event.args;
        var item = $('#bookMarkTree').jqxTree('getItem', args.element);
        var itemId = item.id;
        console.log(item);
        console.log(event.args);
        SendStringPropertyChangeWithValue('Text_SelectedId', itemId);
    });
}

function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams();

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}