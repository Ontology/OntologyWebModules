﻿
var images;
var toolsCount = 0;
var mimeType;
var autoOpen;
var currentUrl;


function init() {
    createOntologyToolbar();
    CreateToolBar();

    testWebSocket();

}

function createOntologyToolbar() {
    $("#listen").jqxToggleButton({ width: '32px', height: '32px', toggled: false, value: '<i class="fa fa-globe" aria-hidden="true"></i>' });
    $('#listen').on('click', function () {
        SendStringPropertyChangeWithValue('IsToggled_Listen', $("#listen").jqxToggleButton('toggled'));
    });

    $("#menu").mouseenter(function () {
        $("#oToolBar").show(400);
    });
    $("#menu").click(function () {
        $("#oToolBar").hide(400);
    });
}

function onMessage(evt) {

    console.log(evt.data);
    var objJSON = eval("(function(){return " + evt.data + ";} )()");



    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemId == 'Url_Open') {
            currentUrl = viewItem.LastValue;
            if ($("#jqxAutoOpenButton").jqxToggleButton('toggled')) {
                if (currentUrl != undefined && currentUrl != "") {
                    openUrl();
                }

            }






        }
        else if (viewItem.ViewItemId == 'IsSuccessful_Login') {
            if (!viewItem.LastValue) {

                return;
            }

        }
        else if (viewItem.ViewItemId == 'IsEnabled_NavFirst') {
            $('#jqxNavFirstButton').jqxButton({ disabled: !viewItem.LastValue });
        }
        else if (viewItem.ViewItemId == 'IsEnabled_NavPrevious') {
            $('#jqxNavPreviousButton').jqxButton({ disabled: !viewItem.LastValue });

        }
        else if (viewItem.ViewItemId == 'IsEnabled_NavNext') {
            $('#jqxNavNextButton').jqxButton({ disabled: !viewItem.LastValue });

        }
        else if (viewItem.ViewItemId == 'IsEnabled_NavLast') {
            $('#jqxNavLastButton').jqxButton({ disabled: !viewItem.LastValue });
        }
        else if (viewItem.ViewItemId == 'Label_Nav') {
            $('#labelNav').text(viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'Label_Url') {
            $('#labelUrl').text(viewItem.LastValue)
        }
        else if (viewItem.ViewItemId == 'IsToggled_Listen') {
            $("#listen").jqxToggleButton({ toggled: viewItem.LastValue });
        }
        else if (viewItem.ViewItemId == 'IsEnabled_Open') {
            $('#jqxOpen').jqxButton({ disabled: !viewItem.LastValue });
        }
        else if (viewItem.ViewItemId == 'IsChecked_AutoOpen') {
            $("#jqxAutoOpenButton").jqxToggleButton({ toggled: viewItem.LastValue });
        }

    }





    //websocket.close();
}

function openUrl() {
    window.open(currentUrl, "_blank");
    this.focus();

}


function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams();

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}