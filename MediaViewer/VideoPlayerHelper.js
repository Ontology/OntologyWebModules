﻿

var images;
var toolsCount = 0;
var mimeType;
var vidP;
var videos;

function init() {
    
    $('#toolbar').kendoToolBar({
        items: [
                { type: "button", id: "listen", spriteCssClass: "fa fa-assistive-listening-systems", togglable: true },
                { type: "separator" },
                { template: "<label>Autoplay:</label>" },
                { template: '<input type="checkbox" id="isCheckedAutoPlay" data-role="switch" data-change="onChangeAutoPlay"/>' },
                { template: "<label>Bookmark:</label>" },
                { template: '<input type="checkbox" id="toggleBookmarks" data-role="switch" data-change="onChangeBookmarks"/>' },
                { type: "separator" },
                { template: '<label>Last Bookmark:</label>' },
                { template: '<label id="DataText_LastSavedBookmark"></label>' },
        ],
        toggle: function (e) {
            console.log("toggle", e.target.text(), e.checked);
            var id = e.target.attr('id');

            if (id == 'listen') {
                SendStringCommandWithoutParameter('clicked.listen');
            }
            

        }
    });

    $("#isCheckedAutoPlay").kendoMobileSwitch({
        onLabel: "On",
        offLabel: "Off",
        change: function (e) {
            SendPropertyChangeWithValue("isCheckedAutoPlay", "Checked", "CheckBox", e.checked);
        }
    });

    $("#toggleBookmarks").kendoMobileSwitch({
        onLabel: "On",
        offLabel: "Off",
        change: function (e) {
            SendPropertyChangeWithValue("toggleBookmarks", "Checked", "ToggleButton", e.checked);
        }
    });

    $("#mediaplayer").kendoMediaPlayer({
        autoPlay: false,
        navigatable: true,
        ready: function () {
            var videoPlayer = $('.k-mediaplayer-media');
            $(videoPlayer).on('ended', function () {
                SendStringCommand('stopped', "Pos", this.currentTime);
            });

            $(videoPlayer).on('play', function () {
                SendStringCommand('started', "Pos", this.currentTime);
            });

            $(videoPlayer).on('pause', function () {
                SendStringCommand('paused', "Pos", this.currentTime);
            });
            $(videoPlayer).on('timeupdate', function () {
                SendStringCommand('curpos', "Pos", this.currentTime);
                $("#mediaplayer").trigger('timeChanged');
            });

            if ($("#mediaplayer").data("kendoMediaPlayer").autoPlay) {
                $("#mediaplayer").data("kendoMediaPlayer").play();
            }
        },
        
        
    });


    
    videos = new kendo.data.DataSource({ data: [] });

    var listView = $("#listView").kendoListView({
        dataSource: videos,
        selectable: true,
        scrollable: true,
        template: kendo.template($("#template").html()),
        change: onChange,
        dataBound: onDataBound
    });

    function onChange() {
        var index = this.select().index();
        var dataItem = this.dataSource.view()[index];

        SendStringCommand('started.mediaItem', "idMediaItem", dataItem.idMediaItem);;
        var mediaItem = {
            title: dataItem.title,
            poster: dataItem.poster,
            source: dataItem.source
        };
        $("#mediaplayer").data("kendoMediaPlayer").media(mediaItem);
    }

    function onDataBound() {
        this.select(this.element.children().first());
    }

    testWebSocket();

}

function onMessage(evt) {

    ontoLog('<span style="color: blue;">Data: ' + evt.data + '</span>');
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemId == 'Url_Video') {
            console.info(viewItem.LastValue);
            




        }
        else if (viewItem.ViewItemId == 'itemToAdd') {
            videos.add(viewItem.LastValue);
            $("#listView").data('kendoListView').refresh();
        }
        else if (viewItem.ViewItemId == 'IsSuccessful_Login') {
            if (!viewItem.LastValue) {
                
                return;
            }

        }
        else if (viewItem.ViewItemId == 'isCheckedAutoPlay') {
            console.info('checked: ' + viewItem.LastValue);
            $("#mediaplayer").data("kendoMediaPlayer").autoPlay = viewItem.LastValue;
        }
        else if (viewItem.ViewItemId == 'DataText_LastSavedBookmark') {
            console.info(viewItem.LastValue);
            $('#DataText_LastSavedBookmark').text(viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'Error_BookMark') {
            var error = viewItem.LastValue;
            if (error == undefined) {
                $('#errorMessage').hide();
            }
            else {
                $('#errorMessage').text(viewItem.LastValue);
                $('#errorMessage').show();
            }

        }
        else if (viewItem.ViewItemId == 'Reset') {
            videos.data([]);
        }
        else if (viewItem.ViewItemClass == 'ToggleButton' && viewItem.ViewItemType == 'Checked') {
            var toolbar = $("#toolbar").data("kendoToolBar");
            toolbar.toggle("#" + viewItem.ViewItemId, viewItem.LastValue); //select button with id: "foo"
        }

    }

    //websocket.close();
}




function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams();

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}


window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}