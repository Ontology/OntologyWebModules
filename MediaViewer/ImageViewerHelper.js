﻿

var images;
var output;
var toolsCount = 0;
var gallery;
var pswpElement;
var pswp;
var items;
var isEnabledNavNext;
var ixImage = 0;
var items;

function init() {

    //CreateToolBar();
    testWebSocket();

}

function loadCanvas(dataURL) {
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
    if (dataURL != undefined && dataURL != '') {
        // load image from data url
        var imageObj = new Image();
        imageObj.onload = function () {

            context.drawImage(this, 0, 0);
        };

        imageObj.src = dataURL;
    }

}


function CreateToolBar() {
    $("#jqxNavFirstButton").jqxButton({ width: 32, height: 32 });
    $("#jqxNavPreviousButton").jqxButton({ width: 32, height: 32 });
    $("#jqxNavNextButton").jqxButton({ width: 32, height: 32 });
    $("#jqxNavLastButton").jqxButton({ width: 32, height: 32 });

    $('#jqxNavFirstButton').jqxButton({ disabled: true });
    $('#jqxNavPreviousButton').jqxButton({ disabled: true });
    $('#jqxNavNextButton').jqxButton({ disabled: true });
    $('#jqxNavLastButton').jqxButton({ disabled: true });

    $("#jqxNavFirstButton").on('click', function () {
        SendStringCommandWithoutParameter('NavFirst');

    });
    $("#jqxNavPreviousButton").on('click', function () {
        SendStringCommandWithoutParameter('NavPrevious');
    });
    $("#jqxNavNextButton").on('click', function () {
        $('#slideShow').supersized({
            // Functionality
            slideshow: 1,
            slide_interval: 3000,		// Length between transitions
            transition: 3, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
            transition_speed: 700,		// Speed of transition

            // Components							
            slide_links: 'blank',	// Individual links for each slide (Options: false, 'num', 'name', 'blank')
            slides: items
        });
        SendStringCommandWithoutParameter('NavNext');

    });
    $("#jqxNavLastButton").on('click', function () {
        SendStringCommandWithoutParameter('NavLast');
    });


}

function clickedImage(item) {
    console.log(item);
    var pswpElement = document.querySelectorAll('.pswp')[0];
    // define options (if needed)
    var ix = parseInt(item.getAttribute('ix'));

    var options = {
        // optionName: 'option value'
        // for example:
        index: ix// start at first slide
    };
    console.log(options);
    pswp = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
    pswp.init();
}

function onMessage(evt) {

    console.log(evt.data);
    var objJSON = eval("(function(){return " + evt.data + ";} )()");


    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemId == 'ImageViewItem_ThumbNailItem') {

            //loadCanvas(objJSON.Url_Image);

            var aItem = document.createElement('a');
            var imageItem = document.createElement('img');
            aItem.appendChild(imageItem);
            if (gallery == undefined) {
                ixImage = 0;
                gallery = document.getElementById('demo-test-gallery');
                aItem.setAttribute('link', viewItem.LastValue.MultimediaUrl);
                aItem.setAttribute('ix', ixImage);
                aItem.setAttribute('onclick', 'clickedImage(this)');
                imageItem.setAttribute('src', viewItem.LastValue.ThumbImageUrl);
                aItem.appendChild(imageItem);
                gallery.appendChild(aItem);

                var pswpElement = document.querySelectorAll('.pswp')[0];

                // build items array
                items = [
                    {
                        mediumImage: 
                        {
                            src: viewItem.LastValue.MultimediaUrl,
                            w: 800,
                            h: 600
                        },
                        originalImage: {
                            src: viewItem.LastValue.MultimediaUrl,
                            w: 1400,
                            h: 1050
                        }
                    }
                ];

                // define options (if needed)
                var options = {
                    // optionName: 'option value'
                    // for example:
                    index: 0
                };

                // Initializes and opens PhotoSwipe
                pswp = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

                // create variable that will store real size of viewport
                var realViewportWidth,
                    useLargeImages = false,
                    firstResize = true,
                    imageSrcWillChange;

                // beforeResize event fires each time size of gallery viewport updates
                pswp.listen('beforeResize', function () {
                    // gallery.viewportSize.x - width of PhotoSwipe viewport
                    // gallery.viewportSize.y - height of PhotoSwipe viewport
                    // window.devicePixelRatio - ratio between physical pixels and device independent pixels (Number)
                    //                          1 (regular display), 2 (@2x, retina) ...


                    // calculate real pixels when size changes
                    realViewportWidth = pswp.viewportSize.x * window.devicePixelRatio;

                    // Code below is needed if you want image to switch dynamically on window.resize

                    // Find out if current images need to be changed
                    if (useLargeImages && realViewportWidth < 1000) {
                        useLargeImages = false;
                        imageSrcWillChange = true;
                    } else if (!useLargeImages && realViewportWidth >= 1000) {
                        useLargeImages = true;
                        imageSrcWillChange = true;
                    }

                    // Invalidate items only when source is changed and when it's not the first update
                    if (imageSrcWillChange && !firstResize) {
                        // invalidateCurrItems sets a flag on slides that are in DOM,
                        // which will force update of content (image) on window.resize.
                        pswp.invalidateCurrItems();
                    }

                    if (firstResize) {
                        firstResize = false;
                    }

                    imageSrcWillChange = false;

                });


                // gettingData event fires each time PhotoSwipe retrieves image source & size
                pswp.listen('gettingData', function (index, item) {

                    // Set image source & size based on real viewport width
                    if (useLargeImages) {
                        item.src = item.originalImage.src;
                        item.w = item.originalImage.w;
                        item.h = item.originalImage.h;
                    } else {
                        item.src = item.mediumImage.src;
                        item.w = item.mediumImage.w;
                        item.h = item.mediumImage.h;
                    }

                    // It doesn't really matter what will you do here, 
                    // as long as item.src, item.w and item.h have valid values.
                    // 
                    // Just avoid http requests in this listener, as it fires quite often

                });

                pswp.init();
            }
            else {
                ixImage++;
                aItem.setAttribute('link', viewItem.LastValue.MultimediaUrl);
                aItem.setAttribute('ix', ixImage);
                aItem.setAttribute('onclick', 'clickedImage(this)');
                imageItem.setAttribute('src', viewItem.LastValue.ThumbImageUrl);
                aItem.appendChild(imageItem);
                gallery.appendChild(aItem);
                pswp.items.push({
                    mediumImage:
                    {
                        src: viewItem.LastValue.MultimediaUrl,
                        w: 800,
                        h: 600
                    },
                    originalImage: {
                        src: viewItem.LastValue.MultimediaUrl,
                        w: 1400,
                        h: 1050
                    }
                });

                // sets a flag that slides should be updated
                pswp.invalidateCurrItems();
                // updates the content of slides
                pswp.updateSize(true);
            }




        }
        else if (viewItem.ViewItemId == 'IsSuccessful_Login') {
            if (!viewItem.LastValue) {
                gallery = undefined;
                $('#demo-test-gallery').html('');
                return;
            }
            //console.info('send width and height');
            //console.info($('canvas').width);
            //SendStringPropertyChangeWithValue('Width_Canvas', $('canvas').width());
            //SendStringPropertyChangeWithValue('Height_Canvas', $('canvas').height());
            //console.info('sended width and height');

        }
        else if (viewItem.ViewItemId == 'IsEnabled_NavFirst') {
            //$('#jqxNavFirstButton').jqxButton({ disabled: !objJSON.IsEnabled_NavFirst });
        }
        else if (viewItem.ViewItemId == 'IsEnabled_NavPrevious') {
            //$('#jqxNavPreviousButton').jqxButton({ disabled: !objJSON.IsEnabled_NavPrevious });

        }
        else if (viewItem.ViewItemId == 'IsEnabled_NavNext') {

            //$('#jqxNavNextButton').jqxButton({ disabled: !objJSON.IsEnabled_NavNext });

        }
        else if (viewItem.ViewItemId == 'IsEnabled_NavLast') {
            //$('#jqxNavLastButton').jqxButton({ disabled: !objJSON.IsEnabled_NavLast });
        }
        else if (viewItem.ViewItemId == 'Label_Nav') {
            //$('#labelNav').text(objJSON.Label_Nav);
        }
        else if (viewItem.ViewItemId == 'Count_ToSave') {
            //$('#countToSave').text(objJSON.Count_ToSave);
        }

    }




    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams();

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}