﻿var jPlaylist;

var toolsCount = 0;
var mimeType;
var audP;

function init() {
    noLog = true;
    initPlayer();
    initToolbar();
    //createOntologyToolbar();
    //createToolBar();
    //audP = document.getElementById('audioPlayer');
    //audP.onended = function () {
    //    SendStringCommand('stopped', "Pos", this.currentTime);
    //}
    //audP.onplay = function () {
    //    SendStringCommand('started', "Pos", this.currentTime);
    //}
    //audP.onpause = function () {
    //    SendStringCommand('paused', "Pos", this.currentTime);
    //}
    //audP.ontimeupdate = function () {
    //    SendStringCommand('curpos', "Pos", this.currentTime);
    //}

    testWebSocket();

}

function initToolbar() {
    $('#toolbar').kendoToolBar({
        items: [
                { type: "button", id: "listen", spriteCssClass: "fa fa-assistive-listening-systems", togglable: true },
                { type: "seperator" },
                { type: "button", id: "bookmark", spriteCssClass: "fa fa-bookmark", togglable: true },
        ],
        toggle: function (e) {
            console.log("toggle", e.target.text(), e.checked);
            var id = e.target.attr('id');

            if (id == 'listen') {
                SendStringPropertyChangeWithValue('IsToggled_Listen', e.checked);
            }
            else if (id == 'bookmark') {
                SendPropertyChangeWithValue("toggleBookmarks", "Checked", "ToggleButton", e.checked);
            }
            
        }
    });

}

function initPlayer() {
    jPlaylist = new jPlayerPlaylist({
        jPlayer: "#jquery_jplayer_2",
        cssSelectorAncestor: "#jp_container_2"
    }, [
		
    ], {
        supplied: "mp3",
        wmode: "window",
        useStateClassSkin: true,
        autoBlur: false,
        smoothPlayBar: true,
        keyEnabled: true,
        ended: function (event) {
            
            SendPropertyChangeWithValue("stopped", "Other", "Other", { id: jPlaylist.playlist[jPlaylist.current].id, currentTime: event.jPlayer.status.currentTime });
        },
        play: function (event) {
            SendPropertyChangeWithValue("started", "Other", "Other", { id: jPlaylist.playlist[jPlaylist.current].id, currentTime: event.jPlayer.status.currentTime });
            
        },
        pause: function (event) {
            SendPropertyChangeWithValue("paused", "Other", "Other", { id: jPlaylist.playlist[jPlaylist.current].id, currentTime: event.jPlayer.status.currentTime });
            
        },
        timeupdate: function (event) {
            SendPropertyChangeWithValue("curpos", "Other", "Other", { id: jPlaylist.playlist[jPlaylist.current].id, currentTime: event.jPlayer.status.currentTime });
        }
    });
}

function onMessage(evt) {

    ontoLog('<span style="color: blue;">Data: ' + evt.data + '</span>');
    var objJSON = eval("(function(){return " + evt.data + ";} )()");
    console.info(evt.data);


    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemId == 'MultimediaItem') {
            console.info(viewItem.LastValue);
            if (viewItem.LastValue == "") return;
            jPlaylist.add({
                id: viewItem.LastValue.Id,
                title: viewItem.LastValue.Name,
                mp3: viewItem.LastValue.MultimediaAbsoluteUrl
            })
            
            //loadAudio(viewItem.LastValue);




        }
        else if (viewItem.ViewItemId == 'IsSuccessful_Login') {
            if (!viewItem.LastValue) {

                return;
            }

        }
        //else if (viewItem.ViewItemId == 'IsEnabled_NavFirst') {
        //    $('#jqxNavFirstButton').jqxButton({ disabled: !viewItem.LastValue });
        //}
        //else if (viewItem.ViewItemId == 'IsEnabled_NavPrevious') {
        //    $('#jqxNavPreviousButton').jqxButton({ disabled: !viewItem.LastValue });

        //}
        //else if (viewItem.ViewItemId == 'IsEnabled_NavNext') {
        //    $('#jqxNavNextButton').jqxButton({ disabled: !viewItem.LastValue });

        //}
        //else if (viewItem.ViewItemId == 'IsEnabled_NavLast') {
        //    $('#jqxNavLastButton').jqxButton({ disabled: !viewItem.LastValue });
        //}
        //else if (viewItem.ViewItemId == 'Label_Nav') {
        //    $('#labelNav').text(viewItem.LastValue);
        //}
        //else if (viewItem.ViewItemId == 'Count_ToSave') {
        //    $('#countToSave').text(viewItem.LastValue);
        //}
        //else if (viewItem.ViewItemId == 'Label_MediaItem') {
        //    $('#mediaItemName').text(viewItem.LastValue)
        //}
        //else if (viewItem.ViewItemId == 'DataText_MediaMime') {
        //    mimeType = viewItem.LastValue;
        //}
        //else if (viewItem.ViewItemId == 'IsChecked_AutoPlay') {
        //    console.info('checked: ' + viewItem.LastValue);
        //    audP.autoplay = viewItem.LastValue;
        //    console.info('autoplay: ' + audP.autoplay);
        //}
        //else if (viewItem.ViewItemId == 'DataText_LastSavedBookmark') {
        //    console.info(viewItem.LastValue);
        //    $('#savedBookmark').text(viewItem.LastValue);
        //}
        //else if (viewItem.ViewItemId == 'Error_BookMark') {
        //    var error = viewItem.LastValue;
        //    if (error == undefined) {
        //        $('#errorMessage').hide();
        //    }
        //    else {
        //        $('#errorMessage').text(viewItem.LastValue);
        //        $('#errorMessage').show();
        //    }

        //}
        else if (viewItem.ViewItemId == 'IsToggled_Listen') {
            var toolbar = $("#toolbar").data("kendoToolBar");
            toolbar.toggle("#listen", viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'toggleBookmarks') {
            var toolbar = $("#toolbar").data("kendoToolBar");
            toolbar.toggle("#bookmark", viewItem.LastValue);
        }
        //else if (viewItem.ViewItemId == 'Sec_MoveTo') {
        //    audP.currentTime = viewItem.LastValue;
        //}
    }



    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams();

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

function loadAudio(dataURL) {
    console.info(dataURL);
    console.info('autoplay:' + audP.autoplay);
    $('#audioPlayer').attr('src', mimeType);
    $('#audioPlayer').attr('src', dataURL);
    if (audP.autoplay) {

        audP.load();
    }

    audP.onloadeddata = function () {
        SendStringCommandWithoutParameter("Loaded")
    };

}




window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}