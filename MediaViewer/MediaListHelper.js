﻿

var toolsCount = 0;

function init() {

    testWebSocket();
    initControls();
}

function initControls() {
    createUploadWindow();
    createPlayerWindow();

    $('#uploadItems').jqxButton({ width: 30, height: 30 })
    $('#uploadItems').on('click', function () {
        SendStringCommandWithoutParameter('UploadItems')
    });

    $('#removeFromList').jqxButton({ width: 30, height: 30 })
    $('#removeFromList').on('click', function () {
        SendStringCommandWithoutParameter('RemoveItems')
    });

    $('#applyItem').jqxButton({ width: 30, height: 30 })
    $('#applyItem').on('click', function () {
        SendStringCommandWithoutParameter('ApplyItems')
    });

    $('#isListen').jqxToggleButton({ width: 30, height: 30 });
    $('#isListen').on('click', function () {
        SendPropertyChangeWithValue('isListen', 'Other', 'Other', $("#isListen").jqxToggleButton('toggled'));
    });

    $('#openPlayer').jqxButton({ width: 30, height: 30 })
    $('#openPlayer').on('click', function () {
        SendStringCommandWithoutParameter('OpenPlayer')
    });
    
}

//Creating the demo window
function createUploadWindow() {
    var jqxWidget = $('#grid');
    var offset = jqxWidget.offset();
    $('#uploadMediaWindow').jqxWindow({
        position: { x: 100, y: 0 },
        showCollapseButton: true, maxHeight: 700, maxWidth: 1224, minHeight: 600, minWidth: 1024, height: 700, width: 1224,
        isModal: true,
        initContent: function () {
            $('#uploadMediaWindow').jqxWindow('focus');
        }
    });



    $('#uploadMediaWindow').jqxWindow('close');
};

//Creating the demo window
function createPlayerWindow() {
    var jqxWidget = $('#grid');
    var offset = jqxWidget.offset();
    $('#playerWindow').jqxWindow({
        position: { x: 0, y: 0 },
        showCollapseButton: true, maxHeight: 1400, maxWidth: 1400, minHeight: 200, minWidth: 200, height: playerHeight, width: playerWidth,
        isModal: true,
        initContent: function () {
            $('#playerWindow').jqxWindow('focus');
        }
    });



    $('#playerWindow').jqxWindow('close');
};

function onMessage(evt) {

    console.info(evt.data);
    var objJSON = eval("(function(){return " + evt.data + ";} )()");



    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemType == 'DataSource' && viewItem.ViewItemClass == 'Grid') {
            console.info(viewItem.LastValue);
            //createGrid(viewItem.LastValue);

            var dataSource = JSON.parse(JSON.stringify(viewItem.LastValue))

            dataAdapter = createDataAdapter(dataSource)

            if (dataAdapter != undefined && columnConfig != undefined) {
                GetColumnConfig();

                $('#grid').createGrid(dataAdapter, columnConfig, "Module");
                $('#grid').on('cellselect', function (event) {
                    // event arguments.
                    var args = event.args;
                    // column data field.
                    var datafield = event.args.datafield;
                    // row's bound index.
                    var rowBoundIndex = args.rowindex;
                    // new cell value.
                    var value = args.newvalue;
                    // old cell value.
                    var oldvalue = args.oldvalue;

                    var data = $('#' + viewItem.ViewItemId).jqxGrid('getrowdata', rowBoundIndex);

                    var sendData = JSON.stringify({ "Id": data.IdRow, "ColumnName": datafield, "IdRow": rowBoundIndex });
                    SendPropertyChangeWithValue(viewItem.ViewItemId, "SelectedIndex", "Grid", sendData);
                    SendStringCommand("selectedMediaItem", "idItem", data.IdItem);
                });
                $('#' + viewItem.ViewItemId).on('cellvaluechanged', function (event) {
                    // event arguments.
                    var args = event.args;
                    // column data field.
                    var datafield = event.args.datafield;
                    // row's bound index.
                    var rowBoundIndex = args.rowindex;
                    // new cell value.
                    var value = args.newvalue;
                    // old cell value.
                    var oldvalue = args.oldvalue;

                    var data = $('#' + viewItem.ViewItemId).jqxGrid('getrowdata', rowBoundIndex);

                    if (datafield == 'ItemApply') {
                        if (value == true) {
                            SendStringCommand("AppliedObjectRow", "Id", data.IdRow);
                        }
                        else {
                            SendStringCommand("UnAppliedObjectRow", "Id", data.IdRow);
                        }
                    }
                    ontoLog(args);
                });

                $("#grid").on("sort", function (event) {
                    // event arguments.
                    var args = event.args;
                    // sorting information.
                    var sortInfo = event.args.sortinformation;
                    // sort direction.
                    var sortdirection = sortInfo.sortdirection.ascending ? "ascending" : "descending";
                    // column data field.
                    var sortColumnDataField = sortInfo.sortcolumn;

                    var value = {
                        Column: sortColumnDataField,
                        Direction: sortdirection
                    };

                    SendPropertyChangeWithValue('grid', 'Sort', 'Grid', value);
                });
            }

        }
        else if (viewItem.ViewItemType == 'ColumnConfig' && viewItem.ViewItemClass == 'Grid') {
            dataAdapter = null;
            columnConfig = JSON.parse(JSON.stringify(viewItem.LastValue.ColumnList))
        }
        else if (viewItem.ViewItemId == 'reloadGrid') {
            dataAdapter.dataBind();
        }
        else if (viewItem.ViewItemId == 'OpenUploadView') {
            var iframeUploadSrc = $('#iframeUpload').attr('src');

            $('#iframePlayer').attr('src', iframeUploadSrc);
            $('#uploadMediaWindow').jqxWindow('open');
        }
        else if (viewItem.ViewItemId == 'CloseUploadView') {
            $('#uploadMediaWindow').jqxWindow('close');
        }
        else if (viewItem.ViewItemId == 'OpenPlayerView') {
            $('#playerWindow').jqxWindow('open');
        }
        else if (viewItem.ViewItemClass == 'IFrame' && viewItem.ViewItemType == 'Content') {
            $('#' + viewItem.ViewItemId).attr('src', viewItem.LastValue);
        }
        else if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemId == 'viewText') {
            document.title = viewItem.LastValue;
        }
        else if (viewItem.ViewItemId == 'isListen') {
            if (viewItem.LastValue) {
                $('#isListen').jqxToggleButton('check');
            }
            else {
                $('#isListen').jqxToggleButton('unCheck');
            }
        }
    }


    //websocket.close();
}

function GetColumnConfig() {
    for (var i in columnConfig) {
        if (columnConfig[i].datafield == 'Url') {
            columnConfig[i].cellsrenderer = linkrenderer;
        }
    }
}

function onOpen(evt) {
    sendStandardParams();

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
}

function onError(evt) {
    
}

function errorOccured(error, url, line) {

}


window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}