﻿

var images;
var toolsCount = 0;
var mimeType;
var pdfUrl;

function init() {

    CreateToolBar();
    testWebSocket();

}

function onMessage(evt) {

    ontoLog('<span style="color: blue;">Data: ' + evt.data + '</span>');
    var objJSON = eval("(function(){return " + evt.data + ";} )()");



    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemId == 'Url_PDF') {
            console.info(viewItem.LastValue);

            loadPDF(viewItem.LastValue);



        }
        else if (viewItem.ViewItemId == 'IsSuccessful_Login') {
            if (!viewItem.IsSuccessful_Login) {

                return;
            }

        }
        else if (viewItem.ViewItemId == 'IsEnabled_NavFirst') {
            $('#jqxNavFirstButton').jqxButton({ disabled: !viewItem.LastValue });
        }
        else if (viewItem.ViewItemId == 'IsEnabled_NavPrevious') {
            $('#jqxNavPreviousButton').jqxButton({ disabled: !viewItem.LastValue });

        }
        else if (viewItem.ViewItemId == 'IsEnabled_NavNext') {
            $('#jqxNavNextButton').jqxButton({ disabled: !viewItem.LastValue });

        }
        else if (viewItem.ViewItemId == 'IsEnabled_NavLast') {
            $('#jqxNavLastButton').jqxButton({ disabled: !viewItem.LastValue });
        }
        else if (viewItem.ViewItemId == 'IsEnabled_AutoOpen') {
            $('#jqxAutoOpenButton').jqxButton({ disabled: !viewItem.LastValue });

        }
        else if (viewItem.ViewItemId == 'IsEnabled_OpenPdf') {
            $('#jqxOpen').jqxButton({ disabled: !viewItem.LastValue });

        }
        else if (viewItem.ViewItemId == 'Label_Nav') {
            $('#labelNav').text(viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'Count_ToSave') {
            $('#countToSave').text(viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'Label_MediaItem') {
            $('#mediaItemName').text(viewItem.LastValue)
        }
        else if (viewItem.ViewItemId == 'DataText_MediaMime') {
            mimeType = viewItem.LastValue;
        }


    }




    //websocket.close();
}


function loadPDF(dataURL) {
    console.info(dataURL);
    if (dataURL != undefined && dataURL != '') {
        window.open(dataURL, '_blank');
    }


}


function CreateToolBar() {
    $("#jqxNavFirstButton").jqxButton({ width: 32, height: 32 });
    $("#jqxNavPreviousButton").jqxButton({ width: 32, height: 32 });
    $("#jqxNavNextButton").jqxButton({ width: 32, height: 32 });
    $("#jqxNavLastButton").jqxButton({ width: 32, height: 32 });
    $("#jqxAutoOpenButton").jqxToggleButton({ width: 32, height: 32, toggled: false });

    $('#jqxNavFirstButton').jqxButton({ disabled: true });
    $('#jqxNavPreviousButton').jqxButton({ disabled: true });
    $('#jqxNavNextButton').jqxButton({ disabled: true });
    $('#jqxNavLastButton').jqxButton({ disabled: true });
    $('#jqxOpen').jqxButton({ disabled: true });

    $("#jqxNavFirstButton").on('click', function () {
        SendStringCommandWithoutParameter('NavFirst');

    });
    $("#jqxNavPreviousButton").on('click', function () {
        SendStringCommandWithoutParameter('NavPrevious');
    });
    $("#jqxNavNextButton").on('click', function () {
        SendStringCommandWithoutParameter('NavNext');
    });
    $("#jqxNavLastButton").on('click', function () {
        SendStringCommandWithoutParameter('NavLast');
    });

    $("#jqxOpen").on('click', function () {
        SendStringCommandWithoutParameter('OpenPdf');
    });

    $("#jqxAutoOpenButton").on('click', function () {
        var toggled = $("#jqxAutoOpenButton").jqxToggleButton('toggled');
        if (toggled) {
            SendStringPropertyChangeWithValue('IsChecked_AutoOpen', true);
        }
        else {
            SendStringPropertyChangeWithValue('IsChecked_AutoOpen', false);
        }
    });
}

function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams();

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}