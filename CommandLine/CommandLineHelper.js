﻿var scriptletItem;
var editor;
var markerIds = [];
var rangesVariable = [];
var myWindow;

function init() {
    testWebSocket();
    initToolbar();

    myWindow = $("#window");

    myWindow.kendoWindow({
        width: "600px",
        height: "400px",
        title: "Variables",
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ]
    }).data("kendoWindow");
}

function initToolbar() {
    $('#toolbar').kendoToolBar({
        items: [
                { type: "button", id: "listen", spriteCssClass: "fa fa-assistive-listening-systems", togglable: true },
                { type: "seperator" },
                { type: "button", id: "btnSave", spriteCssClass: "fa fa-floppy-o", enable: false },
                { type: "seperator" },
                { type: "button", id: "btnFindVariables", spriteCssClass: "fa fa-eye", enable: true },
                { type: "button", id: "btnExchangeVariables", spriteCssClass: "fa fa-exchange", enable: true },
                { type: "seperator" },
                { type: "button", id: "btnRefresh", spriteCssClass: "fa fa-refresh", enable: false },
        ],
        toggle: function (e) {
            console.log("toggle", e.target.text(), e.checked);
            var id = e.target.attr('id');

            if (id == 'listen') {
                SendStringCommandWithoutParameter('clicked.listen');
            }

        }

        
    });

    $('#btnSave').kendoButton({
        click: function (e) {
            SendStringCommandWithoutParameter('clicked.save');
        }
    });

    $('#btnFindVariables').kendoButton({
        click: function (e) {
            findVariables();
        }
    });

    $('#btnExchangeVariables').kendoButton({
        click: function (e) {
            findVariables();
            $('#window').data("kendoWindow").open();
        }
    });

    $('#btnRefresh').kendoButton({
        click: function (e) {
            confirmText("Wollen Sie den Code wirklich neu laden?");

        }
    });
}

function confirmText(text) {

    elem = $(this);

    var kendoWindow = $('<div>' + text + '</div>').kendoWindow({
        title: "Confirm",
        resizable: false,
        modal: true
    });

    kendoWindow.data("kendoWindow")
        .content($("#delete-confirmation").html());

    kendoWindow.data("kendoWindow").center().open();

    kendoWindow
        .find(".delete-confirm,.delete-cancel")
            .click(function () {
                if ($(this).hasClass("delete-confirm")) {
                    kendoWindow.data("kendoWindow").close();
                    SendStringCommandWithoutParameter('code.reload');
                }
                else {
                    kendoWindow.data("kendoWindow").close();
                }


            })
            .end()

}

function findVariables() {
    if (scriptletItem == undefined || scriptletItem.Variables == undefined) {
        return;
    }

    for (var ix in scriptletItem.Variables) {
        var variable = '@' + scriptletItem.Variables[ix].Name + '@';
        editor.$search.set({ needle: variable });
        var ranges = editor.$search.findAll(editor.getSession());
        for (var jx in ranges) {
            var range = ranges[jx];
            rangesVariable.push({
                range: range,
                variable: variable
            });
            markerIds.push(editor.session.addMarker(range, "ace_bracketx", "text"))
        }
    }
}

function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);

        
        if (viewItem.ViewItemId == 'viewText') {
            document.title = viewItem.LastValue;
        }
        else if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemClass == 'Input' && viewItem.ViewItemType == 'Content') {
            setViewItemContent(viewItem);
        }
        else if (viewItem.ViewItemClass == 'NumericInput' && viewItem.ViewItemType == 'Content') {
            var numerictextbox = $("#" + viewItem.ViewItemId).data("kendoNumericTextBox");

            numerictextbox.value(viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'isListen') {
            var toolbar = $("#toolbar").data("kendoToolBar");
            toolbar.toggle("#listen", viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'script') {
            $('#window').data("kendoWindow").close();
            for (var ix in markerIds) {
                editor.session.removeMarker(markerIds[ix]);
            }
            rangesVariable = [];
            scriptletItem = viewItem.LastValue;
            editor = ace.edit(viewItem.ViewItemId);
            editor.setTheme("ace/theme/twilight");
            editor.session.setMode("ace/mode/" + scriptletItem.Mode);
            editor.setReadOnly(true)
            editor.on('change', function () {
                
                var isReadOnly = editor.getReadOnly();
                if (isReadOnly) return;
                for (var ix in markerIds) {
                    editor.session.removeMarker(markerIds[ix]);
                }
                rangesVariable = [];
                SendStringCommandWithoutParameter('code.changed');

            });
            AddVariablesToWindow(scriptletItem.Variables);

            $.ajax({
                url: scriptletItem.UrlScript,
                success: function (data) {
                    editor.setValue(data);
                    editor.setReadOnly(false);
                    editor.container.style.opacity = 1;
                }
            });
        }
        else if (viewItem.ViewItemId == 'send.content') {
            var content = editor.getValue();
            var changedItem = {
                IdAttributeCode: scriptletItem.IdAttributeCode,
                Code: content
            };
            editor.setReadOnly(true)
            editor.container.style.opacity = 0.5;
            SendPropertyChangeWithValue('script', "Content", "Other", changedItem);
        }
        else if (viewItem.ViewItemId == 'script.saved') {
            editor.setReadOnly(false)
            editor.container.style.opacity = 1;
        }
        
        
    }

    //websocket.close();
}

function AddVariablesToWindow(variables) {
    var fieldList = $('#window').find('.fieldlist');
    fieldList.empty();
    for (var ix in variables) {
        var variable = variables[ix];
        var tr = $('<tr></tr>');
        fieldList.append(tr);
        var label = $('<td valign="middle">' + variable.Name + ':</td>');
        tr.append(label);
        var input = $('<td><input type="text" class="k-textbox variableInput" data-variable="@' + variable.Name + '@" style="width: 100%;" /></td>');
        tr.append(input);
    }

    $('.variableInput').on('change', function () {
        editor.setReadOnly(true);
        var variable = $(this).data('variable');
        var text = $(this).val();
        var rangeVariablesFound = jQuery.grep(rangesVariable, function (elem) {
            return elem.variable == variable;
        });

        for (var ix in rangeVariablesFound) {
            var range = rangeVariablesFound[ix];
            editor.session.replace(range.range, text);
        }
        editor.setReadOnly(false);
    });
    
}

function setButtonIcon(viewItem) {
    if (viewItem.LastValue == true) {
        $('#' + viewItem.ViewItemId).html('<i class="fa fa-assistive-listening-systems" aria-hidden="true"></i>')
        $('#' + viewItem.ViewItemId).removeClass('k-default');
        $('#' + viewItem.ViewItemId).addClass('k-primary');
    }
    else {
        $('#' + viewItem.ViewItemId).html('<i class="fa fa-circle-thin" aria-hidden="true"></i>')
        $('#' + viewItem.ViewItemId).removeClass('k-primary');
        $('#' + viewItem.ViewItemId).addClass('k-default');
        
    }
    
}

function onOpen(evt) {
    ontoLog("CONNECTED");

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}