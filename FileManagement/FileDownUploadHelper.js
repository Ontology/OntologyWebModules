﻿



function init() {


    output = document.getElementById("output");
    testWebSocket();
    initControls();
}

function initControls() {
    createNewItemWindow();
    createDeleteWindow();

    $('#applyItem').jqxButton({ width: 30, height: 30 })
    $('#applyItem').on('click', function () {
        SendStringCommandWithoutParameter('AppliedObjectRows')
    });

    $('#newItem').jqxButton({ width: 30, height: 30 })
    $('#newItem').on('click', function () {
        SendStringCommandWithoutParameter('NewItem')
    });

    $('#deleteItem').jqxButton({ width: 30, height: 30 })
    $('#deleteItem').on('click', function () {
        SendStringCommandWithoutParameter('DeleteItem')
    });

    $('#downloadFiles').jqxButton({ width: 30, height: 30 })
    $('#downloadFiles').on('click', function () {
        SendStringCommandWithoutParameter('DownloadFiles')
    });

    $('#isListen').jqxToggleButton({ width: 30, height: 30 });
    $('#isListen').on('click', function () {
        SendPropertyChangeWithValue('isListen', 'Other', 'Other', $("#isListen").jqxToggleButton('toggled'));
    });

}

//Creating the demo window
function createNewItemWindow() {
    var jqxWidget = $('#grid');
    var offset = jqxWidget.offset();
    $('#newItemWindow').jqxWindow({
        position: { x: 100, y: 100 },
        showCollapseButton: true, maxHeight: 700, maxWidth: 1224, minHeight: 600, minWidth: 1024, height: 700, width: 1224,
        isModal: true,
        initContent: function () {
            $('#newItemWindow').jqxWindow('focus');
        }
    });



    $('#newItemWindow').jqxWindow('close');
};

//Creating the demo window
function createDeleteWindow() {
    $('#deleteWindowMessage').jqxWindow({
        position: { x: 100, y: 100 },
        showCollapseButton: true, maxHeight: 600, maxWidth: 1024, minHeight: 600, minWidth: 1024, height: 600, width: 1024,
        isModal: true,
        initContent: function () {
            $('#deleteWindowMessage').jqxWindow('focus');
        }
    });



    $('#deleteWindowMessage').jqxWindow('close');
};


function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);
 
        if (viewItem.ViewItemType == 'DataSource' && viewItem.ViewItemClass == 'Grid') {
            console.info(viewItem.LastValue);
            //createGrid(viewItem.LastValue);

            var dataSource = JSON.parse(JSON.stringify(viewItem.LastValue))

            dataAdapter = createDataAdapter(dataSource)

            if (dataAdapter != undefined && columnConfig != undefined) {

                $('#grid').createGrid(dataAdapter, columnConfig, "Module");
                $('#grid').on('cellselect', function (event) {
                    // event arguments.
                    var args = event.args;
                    // column data field.
                    var datafield = event.args.datafield;
                    // row's bound index.
                    var rowBoundIndex = args.rowindex;
                    // new cell value.
                    var value = args.newvalue;
                    // old cell value.
                    var oldvalue = args.oldvalue;

                    var data = $('#' + viewItem.ViewItemId).jqxGrid('getrowdata', rowBoundIndex);

                    var sendData = JSON.stringify({ "Id": data.IdRow, "ColumnName": datafield, "RowId": rowBoundIndex });
                    SendPropertyChangeWithValue(viewItem.ViewItemId, "SelectedIndex", "Grid", sendData);
                });

                $('#' + viewItem.ViewItemId).on('cellvaluechanged', function (event) {
                    // event arguments.
                    var args = event.args;
                    // column data field.
                    var datafield = event.args.datafield;
                    // row's bound index.
                    var rowBoundIndex = args.rowindex;
                    // new cell value.
                    var value = args.newvalue;
                    // old cell value.
                    var oldvalue = args.oldvalue;

                    var data = $('#' + viewItem.ViewItemId).jqxGrid('getrowdata', rowBoundIndex);

                    if (datafield == 'Apply') {
                        if (value == true) {
                            SendStringCommand("PreAppliedRow", "IdRow", data.IdRow);
                        }
                        else {
                            SendStringCommand("UnPreAppliedRow", "IdRow", data.IdRow);
                        }
                    }
                    ontoLog(args);
                });
            }

        }
        else if (viewItem.ViewItemId == 'isListen') {
            if (viewItem.LastValue) {
                $('#isListen').jqxToggleButton('check');
            }
            else {
                $('#isListen').jqxToggleButton('unCheck');
            }
        }
        else if (viewItem.ViewItemType == 'ColumnConfig' && viewItem.ViewItemClass == 'Grid') {
            columnConfig = JSON.parse(JSON.stringify(viewItem.LastValue.ColumnList))
        }
        else if (viewItem.ViewItemId == 'reloadGrid') {
            dataAdapter.dataBind();
        }
        else if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemId == 'downloadUrl') {
            window.open(viewItem.LastValue, 'Download');
        }
         
    }

    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");

   
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}