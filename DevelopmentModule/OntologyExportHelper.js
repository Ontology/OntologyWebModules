﻿function init() {


    testWebSocket();
    initToolbar();
}

function initToolbar() {
    $("#toolbar").kendoToolBar({
        items: [
            {
                template: '<button id="createFiles" class="k-default"><i class="fa fa-file-text" aria-hidden="true"></i></button>'
            },
            {
                template: '<button id="saveFiles" class="k-default"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>'
            }
        ]
    });
}

function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);

    }

    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}