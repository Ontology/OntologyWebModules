﻿

var with1 = 350;
var with2 = 345;


function init() {

    testWebSocket();
    initControls();
}

function initControls() {


}

function onMessage(evt) {

    console.log(evt.data);
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemType == 'Visible' && viewItem.ViewItemClass == 'Label') {
            setViewItemVisibility(viewItem);
        }
        else if (viewItem.ViewItemType == 'Content' && viewItem.ViewItemClass == 'Label') {
            setViewItemContent(viewItem);
        }
        else if (viewItem.ViewItemId == 'cmboLanguages' && viewItem.ViewItemType == 'DataSource') {

            $('#' + viewItem.ViewItemId).createCombo(viewItem.LastValue, with1, 25);

            $("#" + viewItem.ViewItemId).on('select', function (event) {
                if (event.args) {
                    var item = event.args.item;
                    if (item) {
                        SendStringCommand("SelectedLanguage", 'Id', item.value);
                    }
                }
            });
        }
        else if (viewItem.ViewItemId == 'cmboPLanguages' && viewItem.ViewItemType == 'DataSource') {

            $('#' + viewItem.ViewItemId).createCombo(viewItem.LastValue, with1, 25);

            $("#" + viewItem.ViewItemId).on('select', function (event) {
                if (event.args) {
                    var item = event.args.item;
                    if (item) {
                        SendStringCommand("SelectedPLanguage", 'Id', item.value);
                    }
                }
            });
        }
        else if (viewItem.ViewItemId == 'cmboUsers' && viewItem.ViewItemType == 'DataSource') {

            $('#' + viewItem.ViewItemId).createCombo(viewItem.LastValue, with1, 25);

            $("#" + viewItem.ViewItemId).on('select', function (event) {

                if (event.args) {
                    var item = event.args.item;
                    if (item) {
                        SendStringCommand("SelectedUser", 'Id', item.value);
                    }
                }
            });
        }
        else if (viewItem.ViewItemId == 'cmboStates' && viewItem.ViewItemType == 'DataSource') {

            $('#' + viewItem.ViewItemId).createCombo(viewItem.LastValue, with1, 25);

            $("#" + viewItem.ViewItemId).on('select', function (event) {
                if (event.args) {
                    var item = event.args.item;
                    if (item) {
                        SendStringCommand("SelectedState", 'Id', item.value);
                    }
                }
            });
        }
        else if (viewItem.ViewItemId == 'lstBoxAdditionalLanguages' && viewItem.ViewItemType == 'DataSource') {

            $('#' + viewItem.ViewItemId).createListBox(viewItem.LastValue, with1, 400, true);

            $('#' + viewItem.ViewItemId).on('checkChange', function (event) {
                var args = event.args;
                if (args.checked) {
                    var item = args.item;
                    SendStringCommand("CheckedAdditionalLanguages", "Id", item.value);
                }
                else {
                    var item = args.item;
                    SendStringCommand("UnCheckedAdditionalLanguages", "Id", item.value);
                }

            });
        }
        else if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemType == 'Visible') {
            setViewItemVisibility(viewItem);
        }
        else if (viewItem.ViewItemType == 'Content' && viewItem.ViewItemClass == 'Input') {
            setViewItemContent(viewItem);
        }
        else if (viewItem.ViewItemClass == 'Input' && viewItem.ViewItemType == 'PlaceHolder') {
            $("#" + viewItem.ViewItemId).jqxInput({ placeHolder: viewItem.LastValue, height: 25, width: with2, minLength: 1 });
        }
        else if (viewItem.ViewItemClass == "Combobox" && viewItem.ViewItemType == 'SelectedIndex') {
            setSelectedIndex(viewItem);
        }
        else if (viewItem.ViewItemClass == "ListBox" && viewItem.ViewItemType == 'SelectedIndex') {
            checkListBoxIndex(viewItem);
        }
        else if (viewItem.ViewItemId == 'exitMessage') {
            SetCloseMessage(viewItem);
        }

    }




    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");


    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}