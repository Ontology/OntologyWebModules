﻿

function init() {

    $('#jqxToolBar').createOTreeBar()
    testWebSocket();

}

function onMessage(evt) {

    console.log(evt.data);
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemId == 'Url_TreeData') {

            $('#list').createOntoTree(viewItem.LastValue);
            //jQuery.getJSON(objJSON.Url_TreeData, function (data) {
            //    initTree(data);
            //});



        }
        else if (viewItem.ViewItemId == 'IsSuccessful_Login') {
            if (!viewItem.LastValue) {

                return;
            }

        }
        else if (viewItem.ViewItemId == 'ViewTreeNodes_PathNodes') {
            console.info('pathNodes: ' + viewItem.LastValue);
            var pathNodes = viewItem.LastValue;
            $('#jqxToolBar').configureOTreePath(pathNodes);
        }

    }




    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");


    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}