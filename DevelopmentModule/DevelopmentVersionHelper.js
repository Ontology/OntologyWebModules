﻿

function init() {

    testWebSocket();

}

function onMessage(evt) {

    console.log(evt.data);
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemType == 'Content' && viewItem.ViewItemClass == 'Label') {
            setViewItemContent(viewItem);
        }
        else if (viewItem.ViewItemType == 'Content' && viewItem.ViewItemClass == 'IFrame') {
            setViewItemContent(viewItem);
        }
        else if (viewItem.ViewItemId == 'viewText') {
            document.title = viewItem.LastValue;
        }
    }




    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");
    

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}