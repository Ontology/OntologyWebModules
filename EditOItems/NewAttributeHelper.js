﻿var valElement;

function init() {

    initControls()
    testWebSocket(null, true);

}

function initControls() {
    $("#newGuid").jqxButton({ width: 40, height: 27 });
    $("#newGuid").on('click', function () {
        SendStringCommandWithoutParameter("SetNewGuid")
    });
    $("#saveAttribute").jqxButton({ width: 50, height: 27 });
    $("#saveAttribute").jqxButton('disabled', true);
    $("#saveAttribute").on('click', function () {
        SendStringCommandWithoutParameter("SaveItem")
    });
}

function onMessage(evt) {

    console.log(evt.data);
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];


        if (viewItem.ViewItemClass == 'CheckBox' && viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemClass == 'DateTimeInput' && viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemClass == 'NumericInput' && viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemClass == 'NumericInput' && viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemClass == 'TextArea' && viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemType == 'Content' || viewItem.ViewItemType == 'Caption') {
            setViewItemContent(viewItem)
        }
        else if (viewItem.ViewItemClass == 'Button' && viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem)
        }
        else if (viewItem.ViewItemId == 'newAttribute') {
            

            $('#valueContainer').html('');



            if (viewItem.LastValue.IdDataType == dataTypes.idDataTypeBool) {
                var divItem = document.createElement("div");
                divItem.id = "inpValue";

                $('#valueContainer').append(divItem);

                $("#inpValue").jqxCheckBox({ width: 120, height: 25 });
                $("#inpValue").on('change', function (event) {
                    var checked = event.args.checked;
                    
                    SendPropertyChangeWithValue("inpValue", "Content", "CheckBox", checked);
                });
            }
            else if (viewItem.LastValue.IdDataType == dataTypes.idDataTypeDateTime) {
                var divItem = document.createElement("div");
                divItem.id = "inpValue";

                $('#valueContainer').append(divItem);

                $("#inpValue").jqxDateTimeInput({ formatString: "dd.MM.yyyy HH:mm:ss", showTimeButton: true, showCalendarButton: true, width: '300px', height: '25px' });
            }
            else if (viewItem.LastValue.IdDataType == dataTypes.idDataTypeInt) {
                var divItem = document.createElement("div");
                divItem.id = "inpValue";

                $('#valueContainer').append(divItem);

                $("#inpValue").jqxNumberInput({ width: '250px', height: '25px', symbol: "", min: 0, spinButtons: true, decimalDigits: 0, digits: 10 });
            }
            else if (viewItem.LastValue.IdDataType == dataTypes.idDataTypeDouble) {
                var divItem = document.createElement("div");
                divItem.id = "inpValue";

                $('#valueContainer').append(divItem);

                $("#inpValue").jqxNumberInput({ width: '250px', height: '25px', symbol: "", min: 0, spinButtons: true, decimalDigits: 6, digits: 10 });
            } 
            else if (viewItem.LastValue.IdDataType == dataTypes.idDataTypeString) {
                var divItem = document.createElement("textarea");
                divItem.id = "inpValue";

                $('#valueContainer').append(divItem);
                $("#inpValue").on('change', function () {
                    SendPropertyChangeWithValue("inpValue", "Content", "TextArea", this.value);
                });
            }       

            $('#inpAttributeGuid').val(viewItem.LastValue.IdAttribute);
            $('#txtClass').html(viewItem.LastValue.NameClass);
            $('#txtObject').html(viewItem.LastValue.NameObject);
            $('#itemIdentificationLabel').html(viewItem.LastValue.NameAttributeType + '(' + viewItem.LastValue.NameDataType + ')');
        }
        else if (viewItem.ViewItemId == 'newGuid') {
            $('#inpAttributeGuid').val(viewItem.LastValue);
        }
        

    }

    //websocket.close();
}


function onOpen(evt) {
    ontoLog("CONNECTED");
    SendPropertyChangeWithValue("viewId", "Other", "Other", view)
    var idAttribute = getUrlParameter('IdAttribute');
    if (idAttribute != undefined) {
        SendPropertyChangeWithValue("idAttribute", "Other", "Other", idAttribute)
    }
    else {
        var idClass = getUrlParameter('Class');
        var idObject = getUrlParameter('Object');
        var idAttributeType = getUrlParameter('AttributeType');

        if (idClass != undefined &&
            idObject != undefined &&
            idAttributeType != undefined) {

            var newAttributeItem = {
                IdAttributeType: idAttributeType,
                IdClass: idClass,
                IdObject: idObject
            };

            SendPropertyChangeWithValue("newAttributeRequest", "Other", "Other", newAttributeItem);
        }

        
    }
    
    
    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}