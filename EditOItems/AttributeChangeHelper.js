﻿var idAttribute;

var views = {
    ViewIdBool: "b6c235c44a5a42a2b5f6e6ae75dbf753",
    ViewIdDateTime: "27e56a1b78e84394863ea5738b6cacb8",
    ViewIdDouble: "c31480ee106c42d78a9fab3d132d4697",
    ViewIdInt: "c8df2d290506490489e6423caef95808",
    ViewIdString: "0836057505884a76a922b3c34dbb7e52"
};

function init() {

    initControls()
    testWebSocket(null, true);

}

function initControls() {
    if (view == views.ViewIdBool) {
        $("#inpValue").jqxCheckBox({ width: 120, height: 25 });
        
    }
    else if (view == views.ViewIdDateTime) {
        $("#inpValue").jqxDateTimeInput({ formatString: "dd.MM.yyyy HH:mm:ss", showTimeButton: true, showCalendarButton: true, width: '300px', height: '25px' });
    }
    else if (view == views.ViewIdDouble) {
        $("#inpValue").jqxNumberInput({ width: '250px', height: '25px', symbol: "", min: 0, spinButtons: true, decimalDigits: 6, digits: 10 });
    }
    else if (view == views.ViewIdInt) {
        $("#inpValue").jqxNumberInput({ width: '250px', height: '25px', symbol: "", min: 0, spinButtons: true, decimalDigits: 0, digits: 10 });
        
    }
    else if (view == views.ViewIdString) {
        $("#inpValue").on('change', function () {
            SendPropertyChangeWithValue("inpValue", "Content", "TextArea", this.value);
        });
    }
    
}

function onMessage(evt) {

    console.log(evt.data);
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];


        if (view == views.ViewIdBool && viewItem.ViewItemClass == 'CheckBox' && viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (view == views.ViewIdDateTime && viewItem.ViewItemClass == 'DateTimeInput' && viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (view == views.ViewIdDouble && viewItem.ViewItemClass == 'NumericInput' && viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (view == views.ViewIdInt && viewItem.ViewItemClass == 'NumericInput' && viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (view == views.ViewIdString && viewItem.ViewItemClass == 'TextArea' && viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemType == 'Content' || viewItem.ViewItemType == 'Caption') {
            setViewItemContent(viewItem)
        }
        else if (viewItem.ViewItemId == 'newAttribute') {
            $('#inpAttributeGuid').val(viewItem.LastValue.IdAttribute);
            $('#txtClass').html(viewItem.LastValue.NameClass);
            $('#txtObject').html(viewItem.LastValue.NameObject);
            $('#labelReference').html(viewItem.LastValue.NameAttributeType + '(' + viewItem.LastValue.NameDataType + ')');
            $('#window').jqxWindow('open');
        }
        else if (viewItem.ViewItemId == 'newGuid') {
            $('#inpAttributeGuid').val(viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'CloseAddWindow') {
            $('#window').jqxWindow('close');
        }
        

    }

    //websocket.close();
}

function _createWindow() {
    var jqxWidget = $('#attributeArea');
    var offset = jqxWidget.offset();
    $('#window').jqxWindow({
        position: { x: offset.left, y: offset.top },
        showCollapseButton: true, maxHeight: 1400, maxWidth: 1400, minHeight: 100, minWidth: 200, height: 250, width: 320,
        initContent: function () {
            $('#window').jqxWindow('close');
        }
    });
};

function onOpen(evt) {
    ontoLog("CONNECTED");
    SendPropertyChangeWithValue("viewId", "Other", "Other", view)
    if (idAttribute != undefined) {
        var idAttribute = getUrlParameter('IdAttribute');
        SendPropertyChangeWithValue("idAttribute", "Other", "Other", idAttribute)
    }

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}