﻿var module = 'EditOItems'
var controller = 'EditOItemController'
var moduleId = '771fd691cdd144378e911afcd006aedb'
var controllerId = '194d9007a08b4ed59ad5ba2c165ba140'
var view = '523c22bb4ea44b8da38259218e5c29fb'
var lblSave = 'Save';
var lblApply = 'Apply';

var dataAdapter
var columnConfig

function init() {

    initControls()
    testWebSocket(null, true);

}

function initControls() {
    

    var jqxWidget = $('#itemGrid');
    var offset = jqxWidget.offset();
    $('#saveWindow').jqxWindow({
        position: { x: offset.left + 50, y: offset.top + 50 },
        showCollapseButton: true, maxHeight: 400, maxWidth: 700, minHeight: 200, minWidth: 200, height: 300, width: 500,
        initContent: function () {
            $('#saveWindow').jqxWindow('focus');
        }
    });
    $('#saveWindow').hide();
    //$('#inpParent').jqxInput({ height: 25, width: 200, minLength: 1 });
    //$('#inpGuid').jqxInput({ height: 25, width: 200, minLength: 1 });
    //$('#listenClass').jqxToggleButton({ height: 25, width: 25 });
    //$("#newGuid").jqxButton({ width: 100, height: 25 });

}

function onMessage(evt) {

    console.log(evt.data);
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemId == 'Url_Menu') {
            isInitializingController = true;
            $.getJSON(viewItem.LastValue, function (data) {
                console.log('data:', data);
                $('#jqxMenu').createMenu(data, false);
            });
            isInitializingController = false;
        }
        else if (viewItem.ViewItemId == 'itemGrid' && viewItem.ViewItemType == 'DataSource') {

            var dataSource = JSON.parse(JSON.stringify(viewItem.LastValue))

            dataAdapter = createDataAdapter(dataSource)

            testCreateGrid(viewItem, dataAdapter, columnConfig)
        }
        else if (viewItem.ViewItemId == 'itemGrid' && viewItem.ViewItemType == 'ColumnConfig') {
            columnConfig = JSON.parse(JSON.stringify(viewItem.LastValue.ColumnList))

            testCreateGrid(viewItem, dataAdapter, columnConfig)

        }
        else if (viewItem.ViewItemId == 'itemGrid' && viewItem.ViewItemType == 'Content') {

            for (var jx in viewItem.LastValue) {
                var editItem = viewItem.LastValue[jx];
                if (!editItem.SaveAllowed) {
                    $('#' + viewItem.ViewItemId).jqxGrid('setcellvaluebyid', editItem.Guid, "Comment", "Save Not Allowed");
                }
                else if (editItem.Comment != '') {
                    $('#' + viewItem.ViewItemId).jqxGrid('setcellvaluebyid', editItem.Guid, "Comment", editItem.Comment);
                }
                else {
                    $('#' + viewItem.ViewItemId).jqxGrid('setcellvaluebyid', editItem.Guid, "Comment", '');
                }

            }

        }
        else if (viewItem.ViewItemId == 'EditItem_EditItemCreate') {
            var row = {};
            row["Save"] = viewItem.LastValue.Save;
            row["Guid"] = viewItem.LastValue.Guid;
            row["Name"] = viewItem.LastValue.Name;
            row["ItemType"] = viewItem.LastValue.ItemType;
            row["GuidParent"] = viewItem.LastValue.GuidParent;
            row["NameParent"] = viewItem.LastValue.NameParent;
            row["Comment"] = viewItem.LastValue.Comment;

            var commit = $("#itemGrid").jqxGrid('addrow', null, row);
        }
        else if (viewItem.ViewItemId == 'EditItem_EditItemDelete') {


            var commit = $("#itemGrid").jqxGrid('deleterow', viewItem.LastValue.Guid);
        }
        else if (viewItem.ViewItemId == 'listenItem') {
            if (viewItem.ViewItemType == 'Checked') {
                setViewItemChecked(viewItem);
            }
        }
        else if (viewItem.ViewItemId == 'message') {
            $('#' + viewItem.ViewItemId).html(viewItem.LastValue);
            $('#saveWindow').show();
        }
        else if (viewItem.ViewItemType == 'Visible') {
            setViewItemVisibility(viewItem)
        }
        else if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem)
        }
        else if (viewItem.ViewItemType == 'Content') {
            if (viewItem.ViewItemClass == 'DropDownList') {
                

                setViewItemContent(viewItem, 205, 25);
                $('#' + viewItem.ViewItemId).on('change', function (event) {
                    var args = event.args;
                    if (args) {
                        // index represents the item's index.                      
                        var index = args.index;
                        $('#' + viewItem.ViewItemId).sendStringListSelectedIndex(index)
                    }
                })

            }
            else {
                setViewItemContent(viewItem);
                if (viewItem.ViewItemId == 'applyItem') {
                    lblApply = viewItem.LastValue;
                }
                else if (viewItem.ViewItemId == 'saveItem') {
                    lblSave = viewItem.LastValue;
                }
            }




        }
        else if (viewItem.ViewItemType == 'SelectedIndex') {
            setSelectedIndex(viewItem);
        }
        else if (viewItem.ViewItemType == 'IconLabel') {
            setIconLabel(viewItem)

            $("#" + viewItem.ViewItemId).on('click', function () {
                var toggled = $("#" + viewItem.ViewItemId).jqxToggleButton('toggled');
                $("#" + viewItem.ViewItemId).sendIsChecked(toggled)
            });
        }
        else if (viewItem.ViewItemType == 'Checked') {
            setViewItemChecked(viewItem);
        }
        else if (viewItem.ViewItemId == 'exitMessage') {
            SetCloseMessage(viewItem);
        }
        else if (viewItem.ViewItemId == 'ModelSended') {
            SendStringCommandWithoutParameter('AckModelSended');
        }
        
    }

    //websocket.close();
}

$.fn.createGrid = function (dataAdapter, columnConfig) {
    var elementId = this[0].id

    $("#" + elementId).jqxGrid(
    {
        source: dataAdapter,
        width: "100%",
        height: 500,
        editable: true,
        showtoolbar: true,
        rendertoolbar: function (toolbar) {
            var me = this;
            var container = $("<div style='margin: 5px;'></div>");
            toolbar.append(container);

            container.append('<input id="addrowbutton" type="button" value="New Item" />');
            container.append('<input style="margin-left: 5px;" id="deleterowbutton" type="button" value="Delete Item" />');
            container.append('<input style="margin-left: 5px;" id="listenItem" type="button" value="Listen" />');
            container.append('<input style="margin-left: 5px;" id="listenParent" type="button" value="Listen Parent" />');
            container.append('<input style="margin-left: 5px;" id="inpParent" type="text" />');
            container.append('<input id="saveItem" type="button" value="' + lblSave + '"/>');
            container.append('<input id="applyItem" type="button" value="' + lblApply + '"/>');

            $("#addrowbutton").jqxButton();
            $("#deleterowbutton").jqxButton();
            $("#listenItem").jqxToggleButton();
            $("#listenParent").jqxToggleButton();
            $('#inpParent').jqxInput({ height: 25, width: 200, minLength: 1 });

            $("#saveItem").jqxButton({ width: 100, height: 25 });
            $("#saveItem").on('click', function () {
                SendStringCommandWithoutParameter('SaveItems')
            });

            $("#applyItem").jqxButton({ width: 100, height: 25 });
            $("#applyItem").on('click', function () {
                SendStringCommandWithoutParameter('ApplyItems')
            });
            $("#cancel").jqxButton({ width: 100, height: 25 });

            // create new row.
            $("#addrowbutton").on('click', function () {
                SendStringCommandWithoutParameter('AddItem')
            });

            // delete row.
            $("#deleterowbutton").on('click', function () {
                var selectedrowindex = $("#" + elementId).jqxGrid('getselectedrowindex');
                var selectedrowindex = $("#" + elementId).jqxGrid('getselectedrowindex');
                var rowscount = $("#" + elementId).jqxGrid('getdatainformation').rowscount;
                if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                    var id = $("#" + elementId).jqxGrid('getrowid', selectedrowindex);
                    SendStringCommand('RemoveRow', 'Guid', id)
                }

            });

            $("#listenItem").on('click', function () {
                var toggled = $("#listenItem").jqxToggleButton('toggled');
                var viewItem = new ViewItem();
                viewItem.ViewItemId = "listenItem";
                viewItem.ViewItemClass = "ToggleButton";
                viewItem.ViewItemType = "Checked";
                viewItem.ViewItemValue = toggled;
                SendViewItems([viewItem])
            });

            $("#listenParent").on('click', function () {
                var toggled = $("#listenParent").jqxToggleButton('toggled');
                var viewItem = new ViewItem();
                viewItem.ViewItemId = "listenParent";
                viewItem.ViewItemClass = "ToggleButton";
                viewItem.ViewItemType = "Checked";
                viewItem.ViewItemValue = toggled;
                SendViewItems([viewItem])
            });
        },
        columns: columnConfig
    });

    $("#" + elementId).show();


    SendStringCommandWithoutParameter('GridInitialized');
}

function testCreateGrid(viewItem, dataAdapter, columnConfig) {
    ontoLog(viewItem);
    ontoLog(dataAdapter);
    ontoLog(columnConfig);
    if (dataAdapter != undefined && columnConfig != undefined) {
        $('#' + viewItem.ViewItemId).createGrid(dataAdapter, columnConfig)
        $('#' + viewItem.ViewItemId).on('cellendedit', function (event) {
            var args = event.args
            var data = $('#' + viewItem.ViewItemId).jqxGrid('getrowdata', args.rowindex)

            var name = data['Name'];
            var listen = data['Listen'];

            if (args.datafield == "Name") {
                name = args.value
            }

            var editItem = {
                Guid: data['Guid'],
                Name: name,
                Comment: data['Comment']
            }

            ontoLog('editItem:' + JSON.stringify(editItem))
            SendPropertyChangeWithValue('EditItem_EditItemUpdate', '', '', editItem)
            //$("#cellendeditevent").text("Event Type: cellendedit, Column: " + args.datafield + ", Row: " + (1 + args.rowindex) + ", Value: " + args.value);
        });

        $('#' + viewItem.ViewItemId).on('rowselect', function (event) {
            // event arguments.
            var args = event.args;
            // row's bound index.
            var rowBoundIndex = args.rowindex;
            // row's data. The row's data object or null(when all rows are being selected or unselected with a single action). If you have a datafield called "firstName", to access the row's firstName, use var firstName = rowData.firstName;
            var rowData = args.row;

            SendStringCommand('SelectedRow', 'Guid', rowData['Guid'])
        });
    }
}

function onOpen(evt) {
    ontoLog("CONNECTED");

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}