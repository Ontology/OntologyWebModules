﻿var dataAdapter
var columnConfig

function init() {


    createControls()
    testWebSocket()

}

function createControls() {
    $("#passwordInp").jqxPasswordInput({ width: '300px', height: '20px', showStrength: true, showStrengthPosition: "right" });
    $("#baseUrlInp").jqxInput({ width: '300px', height: '20px' });
    $("#userNameInp").jqxInput({ width: '300px', height: '20px' });

    $("#buttonGetBlogs").jqxButton({ width: 100, height: 25 });
    $("#buttonGetBlogs").on('click', function () {
        SendStringCommandWithoutParameter('GetBlogs')
    });

    $("#buttonPostList").jqxButton({ width: 100, height: 25 });
    $("#buttonPostList").on('click', function () {
        SendStringCommandWithoutParameter('GetPosts')
    });

    $("#jqxLoader").jqxLoader({ isModal: true, width: 100, height: 60, imagePosition: 'top' });
}


function onMessage(evt) {

    console.log(evt.data);
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemId == 'jqxLoader') {
            if (viewItem.LastValue == true) {
                $('#' + viewItem.ViewItemId).jqxLoader('open');
            }
            else {
                $('#' + viewItem.ViewItemId).jqxLoader('close');
            }
        }
        else if (viewItem.ViewItemId == 'RefreshWebLogs') {
            var dataAdapterDropDown = $('#blogsDropdown').jqxDropDownList('source');
            dataAdapterDropDown.dataBind()
            $("#blogsDropdown").jqxDropDownList({ selectedIndex: 0 });
        }
        else if (viewItem.ViewItemId == 'RefreshPostGrid') {
            $("#gridBlogentries").jqxGrid('updatebounddata', 'cells');
        }
        else if (viewItem.ViewItemId == 'gridBlogentries' && viewItem.ViewItemType == 'DataSource') {

            var dataSource = JSON.parse(JSON.stringify(viewItem.LastValue))

            dataAdapter = createDataAdapter(dataSource)

            testCreateGrid(viewItem, dataAdapter, columnConfig)
        }
        else if (viewItem.ViewItemId == 'gridBlogentries' && viewItem.ViewItemType == 'ColumnConfig') {
            columnConfig = JSON.parse(JSON.stringify(viewItem.LastValue.ColumnList))

            testCreateGrid(viewItem, dataAdapter, columnConfig)

        }
        else if (viewItem.ViewItemClass == 'DropDownList' && viewItem.ViewItemType == 'DataSource') {
            //$('#' + viewItem.ViewItemId).jqxDropDownList('destroy');
            setDataAdapterSourceDropDownList(viewItem, JSON.stringify(viewItem.LastValue));

            $('#' + viewItem.ViewItemId).on('change', function (event) {
                var args = event.args;
                if (args) {
                    // index represents the item's index.     

                    var index = args.index;
                    ontoLog('selected:' + index);
                    $('#' + viewItem.ViewItemId).sendStringListSelectedIndex(index)
                }
            })
        }
        else if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemType == 'Content') {
            setViewItemContent(viewItem, 200, 25);
        }


    }




    //websocket.close();
}

function testCreateGrid(viewItem, dataAdapter, columnConfig) {
    if (dataAdapter != undefined && columnConfig != undefined) {
        $('#' + viewItem.ViewItemId).createGrid(dataAdapter, columnConfig)
        $('#' + viewItem.ViewItemId).on('rowselect', function (event) {
            // event arguments.
            var args = event.args;
            // row's bound index.
            var rowBoundIndex = args.rowindex;
            // row's data. The row's data object or null(when all rows are being selected or unselected with a single action). If you have a datafield called "firstName", to access the row's firstName, use var firstName = rowData.firstName;
            var rowData = args.row;

            SendStringCommand('SelectedRow', 'Guid', rowData['Guid'])
        });
    }
}

$.fn.createGrid = function (dataAdapter, columnConfig) {
    var elementId = this[0].id

    $("#" + elementId).jqxGrid(
    {
        source: dataAdapter,
        width: "100%",
        height: "100%",
        editable: false,
        columns: columnConfig
    });

    ontoLog('show Grid');
    $("#" + elementId).show();

    SendStringCommandWithoutParameter('GridInitialized');
}

function onOpen(evt) {
    ontoLog("CONNECTED");


    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}