﻿

function init() {

    $('#jqxToolBar').createOTreeBar()
    $('body').openPageLoader();
    ontoLog('init classTree');
    testWebSocket();

}


function onMessage(evt) {

    console.log(evt.data);
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemId == 'Url_TreeData') {
            $('#list').createOntoTree(viewItem.LastValue);
            //jQuery.getJSON(objJSON.Url_TreeData, function (data) {
            //    initTree(data);
            //});



        }
        else if (viewItem.ViewItemId == 'IsSuccessful_Login') {
            if (!viewItem.LastValue) {

                return;
            }

        }
        else if (viewItem.ViewItemId == 'PathNodes') {
            console.info('pathNodes: ' + viewItem.LastValue);
            var pathNodes = viewItem.LastValue;
            $('#jqxToolBar').configureOTreePath(pathNodes);
            $('#jqxToolBar').resizeToFit({ height: false, width: true });
        }
        else if (viewItem.ViewItemId == 'viewText') {
            document.title = viewItem.LastValue;
        }
        else if (viewItem.ViewItemId == commandShowLoader) {
            $('body').openPageLoader();
        }
        else if (viewItem.ViewItemId == commandHideLoader) {
            $('body').closePageLoader();
        }

    }




    //websocket.close();
}


function onOpen(evt) {
    ontoLog("CONNECTED");


    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}