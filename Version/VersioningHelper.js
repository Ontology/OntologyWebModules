﻿var dataAdapter;
var columnConfig;

function init() {

    initControls();
    testWebSocket();

}

function initControls() {
    $("#majorInput").jqxNumberInput({ width: '60px', height: '25px', inputMode: 'simple', spinButtons: true, decimalDigits: 0, min: 0 });
    $('#majorInput').on('change', function (event) {
        var value = event.args.value;
        SendPropertyChangeWithValue("majorInput", "Content", "NumericInput", value);
    });

    $("#minorInput").jqxNumberInput({ width: '60px', height: '25px', inputMode: 'simple', spinButtons: true, decimalDigits: 0, min: 0 });
    $('#minorInput').on('change', function (event) {
        var value = event.args.value;
        SendPropertyChangeWithValue("minorInput", "Content", "NumericInput", value);
    });

    $("#buildInput").jqxNumberInput({ width: '60px', height: '25px', inputMode: 'simple', spinButtons: true, decimalDigits: 0, min: 0 });
    $('#buildInput').on('change', function (event) {
        var value = event.args.value;
        SendPropertyChangeWithValue("buildInput", "Content", "NumericInput", value);
    });

    $("#revisionInput").jqxNumberInput({ width: '60px', height: '25px', inputMode: 'simple', spinButtons: true, decimalDigits: 0, min: 0 });
    $('#revisionInput').on('change', function (event) {
        var value = event.args.value;
        SendPropertyChangeWithValue("revisionInput", "Content", "NumericInput", value);
    });
                

    $('#ok').jqxButton();
    $('#ok').on('click', function (event) {
        SendStringCommandWithoutParameter('SaveVersion');
    });
    $('#clear').jqxButton();
    $('#clear').on('click', function (event) {
        SendStringCommandWithoutParameter('Clear');
    })

    $('#inpDescription').dblclick(function () {
        SendStringCommandWithoutParameter('ToggleReadonlyOfDescription');
    });

    $('#inpDescription').on('change', function (event) {
        var val = $('#inpDescription').val();
        SendPropertyChangeWithValue("inpDescription", "Content", "TextArea", val);
        
    });
}

function onMessage(evt) {

    var objJSON = eval("(function(){return " + evt.data + ";} )()");


    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        ontoLog(viewItem);

        if (viewItem.ViewItemId == 'IsSuccessful_Login') {
            if (!viewItem.LastValue) {

                return;
            }

        }
        else if (viewItem.ViewItemType == "Enable") {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemType == "Content") {
            setViewItemContent(viewItem);
        }
        else if (viewItem.ViewItemClass == "Combobox" && viewItem.ViewItemType == "DataSource") {
            $('#' + viewItem.ViewItemId).createCombo(viewItem.LastValue, "100%", 25);

            $("#" + viewItem.ViewItemId).on('select', function (event) {
                if (event.args) {
                    var item = event.args.item;
                    if (item) {
                        SendStringCommand("SelectedLogstate", 'Id', item.value);
                    }
                }
            });
        }
        else if (viewItem.ViewItemType == "Readonly") {
            setViewItemReadonlyState(viewItem);
            
        }
        else if (viewItem.ViewItemType == "Enable") {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemType == "Content") {
            setViewItemContent(viewItem);
        }
        else if (viewItem.ViewItemClass == "Combobox" && viewItem.ViewItemType == 'SelectedIndex') {
            setSelectedIndex(viewItem);
        }
    }



    //websocket.close();
}


function onOpen(evt) {
    ontoLog("CONNECTED");

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}



window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}