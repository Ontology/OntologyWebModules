﻿var dataAdapter;
var columnConfig;

function init() {

    initControls();
    testWebSocket();

}

function initControls() {
    $('#newItem').jqxButton({ width: 25, disabled: true });
    $('#removeItem').jqxButton({ width: 25, disabled: true });
}

function onMessage(evt) {

    var objJSON = eval("(function(){return " + evt.data + ";} )()");


    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        ontoLog(viewItem);

        if (viewItem.ViewItemId == 'IsSuccessful_Login') {
            if (!viewItem.LastValue) {

                return;
            }

        }
        else if (viewItem.ViewItemClass == 'ContextMenu' && viewItem.ViewItemType == 'Content') {
            $('#' + viewItem.ViewItemId).createMenu(viewItem.LastValue, true);
        }
        else if (viewItem.ViewItemClass == 'Grid' && viewItem.ViewItemType == 'DataSource') {
            var dataSource = JSON.parse(JSON.stringify(viewItem.LastValue))
            dataAdapter = createDataAdapter(dataSource)
            $('#' + viewItem.ViewItemId).createGrid(dataAdapter, columnConfig);
        }
        else if (viewItem.ViewItemClass == 'Grid' && viewItem.ViewItemType == 'ColumnConfig') {
            columnConfig = JSON.parse(JSON.stringify(viewItem.LastValue.ColumnList))

        }
        else if (viewItem.ViewItemClass == 'Grid' && viewItem.ViewItemType == 'Command') {
            if (viewItem.LastValue == 'Refresh') {
                dataAdapter.dataBind();
            }
        }
        else if (viewItem.ViewItemType == "Enable") {
            setViewItemEnableState(viewItem);
        }
    }



    //websocket.close();
}

$.fn.createGrid = function (dataAdapter, columnConfig) {
    var elementId = this[0].id
    $("#" + elementId).jqxGrid(
    {
        source: dataAdapter,
        width: "100%",
        height: "100%",
        pageable: true,
        filterable: true,
        sortable: true,
        altrows: true,
        enabletooltips: true,
        editable: true,
        autoshowfiltericon: true,
        showfilterrow: true,
        columnsresize: true,
        selectionmode: 'multiplerowsextended',
        columns: columnConfig
    });

    $('#' + elementId).show();

    //$("#" + elementId).on('mousedown', function (event) {
    //    var rightClick = isRightClick(event) || $.jqx.mobile.isTouchDevice();
    //    if (rightClick) {
    //        var scrollTop = $(window).scrollTop();
    //        var scrollLeft = $(window).scrollLeft();
    //        $('#contextMenu').jqxMenu('open', parseInt(event.clientX) + 5 + scrollLeft, parseInt(event.clientY) + 5 + scrollTop);
    //        return false;
    //    }
    //});

}

function onOpen(evt) {
    ontoLog("CONNECTED");


    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}