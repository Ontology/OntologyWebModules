﻿function init() {


    

    testWebSocket();
    $('#inpName').on('change', function () {
        var value = $('#inpName').val();
        SendPropertyChangeWithValue('inpName', 'Content', 'Input', value);
    });

    $('#btnSave').click(function () {
        SendStringCommandWithoutParameter('clicked.save')
    });
}



function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);

        if (viewItem.ViewItemType == 'Content') {
            setViewItemContent(viewItem);
            if (viewItem.ViewItemClass == 'KendoDropDownList') {
                $('#' + viewItem.ViewItemId).change(function (e) {
                    var value = $("#dropDownSourceTypes").val();
                    SendPropertyChangeWithValue(viewItem.ViewItemId, 'SelectedIndex', 'KendoDropDownList', value)
                });
            }
        }
        else if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemId == 'viewText') {
            document.title = viewItem.LastValue;
        }
        
    }

    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams();
    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}