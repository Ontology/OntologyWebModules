﻿function init() {

    initWindow();

    testWebSocket();

    
    initToolbar();
}

function initWindow() {
    $('#windowCreateLiteraturquelle').kendoWindow({
        width: '500px',
        height: '400px',
        title: 'Neue Literaturquelle',
        visible: false,
        actions: [
            "Refresh",
            "Minimize",
            "Maximize",
            "Close"
        ],
        close: function (e) {

        },
        refresh: function () {
            var iframe = windowContainer.find('iframe');
            iframe.attr('src', iframe.attr('src'));
        }
    })
}

function initToolbar() {
    $('#toolbar').kendoToolBar({
        items: [
                { type: "button", id: "listen", spriteCssClass: "fa fa-assistive-listening-systems", togglable: true },
                { type: "separator" },
                { type: "button", id: "addSource", spriteCssClass: "fa fa-plus-circle"}
        ],
        toggle: function (e) {
            console.log("toggle", e.target.text(), e.checked);
            var id = e.target.attr('id');

            if (id == 'listen') {
                SendStringCommandWithoutParameter('clicked.listen');
            }
            

        },
        click: function (e) {
            var id = e.target.attr('id');
            if (id == 'addSource') {
                SendStringCommandWithoutParameter('clicked.addSource');
            }
        }
    });

}

function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);

        if (viewItem.ViewItemType == 'DataSource' && viewItem.ViewItemClass == 'Grid') {
            console.info(viewItem.LastValue);
        //createGrid(viewItem.LastValue);

            var gridConfig = {
                gridConfig: viewItem.LastValue,
                dataBoundFunc: function (e) {
                    
                    
                },
                changeFunc: function (e) {
                    var cell = this.select(),
                        cellIndex = cell.index(),
                        column = this.columns[cellIndex],
                        dataSource = this.dataSource,
                        dataItem = dataSource.view()[cell.closest("tr").index()];

                    var sendItem = {
                        Uid: dataItem.uid,
                        Property: column.field,
                        LiteraturQuelle: dataItem

                    };

                    SendPropertyChangeWithValue("grid", "SelectedIndex", "Grid", sendItem);
                    
                },
                dataSourceChnageFunc: function (e) {
                    console.log(e);
                }
            }
            $('#grid').createKendoGrid(gridConfig);

        }
        else if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemClass == 'Input' && viewItem.ViewItemType == 'Content') {
            setViewItemContent(viewItem);
        }
        else if (viewItem.ViewItemClass == 'NumericInput' && viewItem.ViewItemType == 'Content') {
            var numerictextbox = $("#" + viewItem.ViewItemId).data("kendoNumericTextBox");

            numerictextbox.value(viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'isListen') {
            var toolbar = $("#toolbar").data("kendoToolBar");
            toolbar.toggle("#listen", viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'iframeCreateLiteraturquelle') {
            $('#' + viewItem.ViewItemId).attr('src', viewItem.LastValue);
            $('#windowCreateLiteraturquelle').data("kendoWindow").center().open()
        }
        else if (viewItem.ViewItemId == 'addedLiteraturQuelle') {
            $("#grid").data("kendoGrid").dataSource.add(viewItem.LastValue.LiteraturQuelle);
            $("#grid").data("kendoGrid").refresh();
        }
    }

    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams();
    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}