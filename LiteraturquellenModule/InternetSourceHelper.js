﻿

var toolsCount = 0;
var disableOpenUrl = true;

function init() {

    initControls();
    testWebSocket();

}

function initControls() {
    $("#panelbar").kendoPanelBar({
        expandMode: "single"
    });
    //$("#dateDownload").jqxDateTimeInput({ formatString: "dd.MM.yyyy HH:mm:ss", showTimeButton: true, width: 364 });
    
    $("#btnOpenUrl").kendoButton();
    $("#btnChangeUrl").kendoButton();
    $("#btnChangeAuthor").kendoButton();
    
    kendo.culture('de-DE');

    $("#dateDownload").kendoDateTimePicker({
        format: kendo.culture().calendar.patterns.G,
        change: function () {
            var value = this.value();
            
            SendPropertyChangeWithValue("dateDownload", "Content", "DateTime", value);
        }
    });


    $('#btnChangeUrl').click(function () {
        SendStringCommand('clicked.btnChangeUrl');
    });

    $('#btnChangeAuthor').click(function () {
        SendStringCommand('clicked.btnChangeAuthor');
    });

    $('#openUrlContainer').on('click', function () {
        var url = $('#inpUrl').val();
        window.open(url, '_blank');
    });
}

function onMessage(evt) {

    var objJSON = eval("(function(){return " + evt.data + ";} )()");


    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemType == 'Content') {
            setViewItemContent(viewItem);
        }
        else if (viewItem.ViewItemType == 'Content' && viewItem.ViewItemId == 'iframePDFViewer') {
            $('#' + viewItem.ViewItemId).attr('src', viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'iframeFileUpload') {
            $('#' + viewItem.ViewItemId).attr('src', viewItem.LastValue);
        }
        else if (viewItem.ViewItemType == 'Visible') {

            if (viewItem.LastValue) {
                $('#' + viewItem.ViewItemId).show();
            }
            else {
                $('#' + viewItem.ViewItemId).hide();
            }

        }
        else if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);

        }
        else if (viewItem.ViewItemId == 'IsSuccessful_Login') {
            if (!viewItem.LastValue) {

                return;
            }

        }
        else if (viewItem.ViewItemClass == 'ToggleButton' && viewItem.ViewItemType == 'Checked') {
            if (viewItem.LastValue) {
                $('#' + viewItem.ViewItemId).addClass('k-primary');
            }
            else {
                $('#' + viewItem.ViewItemId).removeClass('k-primary');
            }
        }
    }



    //websocket.close();
}

function onOpen(evt) {
    openDisconnectedWindow();
    ontoLog("CONNECTED");


    //myTree.load("tree.xml");
}


function onClose(evt) {
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}