﻿function init() {

    $('#toolbar').kendoToolBar({
        items: [
                { type: "button", id: "listen", spriteCssClass: "fa fa-assistive-listening-systems", togglable: true },
                { type: "separator" },
                { template: "<label>Name:</label>" },
                { template: '<input id="inpVideoSource" placeholder="Name" style="width:300px"/>' }
        ],
        toggle: function (e) {
            console.log("toggle", e.target.text(), e.checked);
            var id = e.target.attr('id');

            if (id == 'listen') {
                SendStringCommandWithoutParameter('clicked.listen');
            }
            else if (id == 'listenApplied') {
                SendStringCommandWithoutParameter('clicked.listenApplied');
            }

        }
    });
    
    $("#panelbar").kendoPanelBar({
        expandMode: "single"
    });

    $('#detailTabs').kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
    });
    testWebSocket();
   
}



function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);

       if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemId == 'viewText') {
            document.title = viewItem.LastValue;
        }
        else if (viewItem.ViewItemClass == 'IFrame' && viewItem.ViewItemType == 'Content') {
            $('#' + viewItem.ViewItemId).attr('src', viewItem.LastValue);

        }
        
    }

    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams();
    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}