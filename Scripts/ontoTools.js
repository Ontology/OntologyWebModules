﻿var toolTipTimer;

var dataTypes = {
    idDataTypeBool: "dd858f27d5e14363a5c33e561e432333",
    idDataTypeDateTime: "905fda81788f4e3d83293e55ae984b9e",
    idDataTypeInt: "3a4f5b7bda754980933efbc33cc51439",
    idDataTypeDouble: "a1244d0e187f46ee85742fc334077b7d",
    idDataTypeString: "64530b52d96c4df186fe183f44513450"
};

var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties, rowdata) {
    if (value < 20) {
        return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #ff0000;">' + value + '</span>';
    }
    else {
        return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #008000;">' + value + '</span>';
    }
}

var linkrenderer = function (row, column, value) {
    if (value.indexOf('#') != -1) {
        value = value.substring(0, value.indexOf('#'));
    }
    var format = { target: '"_blank"' };
    var html = $.jqx.dataFormat.formatlink(value, format);
    return html;
}

var exitMessage = 'Do you really want to leave?';
var closeMessageSet = false;
/**
 * Generates a GUID string.
 * @returns {String} The generated GUID.
 * @example af8a8416-6e18-a307-bd9c-f2c947bbb3aa
 * @author Slavik Meltser (slavik@meltser.info).
 * @link http://slavik.meltser.info/?p=142
 */
function guid() {
    function _p8(s) {
        var p = (Math.random().toString(16) + "000000000").substr(2, 8);
        return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
    }
    return _p8() + _p8(true) + _p8(true) + _p8();
}

function htmlDecode(input, dest) {
    var e = document.getElementById(dest);
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function getUrlParameters() {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    return sURLVariables;
}

function createMenu(element, dataSource) {


    var dataAdapter = new $.jqx.dataAdapter(dataSource);
    dataAdapter.dataBind();
    var records = dataAdapter.getRecordsHierarchy('Id', 'ParentId', 'items', [{ name: 'Name', map: 'label' }]);

    $("#" + element).jqxMenu({ width: '100%', height: '30px', source: records });
    $("#" + element).css('visibility', 'visible');

    $('#' + element).on('itemclick', function (event) {
        // get the clicked LI element.
        var element = event.args;
        var id = element.id;
        SendStringCommand('ClickedMenuItem', 'Id', id);
    });

    // disable the default browser's context menu.
    $(document).on(element, function (e) {
        return false;
    });
    function isRightClick(event) {
        var rightclick;
        if (!event) var event = window.event;
        if (event.which) rightclick = (event.which == 3);
        else if (event.button) rightclick = (event.button == 2);
        return rightclick;
    }

    for (var ix in dataSource.localdata) {
        var menuEntry = dataSource.localdata[ix];
        if (menuEntry.IsVisible) {
            $('#' + menuEntry.Id).show();
        }
        else {
            $('#' + menuEntry.Id).hide();
        }

        $('#' + element).jqxMenu('disable', menuEntry.Id, !menuEntry.IsEnabled);
    }
}

/**
* Detects the font of an element from the font-family css attribute by comparing the font widths on the element
* @link http://stackoverflow.com/questions/15664759/jquery-how-to-get-assigned-font-to-element
*/
(function ($) {
    $.fn.exists = function () {
        return this.length !== 0;
    }

    $.fn.resizeToFit = function (configuration) {

        if (configuration == undefined) {
            configuration = {
                width: true,
                height: true
            }
        }

        var $element = $(this);

        var position = $element.position();
        var possibleHeight = window.innerHeight;
        var possibleWidth = window.innerWidth;

        var width = possibleWidth - position.left - 15;
        var height = possibleHeight - position.top - 18;

        if (configuration.width) {
            $element.css('width', width + 'px');
        }
        
        if (configuration.height) {
            $element.css('height', height + 'px');
        }
        
    }

    $.fn.detectFont = function () {
        var fontfamily = $(this).css('font-family');
        var fonts = fontfamily.split(',');
        if (fonts.length == 1)
            return fonts[0];

        var element = $(this);
        var detectedFont = null;
        fonts.forEach(function (font) {
            var clone = $('<span>wwwwwwwwwwwwwwwlllllllliiiiii</span>').css({ 'font-family': fontfamily, 'font-size': '70px', 'display': 'inline', 'visibility': 'hidden' }).appendTo('body');
            var dummy = $('<span>wwwwwwwwwwwwwwwlllllllliiiiii</span>').css({ 'font-family': font, 'font-size': '70px', 'display': 'inline', 'visibility': 'hidden' }).appendTo('body');
            //console.log(clone, dummy, fonts, font, clone.width(), dummy.width());
            if (clone.width() == dummy.width())
                detectedFont = font;
            clone.remove();
            dummy.remove();
        });

        return detectedFont;
    }

    $.fn.addRow = function (rows) {
        
        var elementId = this[0].id;
        var value = $('#' + elementId).jqxGrid('addrow', null, rows);
    }

    $.fn.copyValToClipboard = function () {
        // create hidden text element, if it doesn't already exist
        var targetId = "_hiddenCopyText_";
        var isInput = this[0].tagName === "INPUT" || this.tagName === "TEXTAREA";
        var origSelectionStart, origSelectionEnd;
        if (isInput) {
            // can just use the original source element for the selection and copy
            target = this[0];
            origSelectionStart = this[0].selectionStart;
            origSelectionEnd = this[0].selectionEnd;
        } else {
            // must use a temporary form element for the selection and copy
            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = this[0].textContent;
        }
        // select the content
        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);

        // copy the selection
        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch (e) {
            succeed = false;
        }
        // restore original focus
        if (currentFocus && typeof currentFocus.focus === "function") {
            currentFocus.focus();
        }

        if (isInput) {
            // restore prior selection
            this[0].setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            // clear temporary content
            target.textContent = "";
        }
        ontoLog(succeed);
        return succeed;
    }

    $.fn.toggleListenMode = function (content, listen) {
        var elementId = this[0].id;

        if (listen) {
            $('#' + elementId).jqxButton( { value: content + " <span class='blink'><i class='fa fa-assistive-listening-systems' aria-hidden='true'></i></span>"});
                
        }
        else {
            $('#' + elementId).jqxButton({ value: content });
        }
        
    }

    $.fn.showWait = function () {
        var element = this[0];
        var id = 'wait' + element.id;
        var rect = element.getBoundingClientRect();
        var width = (rect.right - rect.left);
        var height = (rect.bottom - rect.top);


        var div = document.createElement('div');
        div.id = id;
        div.setAttribute('class', 'popupToolTipItem w3-animate-opacity');
        div.style.position = "fixed";
        div.style.left = rect.left.toString() + "px";
        div.style.top = rect.top.toString() + "px";
        div.style.width = width.toString() + "px";
        div.style.height = height.toString() + "px";
        div.style.zIndex = 999999;
        div.innerHTML = '<center>...</center>';
        div.style.opacity = '.8';
        div.style.background = 'white';

        document.body.appendChild(div);

        var childElement = div.firstChild;
        rect = childElement.getBoundingClientRect();
        var paddingLeft = (width / 2) - ((rect.right - rect.left) / 2);
        var paddingTop = (height / 2) - ((rect.bottom - rect.top) / 2);

        childElement.style.marginTop = paddingTop + "px";
        childElement.style.marginLeft = paddingLeft + "px";

        div.style.borderRadius = "5px";
        //div.style.width = width.toString() + "px";
        //div.style.height = height.toString() + "px";

    }

    $.fn.removeWait = function () {
        var element = this[0];
        var id = 'wait' + element.id;
        var element = document.getElementById(id);
        if (element != undefined) {
            element.parentElement.removeChild(element);
        }
    }

    $.fn.showToolTip = function (html, forecolor, backcolor) {
        var element = this[0];
        var rect = element.getBoundingClientRect();
        var width = (rect.right - rect.left);
        var height = (rect.bottom - rect.top);
        

        var div = document.createElement('div');
        div.setAttribute('class', 'popupToolTipItem w3-animate-opacity');
        div.style.position = "fixed";
        div.style.left = rect.left.toString() + "px";
        div.style.top = rect.top.toString() + "px";
        div.style.width = width.toString() + "px";
        div.style.height = height.toString() + "px";
        div.style.zIndex = 999999;
        div.innerHTML = html;
        div.style.color = forecolor;
        div.style.opacity = '.8';
        div.style.background = backcolor;

        document.body.appendChild(div);

        var childElement = div.firstChild;
        rect = childElement.getBoundingClientRect();
        var paddingLeft = (width / 2) - ((rect.right - rect.left) / 2);
        var paddingTop = (height / 2) - ((rect.bottom - rect.top) / 2);

        childElement.style.marginTop = paddingTop + "px";
        childElement.style.marginLeft = paddingLeft + "px";
        
        div.style.borderRadius = "5px";
        //div.style.width = width.toString() + "px";
        //div.style.height = height.toString() + "px";

        if (backcolor != undefined) {
            div.style.background = backcolor;
        }

        toolTipTimer = setTimeout(hideToolTips, 600);

        
    }
    $.fn.createMenu = function (dataSource, isContextMenu, itemCount) {
       
        var elementId = this[0].id
        var dataAdapter = new $.jqx.dataAdapter(dataSource);
        var height = itemCount * 30
        var sHeight = height + 'px'
        
        dataAdapter.dataBind();
        var records = dataAdapter.getRecordsHierarchy('Id', 'ParentId', 'items', [{ name: 'Name', map: 'label' }, { name: 'Id', map: 'id' }]);
        
        if (isContextMenu) {
            $("#" + elementId).jqxMenu({ width: '120px', height: sHeight, source: records, autoOpenPopup: false, mode: 'popup' });
        }
        else {
            $("#" + elementId).jqxMenu({ width: '120px', height: sHeight, source: records });
        }
        
        $("#" + elementId).css('visibility', 'visible');

        $('#' + elementId).on('itemclick', function (event) {
            // get the clicked LI element.
            var element = event.args;
            var id = element.id;
            SendStringCommand('ClickedMenuItem', 'Id', id);
        });

        // disable the default browser's context menu.
        if (isContextMenu) {
            $(document).on('contextmenu', function (e) {
                return false;
            });
        }
        

        for (var ix in dataSource.localdata) {
            var menuEntry = dataSource.localdata[ix];
            if (menuEntry.IsVisible) {
                $('#' + menuEntry.Id).show();
            }
            else {
                $('#' + menuEntry.Id).hide();
            }

            $('#' + elementId).jqxMenu('disable', menuEntry.Id, !menuEntry.IsEnabled);
        }

    }

    $.fn.createCombo = function (dataSource, width, height) {
        var elementId = this[0].id
        var dataAdapter = new $.jqx.dataAdapter(dataSource);
        ontoLog(dataSource);
        $("#" + elementId).jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Name", valueMember: "Id", width: width, height: height });
        
    }

    $.fn.createListBox = function (dataSource, width, height, isCheckable) {
        var elementId = this[0].id
        var dataAdapter = new $.jqx.dataAdapter(dataSource);
        ontoLog(dataSource);
        $("#" + elementId).jqxListBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Name", valueMember: "Id", width: width, height: height, checkboxes: isCheckable });

    }

    $.fn.removeRow = function (viewItem) {
        var elementId = this[0].id
        var result = $("#" + elementId).jqxGrid('deleterow', viewItem.LastValue);
        ontoLog(result);
    }

    $.fn.addORow = function (viewItem) {
        var elementId = this[0].id
        var row = { GUID: viewItem.LastValue.GUID, Name: viewItem.LastValue.Name };
        var result = $("#" + elementId).jqxGrid('addrow', null, row);
        ontoLog(result);
    }

    $.fn.createKendoGrid = function (config) {

        var grid = $(this);
        if (grid.hasClass('k-grid')) {
            grid.data("kendoGrid").destroy(); // destroy the Grid

            grid.empty(); // empty the Grid content (inner HTML)
        }

        if (config.dataBoundFunc != undefined) {
            config.gridConfig.dataBound = config.dataBoundFunc;
        }
        
        if (config.changeFunc != undefined) {
            config.gridConfig.change = config.changeFunc;
        }
        
        var dataSource = new kendo.data.DataSource({
            transport: config.gridConfig.dataSource.transport,
            change: config.dataSourceChnageFunc,
            pageSize: config.gridConfig.dataSource.pageSize
        });

        config.gridConfig.dataSource = dataSource;

        
        if (config.gridConfig.rowTemplate != undefined) {
            grid.kendoGrid({
                dataSource: config.gridConfig.dataSource,
                height: config.gridConfig.height,
                width: config.gridConfig.width,
                groupable: config.gridConfig.groupable,
                sortable: config.gridConfig.sortable,
                autoBind: config.gridConfig.autoBind,
                selectable: config.gridConfig.selectable,
                pageable: config.gridConfig.pageable,
                columns: config.gridConfig.columns,
                filterable: config.gridConfig.filterable,
                dataBound: config.gridConfig.dataBound,
                change: config.gridConfig.change,
                editable: config.gridConfig.editable,
                rowTemplate: kendo.template($("#" + config.gridConfig.rowTemplate).html())
            });
        } else {
            grid.kendoGrid({
                dataSource: config.gridConfig.dataSource,
                height: config.gridConfig.height,
                width: config.gridConfig.width,
                groupable: config.gridConfig.groupable,
                sortable: config.gridConfig.sortable,
                autoBind: config.gridConfig.autoBind,
                selectable: config.gridConfig.selectable,
                pageable: config.gridConfig.pageable,
                columns: config.gridConfig.columns,
                filterable: config.gridConfig.filterable,
                dataBound: config.gridConfig.dataBound,
                editable: config.gridConfig.editable,
                change: config.gridConfig.change
            });
        }

        

        setTimeout(function () {
            dataSource.read();
        }, 500);
    };

    $.fn.createGrid = function (dataAdapter, columnConfig, standardFilter, pagerrenderer, notPageable, loadCompleteFunction, selectionMode) {
        var elementId = this[0].id

        var pageable = true;
        var pageNum = 0;
        var pageSize = 20;

        if (selectionMode == undefined) {
            selectionMode = 'singlecell';
        }

        if (notPageable != undefined) {
            pageable = !notPageable;
        }
        if (pagerrenderer != undefined) {
            $("#" + elementId).jqxGrid(
            {
                source: dataAdapter,
                width: "100%",
                height: "100%",
                pageable: pageable,
                filterable: true,
                sortable: true,
                altrows: true,
                enabletooltips: true,
                editable: true,
                autoshowfiltericon: true,
                showfilterrow: true,
                columnsresize: true,
                pagerrenderer: pagerrenderer,
                selectionmode: selectionMode,
                columns: columnConfig,
                ready: loadCompleteFunction,
                pagesizeoptions: ['5', '10', '20', '50', '100', '500', '1000']
                
            });
        }
        else {
            $("#" + elementId).jqxGrid(
            {
                source: dataAdapter,
                width: "100%",
                height: "100%",
                pageable: pageable,
                filterable: true,
                sortable: true,
                altrows: true,
                enabletooltips: true,
                editable: true,
                autoshowfiltericon: true,
                showfilterrow: true,
                columnsresize: true,
                selectionmode: selectionMode,
                columns: columnConfig,
                ready: loadCompleteFunction,
                pagesizeoptions: ['5', '10', '20', '50', '100', '500', '1000']
                
            });
        }
        

        

       



        $("#" + elementId).show();
        initialized = true;
    }
    
})(jQuery);



function addGridFilter(gridId, dataItem, filterValue) {
    var filtergroup = new $.jqx.filter();
    // apply the filters.
    var filter_or_operator = 1;
    var filtercondition = 'equal';
    var filter1 = filtergroup.createfilter('stringfilter', filterValue, filtercondition);
    filtergroup.addfilter(filter_or_operator, filter1);
    $("#" + gridId).jqxGrid('addfilter', dataItem, filtergroup);
    $("#" + gridId).jqxGrid('applyfilters');

    //$("#grid").jqxGrid('clearfilters');
}

function testCreateGrid(viewItem, dataAdapter, columnConfig, idField, filterField, pagerrenderer, gridCompleteFunction) {
    if (dataAdapter != undefined && columnConfig != undefined) {
        $('#' + viewItem.ViewItemId).createGrid(dataAdapter, columnConfig, filterField, pagerrenderer, false, gridCompleteFunction)

        $('#' + viewItem.ViewItemId).on('rowselect', function (event) {

            idInstances = new Array
            // event arguments.
            //alert('done');

            var selectedRows = $('#grid').jqxGrid('getselectedrowindexes', args.rowindex);
            ontoLog(selectedRows);

            for (var ix in selectedRows) {
                var rowId = selectedRows[ix]
                var data = $('#grid').jqxGrid('getrowdata', rowId);
                if (isInstance) {
                    idInstances[ix] = data.IdInstance
                }
                else {
                    idInstances[ix] = data.IdItem
                }

            }
            if (isInstance) {
                SendStringCommand("SelectedObjectRows", idField + "s", idInstances);
            }
            else {
                SendStringCommand("SelectedObjectRows", idField + "s", idInstances);
            }

        });

        $('#' + viewItem.ViewItemId).on('rowunselect', function (event) {
            var args = event.args;
            var data = $('#grid').jqxGrid('getrowdata', args.rowindex);

            SendStringCommand("UnselectedObjectRow", idField, data.IdInstance);
        });

        $('#' + viewItem.ViewItemId).on('cellselect', function (event) {
            // event arguments.
            var args = event.args;
            // column data field.
            var datafield = event.args.datafield;
            // row's bound index.
            var rowBoundIndex = args.rowindex;
            // new cell value.
            var value = args.newvalue;
            // old cell value.
            var oldvalue = args.oldvalue;

            var data = $('#' + viewItem.ViewItemId).jqxGrid('getrowdata', rowBoundIndex);

            var sendData = JSON.stringify({ "Id": data.IdItem, "ColumnName": datafield, "RowId": rowBoundIndex });
            SendPropertyChangeWithValue(viewItem.ViewItemId, "SelectedIndex", "Grid", sendData);
        });

        $('#' + viewItem.ViewItemId).on('cellvaluechanged', function (event) {
            // event arguments.
            var args = event.args;
            // column data field.
            var datafield = event.args.datafield;
            // row's bound index.
            var rowBoundIndex = args.rowindex;
            // new cell value.
            var value = args.newvalue;
            // old cell value.
            var oldvalue = args.oldvalue;

            var data = $('#' + viewItem.ViewItemId).jqxGrid('getrowdata', rowBoundIndex);

            if (datafield == 'ItemApply') {
                if (value == true) {
                    SendStringCommand("AppliedObjectRow", idField, data.IdItem);
                }
                else {
                    SendStringCommand("UnAppliedObjectRow", idField, data.IdItem);
                }
            }
            ontoLog(args);
        });

        $('#' + viewItem.ViewItemId).on("celldoubleclick", function (event) {
            // event arguments.
            var args = event.args;
            // column data field.
            var datafield = event.args.datafield;
            // row's bound index.
            var rowBoundIndex = args.rowindex;
            // new cell value.
            var value = args.newvalue;
            // old cell value.
            var oldvalue = args.oldvalue;

            var data = $('#' + viewItem.ViewItemId).jqxGrid('getrowdata', rowBoundIndex);

            var sendData = JSON.stringify({ "Id": data.IdItem, "ColumnName": datafield, "RowId": rowBoundIndex });
            SendPropertyChangeWithValue(viewItem.ViewItemId, "DoubleClick", "Grid", sendData);

            ontoLog(args);
        });
    }
}

function isRightClick(event) {
    var rightclick;
    if (!event) var event = window.event;
    if (event.which) rightclick = (event.which == 3);
    else if (event.button) rightclick = (event.button == 2);
    return rightclick;
}

function setViewItemChecked(viewItem) {
    if (viewItem.ViewItemType == 'Checked') {
        if (viewItem.ViewItemClass == 'ToggleButton') {
            ontoLog('Checked:' + viewItem.LastValue)
            $('#' + viewItem.ViewItemId).jqxToggleButton({ toggled: viewItem.LastValue });
            
        }
        
    }
}

function createDataAdapter(dataSource) {
    dataAdapter = new $.jqx.dataAdapter(dataSource)
    return dataAdapter
}

function setViewItemVisibility(viewItem) {
    if (viewItem.ViewItemType == 'Visible') {

        viewItemId = viewItem.ViewItemId
        if (viewItemId == '@classItem@') {
            if (viewItem.LastValue == true) {
                $(viewItem.ViewItemClass).show()
            }
            else {
                $(viewItem.ViewItemClass).hide()
            }
        }
        else {
            if (viewItem.LastValue == true) {
                $('#' + viewItem.ViewItemId).show()
            }
            else {
                $('#' + viewItem.ViewItemId).hide()
            }
        }
        
    }
    
}

function setViewItemReadonlyState(viewItem) {

    $('#' + viewItem.ViewItemId).attr('readonly', viewItem.LastValue);
    if (!viewItem.LastValue) {
        $('#' + viewItem.ViewItemId).removeClass('readonly');
    }
    else {
        $('#' + viewItem.ViewItemId).addClass('readonly');

    }
    
}

function setViewItemEnableState(viewItem) {
    

    if ($('#' + viewItem.ViewItemId).hasClass('jqx-widget')) {
        setViewItemEnableStateJqx(viewItem);
    } else {
        $('#' + viewItem.ViewItemId).attr('readonly', !viewItem.LastValue);
        $('#' + viewItem.ViewItemId).attr('disabled', !viewItem.LastValue);
        if ($('#' + viewItem.ViewItemId).hasClass('k-button')) {
            var button = $('#' + viewItem.ViewItemId).data("kendoButton");
            button.enable(viewItem.LastValue);
        }
        else if (viewItem.ViewItemClass == 'NumericInput') {
            var numerictextbox = $('#' + viewItem.ViewItemId).data("kendoNumericTextBox");
            numerictextbox.enable(viewItem.LastValue);
        }
        else if (viewItem.ViewItemClass == 'DatePicker') {
            var datepicker = $('#' + viewItem.ViewItemId).data("kendoDatePicker");

            datepicker.enable(viewItem.LastValue);
        }
        else if (viewItem.ViewItemClass == 'DateTime') {
            var datetimepicker = $('#' + viewItem.ViewItemId).data("kendoDateTimePicker");

            datetimepicker.enable(viewItem.LastValue);
        }
    }
}
function setViewItemEnableStateJqx(viewItem) {
    var elementItem = document.getElementById(viewItem.ViewItemId)
    ontoLog('Element:' + elementItem);
    if (elementItem != undefined){
        if (viewItem.ViewItemType == 'Enable') {
            if (viewItem.ViewItemClass == 'Button') {
                $('#' + viewItem.ViewItemId).jqxButton({ disabled: !viewItem.LastValue });
            }
            else if (viewItem.ViewItemClass == 'Input') {
                $('#' + viewItem.ViewItemId).jqxInput({ disabled: !viewItem.LastValue });
            }
            else if (viewItem.ViewItemClass == 'DropDownList') {
                $('#' + viewItem.ViewItemId).jqxDropDownList({ disabled: !viewItem.LastValue });
            }
            else if (viewItem.ViewItemClass == 'ToggleButton') {
                $('#' + viewItem.ViewItemId).jqxToggleButton({ disabled: !viewItem.LastValue });
            }
            else if (viewItem.ViewItemClass == 'NumericInput') {
                $('#' + viewItem.ViewItemId).jqxNumberInput({ disabled: !viewItem.LastValue });
            }
            else if (viewItem.ViewItemClass == 'CheckBox') {
                $('#' + viewItem.ViewItemId).jqxCheckBox({ disabled: !viewItem.LastValue });
            }
            else if (viewItem.ViewItemClass == 'DateTimeInput') {
                $('#' + viewItem.ViewItemId).jqxCheckBox({ disabled: !viewItem.LastValue });
            }
            else if (viewItem.ViewItemClass == 'TextArea') {
                $('#' + viewItem.ViewItemId).prop("disalbed", !viewItem.LastValue);
            }
            else if (viewItem.ViewItemClass == 'ContextMenuEntry') {
                $('#' + viewItem.ViewItemId).jqxMenu('disable', viewItem.ViewItemId, !viewItem.LastValue);
            }
            else if (viewItem.ViewItemClass == 'Combobox') {
                $('#' + viewItem.ViewItemId).jqxComboBox({ disabled: !viewItem.LastValue });
            }
            else if (viewItem.ViewItemClass == 'ListBox') {
                $('#' + viewItem.ViewItemId).jqxListBox({ disabled: !viewItem.LastValue });
            }
            else if (viewItem.ViewItemClass == 'ToggleButton') {
                $('#' + viewItem.ViewItemId).jqxToggleButton({ disabled: !viewItem.LastValue });
            }
        }
    }
    
    
}

function SetCloseMessage(viewItem) {
    if (viewItem.ViewItemId == 'exitMessage') {
        exitMessage = viewItem.LastValue;
        if (exitMessage == undefined || exitMessage == '') {
            if (closeMessageSet) {
                $(window).unbind('beforeunload');
                closeMessageSet = false;
            }
            
        }
        else {
            if (!closeMessageSet)
            {
                closeMessageSet = true;
                $(window).bind('beforeunload', function () {
                
                    return exitMessage;
                });
            }
            
        }
        
   
    }
}

function setDataAdapterSourceDropDownList(viewItem, source) {
    
    var dataAdapterDropDown = new $.jqx.dataAdapter(JSON.parse(source));
    ontoLog('dataAdapter:' + dataAdapterDropDown);
    $("#" + viewItem.ViewItemId).jqxDropDownList({
        selectedIndex: 0, source: dataAdapterDropDown, displayMember: "Value", valueMember: "Id", width: 200, height: 25
    });
}
function setViewItemContentJqx(viewItem, withToSet, heightToSet) {
    if (viewItem.ViewItemId != '@classItem@') {
        
        var elementItem = document.getElementById(viewItem.ViewItemId)
        ontoLog('Element:' + elementItem);
        if (elementItem != undefined) {
            if (viewItem.ViewItemType == 'Content') {
                if (viewItem.ViewItemClass == 'Button' ||
                    viewItem.ViewItemClass == 'NumericInput' ||
                    viewItem.ViewItemClass == 'TextArea') {
                    $('#' + viewItem.ViewItemId).val(viewItem.LastValue);
                }
                else if (viewItem.ViewItemClass == 'DropDownList') {
                    var source = ''
                    for (var ix in viewItem.LastValue) {
                        var value = viewItem.LastValue[ix]
                        if (source != '') {
                            source += ','
                        }

                        source += '"' + value + '"'
                    }

                    source = '[' + source + ']'

                    if (withToSet == undefined) {
                        withToSet = '200'
                    }

                    if (heightToSet = undefined) {
                        heightToSet = '25'
                    }
                    ontoLog('with:' + withToSet)
                    $('#' + viewItem.ViewItemId).jqxDropDownList({ source: eval("(function(){return " + source + ";} )()"), selectedIndex: 1, width: withToSet, height: heightToSet });
                }
                else if (viewItem.ViewItemClass == 'Input') {
                    $('#' + viewItem.ViewItemId).val(viewItem.LastValue);
                }
                else if (viewItem.ViewItemClass == 'ToggleButton') {
                    $('#' + viewItem.ViewItemId).jqxToggleButton({ width: 100, height: 25 });
                    $('#' + viewItem.ViewItemId).val(viewItem.LastValue);
                }
                else if (viewItem.ViewItemClass == 'CheckBox') {
                    if (viewItem.LastValue == true) {
                        $('#' + viewItem.ViewItemId).jqxCheckBox({ checked: true });
                    }
                    else {
                        $('#' + viewItem.ViewItemId).jqxCheckBox({ checked: false });
                    }

                }
                else if (viewItem.ViewItemClass == 'DateTimeInput') {
                    $('#' + viewItem.ViewItemId).jqxDateTimeInput('val', new Date(viewItem.LastValue));
                }
                else if (viewItem.ViewItemClass == 'IFrame') {
                    $('#' + viewItem.ViewItemId).attr('src', viewItem.LastValue);
                }
                else {

                    $('#' + viewItem.ViewItemId).text(viewItem.LastValue);
                }

            }
            else if (viewItem.ViewItemType == 'Caption') {
                if (viewItem.ViewItemClass == 'CheckBox') {
                    $('#' + viewItem.ViewItemId).find('span')[1].innerHTML = viewItem.LastValue;
                }
            }
        }
    }
    else {
        if (viewItem.ViewItemClass == 'ItemIdentification') {
            $("." + viewItem.ViewItemClass).children(".idLabel").html("<b>" + viewItem.LastValue + "</b>");
        }
        else {
            $("." + viewItem.ViewItemClass).html(viewItem.LastValue);
        }
        
    }
}

function setViewItemContent(viewItem, withToSet, heightToSet) {
    if ($('#' + viewItem.ViewItemId).hasClass('jqx-widget')) {
        setViewItemContentJqx(viewItem, withToSet, heightToSet);
    }
    else {
        if (viewItem.ViewItemId != '@classItem@') {
        
            var elementItem = document.getElementById(viewItem.ViewItemId)
            ontoLog('Element:' + elementItem);
            if (elementItem != undefined) {
                if (viewItem.ViewItemType == 'Content') {
                    if (viewItem.ViewItemClass == 'Button' ||
                        viewItem.ViewItemClass == 'NumericInput' ||
                        viewItem.ViewItemClass == 'TextArea') {
                        $('#' + viewItem.ViewItemId).val(viewItem.LastValue);
                    }
                    else if (viewItem.ViewItemClass == 'DropDownList') {
                        
                    }
                    else if (viewItem.ViewItemClass == 'Input') {
                        $('#' + viewItem.ViewItemId).val(viewItem.LastValue);
                    }
                    else if (viewItem.ViewItemClass == 'ToggleButton') {
                        
                    }
                    else if (viewItem.ViewItemClass == 'CheckBox') {
                        

                    }
                    else if (viewItem.ViewItemClass == 'DateTimeInput') {
                        
                    }
                    else if (viewItem.ViewItemClass == 'IFrame') {
                        $('#' + viewItem.ViewItemId).attr('src', viewItem.LastValue);
                    }
                    else if (viewItem.ViewItemClass == 'KendoDropDownList') {
                        $('#' + viewItem.ViewItemId).kendoDropDownList(viewItem.LastValue);
                    }
                    else if (viewItem.ViewItemClass == 'DateTime') {
                        var value = new Date(viewItem.LastValue);
                        $('#' + viewItem.ViewItemId).data('kendoDateTimePicker').value(value);

                    }
                    else {

                        $('#' + viewItem.ViewItemId).text(viewItem.LastValue);
                    }

                }
                else if (viewItem.ViewItemType == 'Caption') {
                    if (viewItem.ViewItemClass == 'CheckBox') {
                        $('#' + viewItem.ViewItemId).find('span')[1].innerHTML = viewItem.LastValue;
                    }
                }
            }
        }
        else {
            if (viewItem.ViewItemClass == 'ItemIdentification') {
                $("." + viewItem.ViewItemClass).children(".idLabel").html("<b>" + viewItem.LastValue + "</b>");
            }
            else {
                $("." + viewItem.ViewItemClass).html(viewItem.LastValue);
            }
        
        }
    }
    
}

function changeCellOfGrid(viewItem) {
    $("#" + viewItem.ViewItemId).jqxGrid('setcellvalue', viewItem.LastValue.RowId, viewItem.LastValue.ColumnName, viewItem.LastValue.Value);
}

function setSelectedIndex(viewItem) {
    var elementItem = document.getElementById(viewItem.ViewItemId)
    ontoLog('Element:' + elementItem);
    if (elementItem != undefined) {
        if (viewItem.ViewItemType == 'SelectedIndex') {
            if (viewItem.ViewItemClass == 'DropDownList') {
                $("#" + viewItem.ViewItemId).jqxDropDownList({ selectedIndex: viewItem.LastValue });
            }
            else if (viewItem.ViewItemClass == 'Combobox') {
                $("#" + viewItem.ViewItemId).jqxComboBox('selectIndex', viewItem.LastValue);
            }
            
        }
        
    }
}

function checkListBoxIndex(viewItem) {
    if (viewItem.ViewItemClass == 'ListBox' && viewItem.ViewItemType == 'SelectedIndex') {
        for (var ix in viewItem.LastValue) {
            var index = viewItem.LastValue[ix];
            $("#" + viewItem.ViewItemId).jqxListBox('checkIndex', index);
        }
    }
}

function setIconLabel(viewItem) {
    var elementItem = document.getElementById(viewItem.ViewItemId)
    ontoLog('Element:' + elementItem);
    if (elementItem != undefined) {
        if (viewItem.ViewItemType == 'IconLabel') {
            if (viewItem.ViewItemClass == 'ToggleButton') {
                ontoLog('listenClass:' + viewItem.LastValue.FontIcon)
                var iconLabel = viewItem.LastValue
                if (iconLabel.Label != undefined && iconLabel.Label != '') {
                    $("#" + viewItem.ViewItemId).val(iconLabel.Label);
                }
                else if (iconLabel.FontIcon != undefined && iconLabel.FontIcon != '') {
                    $("#" + viewItem.ViewItemId).val("<i class='fa " + iconLabel.FontIcon + "' aria-hidden='true'></i>");
                }

                $("#" + viewItem.ViewItemId).jqxToggleButton({ width: '25', toggled: false });

            }
        }
    }
}

function initTree(element, source) {
    console.info(source);



    $('#' + element).jqxListMenu({ width: '100%', enableScrolling: false, showHeader: true, showBackButton: true, showFilter: false });

    var ul = $('#' + element);
    for (var ix in source) {
        var treeNode = source[ix];
        var li = document.createElement("li");
        li.id = treeNode.id;
        li.appendChild(document.createTextNode(treeNode.label));

        ul.append(li);

    }

    //$('#jqxTree').jqxTree({ source: source, width: '98%', height: '95vh' });
    //$('#jqxTree').on('select', function (event) {
    //    var args = event.args;
    //    var item = $('#jqxTree').jqxTree('getItem', args.element);
    //    treeSelectionChanged(item.id);
    //});
}

function measureText(canvas, text, font) {
    var ctx = canvas.getContext("2d");
    ctx.font = font;
    return ctx.measureText(text).width;
}

class ViewItem {
    constructor() {
        this.ViewItemId = '';
        this.ViewItemType = '';
        this.ViewItemClass = '';
        this.ViewItemValue = null;
    }

    
}

function hideToolTips() {
    var elements = document.getElementsByClassName('popupToolTipItem');
    if (elements.length > 0) {
        for (var ix in elements) {
            if (isElement(elements[ix])) {
                elements[ix].parentElement.removeChild(elements[ix]);
            }
            
        }
    }
    

    clearTimeout(toolTipTimer);
}

/**
     * PRODUCTION
 * Return true if object parameter is a DOM Element and false otherwise.
 *
 * @param {object} Object to test
 * @return {boolean}
 */
isElement = function (a) { try { return a.constructor.__proto__.prototype.constructor.name ? !0 : !1 } catch (b) { return !1 } };

$.fn.visibleHeight = function () {
    var elBottom, elTop, scrollBot, scrollTop, visibleBottom, visibleTop;
    scrollTop = $(window).scrollTop();
    scrollBot = scrollTop + $(window).height();
    elTop = this.offset().top;
    elBottom = elTop + this.outerHeight();
    visibleTop = elTop < scrollTop ? scrollTop : elTop;
    visibleBottom = elBottom > scrollBot ? scrollBot : elBottom;
    return visibleBottom - visibleTop
}