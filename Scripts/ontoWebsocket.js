var host = 'www.omodules.de'
var port = 6401
var sessionId
var noLog = false;
var isInitializingController = false;

function sendStandardParams() {
    var viewId = getUrlParameter("View");
    var classId = getUrlParameter("Class");
    var objectId = getUrlParameter("Object");
    var relationTypeId = getUrlParameter("RelationType");
    var attributeTypeId = getUrlParameter("AttributeType");

    if (viewId != undefined) {
        SendPropertyChangeWithValue("viewId", "Other", "Other", viewId);
    }
    if (classId != undefined) {
        SendPropertyChangeWithValue("classId", "Other", "Other", classId);
    }
    if (objectId != undefined) {
        SendPropertyChangeWithValue("objectId", "Other", "Other", objectId);
    }
    if (relationTypeId != undefined) {
        SendPropertyChangeWithValue("relationTypeId", "Other", "Other", relationTypeId);
    }
    if (attributeTypeId != undefined) {
        SendPropertyChangeWithValue("attributeTypeId", "Other", "Other", attributeTypeId);
    }
}

function testWebSocket(sessionIdToSet, sendItemType) {
    try {
        if (sessionIdToSet != undefined) {
            sessionId = sessionIdToSet
        }

        var wsUri = "wss://" + host + ":" + port + "/" + module + "." + controller + "?Module=" + moduleId + "&Controller=" + controllerId + "&Endpoint=" + guid();
        if (!noLog) ontoLog('initiUri: ' + wsUri);
        
        if (!noLog) ontoLog('sessionId:' + sessionId)

        if (sessionId == undefined || sessionId == null) {
            sessionId = getUrlParameter('Session');
        }
        
        if (sessionId == undefined || sessionId == null) {
            sessionId = getCookie('session')
        }

        if (sessionId == undefined || sessionId == null) {
            sessionId = guid();    
        }
        
        wsUri += "&Session=" + sessionId
        
        if (!noLog) ontoLog('uriWithSession: ' + wsUri);
        
        var itemType = getUrlParameter('ItemType');
        if (itemType == undefined) {
            itemType = 'Object';
        }


        var parentId = getUrlParameter('ParentId');
        if (parentId != undefined) {
            wsUri += "&ParentId=" + parentId
        }

        if (sendItemType != undefined || sendItemType == true) {
            wsUri += "&ItemType=" + itemType
        }

        if (view != undefined) {
            wsUri += "&View=" + view
        }

        var sender = getUrlParameter('Sender');
        if (sender != undefined) {
            wsUri += "&Sender=" + sender;
        }

        if (!noLog) ontoLog('uri: ' + wsUri);
        websocket = new WebSocket(wsUri);
        websocket.onopen = function (evt)
        {
            var params = getUrlParameters();
            SendPropertyChangeWithValue("parameters", "Other", "Other", params)
            onOpen(evt);
        };
        websocket.onclose = function (evt) { onClose(evt) };
        websocket.onmessage = function (evt) { onMessage(evt) };
        websocket.onerror = function (evt) { onError(evt) };
        if (!noLog) ontoLog('ws initialized');
    } catch (e) {
        ontoErrorLog(e.message)
    }


}



$.fn.sendStringListSelectedIndex = function (selectedIndex) {

    if (isInitializingController) return;

    viewItem = {
        ViewItemId: this[0].id,
        ViewItemType: 'SelectedIndex',
        ViewItemClass: 'StringList',
        ViewItemValue: selectedIndex
    }
    var jsonSend = { MessageType: "ViewItem", Items: [viewItem] }
    websocket.send(JSON.stringify(jsonSend))

}

$.fn.sendIsChecked = function (isToggled) {
    if (isInitializingController) return;

    viewItem = {
        ViewItemId: this[0].id,
        ViewItemType: 'Checked',
        ViewItemClass: 'ToggleButton',
        ViewItemValue: isToggled
    }
    var jsonSend = { MessageType: "ViewItem", Items: [viewItem] }
    websocket.send(JSON.stringify(jsonSend))

}

function SendViewItems(viewItems) {
    if (isInitializingController) return;

    var jsonSend = { MessageType: "ViewItem", Items: viewItems }
    websocket.send(JSON.stringify(jsonSend))
}

function SendStringPropertyChange(propertyName) {
    if (isInitializingController) return;

    if (!noLog) ontoLog('send Property: ' + propertyName)
    websocket.send("{ \"MessageType\": \"Change\", \"PropertyName\": \"" + propertyName + "\", \"" + propertyName + "\" : \"" + $('#' + propertyName).val() + "\"}");
}

function SendPropertyChangeWithValue(propertyName, viewItemType, viewItemClass, value) {
    if (isInitializingController) return;

    if (!noLog) ontoLog("Send: " + propertyName);
    viewItem = {
        ViewItemId: propertyName,
        ViewItemType: viewItemType,
        ViewItemClass: viewItemClass,
        ViewItemValue: value
    }
    var jsonSend = { MessageType: "ViewItem", Items: [viewItem] }
    websocket.send(JSON.stringify(jsonSend))
}

function SendStringPropertyChangeWithValue(propertyName, value) {
    if (isInitializingController) return;

    if (!noLog) ontoLog('send Property with Valuel: ' + propertyName)
    websocket.send("{ \"MessageType\": \"Change\", \"PropertyName\": \"" + propertyName + "\", \"" + propertyName + "\" : \"" + value + "\"}");
}

function SendStringEvent(eventName) {
    if (isInitializingController) return;

    websocket.send("{ \"MessageType\": \"Event\", \"Event\": \"" + eventName + "\"}");
}

function SendStringCommand(command, paramName, param) {
    if (isInitializingController) return;

    if (!noLog) ontoLog('send command with parameter: ' + command)
    websocket.send("{ \"MessageType\": \"Command\", \"Command\": \"" + command + "\",\"" + paramName + "\":\"" + param + "\"}");
}

function SendStringCommandWithObjectParam(command, paramName, param) {
    if (isInitializingController) return;

    if (!noLog) ontoLog('send command with parameter: ' + command)
    var send = "{ \"MessageType\": \"Command\", \"Command\": \"" + command + "\",\"" + paramName + "\":" + param + "}";
    websocket.send(send);
}

function SendStringCommandWithoutParameter(command) {
    if (isInitializingController) return;

    if (!noLog) ontoLog('send command without parameter: ' + command)
    websocket.send("{ \"MessageType\": \"Command\", \"Command\": \"" + command + "\" }");
}



