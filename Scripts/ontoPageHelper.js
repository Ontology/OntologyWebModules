﻿var ctrlFlag;
var disconnectWindow;

function sendCookie() {
    var cookieText = document.cookie;
   
    if (cookieText != undefined && cookieText != null && cookieText != '') {
        ontoLog("send Cookie:" + cookieText)
        SendStringPropertyChangeWithValue("Text_Cookie", cookieText);
        ontoLog("sended Cookie")
    }
   

}

(function ($) {
    $.fn.createPageLoaderWindow = function () {
        var elementId = this[0].id
        
        $('#' + elementId).jqxWindow({
            position: { x: 100, y: 100 },
            showCollapseButton: false, maxHeight: 500, maxWidth: 500, minHeight: 500, minWidth: 500, height: 500, width: 500,
            initContent: function () {
                $('#' + elementId).jqxWindow('focus');
                $('#' + elementId).jqxWindow({ isModal: true });
                $('#jqxLoader').jqxLoader({ width: 100, height: 60, imagePosition: 'top' });
                
            }
        });
    }

    $.fn.openPageLoader = function () {
        kendo.ui.progress($(this), true);
    }

    $.fn.closePageLoader = function () {
        kendo.ui.progress($(this), false);
    }
    
})(jQuery);

function openDisconnectedWindow() {
    disconnectWindow = $('<div></div>');
    $('body').append(disconnectWindow);
    disconnectWindow.kendoWindow({
        title: "Disconnected",
        content: {
            template: 'The page is disconnected <button onClick="javascript:location.reload()">reload</button>'
        },
        width: 300,
        height: 160,
        modal: true
        
    }).data("kendoWindow").center().open();

}

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

// Listen to keyboard. 
window.onkeydown = listenToKeyDown;
window.onkeyup = listenToKeyUp;


function listenToKeyUp(e) {
    if (e.keyCode == 17)
        ctrlFlag = 0;
 
}

function listenToKeyDown(e) {
    if (e.keyCode == 17)
        ctrlFlag = 1;

}

var commandShowLoader = "412bb1d7e7494f6f9bb47842704bc1dc";
var commandHideLoader = "f2215017146a4cd788f98a34945d296f";