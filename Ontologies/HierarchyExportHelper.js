﻿function init() {


    testWebSocket();

    $("#exportHierarchy").kendoButton({
        enable: false
    });

    $("#exportHierarchy").click(function () {
        SendStringCommand('clicked.exportHierarchy');
    });

    $('#isListenClassLeft').click(function () {
        SendStringCommand('clicked.isListenClassLeft');
    });

    $('#isListenRelationType').click(function () {
        SendStringCommand('clicked.isListenRelationType');
    });

    $('#isListenClassRight').click(function () {
        SendStringCommand('clicked.isListenClassRight');
    });

    $('#isListenDirection').click(function () {
        SendStringCommand('clicked.isListenDirection');
    });

    $('#isListenFilterRootObject').click(function () {
        SendStringCommand('clicked.isListenFilterRootObject');
    });

    $('#inpParamId').change(function () {
        SendPropertyChangeWithValue('inpParamId', 'Content', 'Input', $('#inpParamId').val());
    });

    $('#inpParamName').change(function () {
        SendPropertyChangeWithValue('inpParamName', 'Content', 'Input', $('#inpParamName').val());
    });

    $('#inpParamIdClass').change(function () {
        SendPropertyChangeWithValue('inpParamIdClass', 'Content', 'Input', $('#inpParamIdClass').val());
    });

    $('#inpParamNameClass').change(function () {
        SendPropertyChangeWithValue('inpParamNameClass', 'Content', 'Input', $('#inpParamNameClass').val());
    });

    $('#inpParamSubItems').change(function () {
        SendPropertyChangeWithValue('inpParamSubItems', 'Content', 'Input', $('#inpParamSubItems').val());
    });

}

function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);

        if (viewItem.ViewItemClass == 'Input') {
            setViewItemContent(viewItem);
        }
        if (viewItem.ViewItemId == 'viewText') {
            document.title = viewItem.LastValue;
        }
        if (viewItem.ViewItemId == 'isListenClassLeft' ||
            viewItem.ViewItemId == 'isListenRelationType' ||
            viewItem.ViewItemId == 'isListenClassRight' ||
            viewItem.ViewItemId == 'isListenDirection' ||
            viewItem.ViewItemId == 'isListenFilterRootObject') {
            setButtonIcon(viewItem);
        }
        if (viewItem.ViewItemId == 'exportHierarchy') {
            $('#' + viewItem.ViewItemId).data("kendoButton").enable(viewItem.LastValue);
        }
        if (viewItem.ViewItemId == 'linkDiv') {
            $('#' + viewItem.ViewItemId).html('<a href="' + viewItem.LastValue + '">Hierarchical File</a>');
        }
    }

    //websocket.close();
}

function setButtonIcon(viewItem) {
    if (viewItem.LastValue == true) {
        $('#' + viewItem.ViewItemId).html('<i class="fa fa-assistive-listening-systems" aria-hidden="true"></i>')
        $('#' + viewItem.ViewItemId).removeClass('k-default');
        $('#' + viewItem.ViewItemId).addClass('k-primary');
    }
    else {
        $('#' + viewItem.ViewItemId).html('<i class="fa fa-circle-thin" aria-hidden="true"></i>')
        $('#' + viewItem.ViewItemId).removeClass('k-primary');
        $('#' + viewItem.ViewItemId).addClass('k-default');
        
    }
    
}

function onOpen(evt) {
    ontoLog("CONNECTED");

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}