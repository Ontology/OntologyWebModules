﻿var dataAdapter
var columnConfig

function init() {


    output = document.getElementById("output");
    testWebSocket();

    
    $('#applyItem').jqxButton({ width: 30, height: 30 })
    $('#applyItem').on('click', function () {
        SendStringCommandWithoutParameter('AppliedObjectRows')
    });

    $('#newItem').jqxButton({ width: 30, height: 30 })
    $('#newItem').on('click', function () {
        SendStringCommandWithoutParameter('NewItem')
    });

    $('#deleteItem').jqxButton({ width: 30, height: 30 })
    $('#deleteItem').on('click', function () {
        SendStringCommandWithoutParameter('NewItem')
    });

    $('#isListen').jqxToggleButton({ width: 30, height: 30 });
    $('#isListen').on('click', function () {
        SendPropertyChangeWithValue('isListen', 'Other', 'Other', $("#isListen").jqxToggleButton('toggled'));
    });

    $('#downloadConfigFiles').jqxButton({ width: 30, height: 30 })
    $('#downloadConfigFiles').on('click', function () {
        SendStringCommandWithoutParameter('DownloadConfigItems')
    });
    
    

    createNewItemWindow();
    createDeleteWindow();
    
}

//Creating the demo window
function createNewItemWindow() {
    var jqxWidget = $('#grid');
    var offset = jqxWidget.offset();
    $('#newItemWindow').jqxWindow({
        position: { x: 100, y: 100 },
        showCollapseButton: true, maxHeight: 700, maxWidth: 1224, minHeight: 600, minWidth: 1024, height: 700, width: 1224,
        isModal: true,
        initContent: function () {
            $('#newItemWindow').jqxWindow('focus');
        }
    });

   

    $('#newItemWindow').jqxWindow('close');
};

function createEditNameWindow() {
    var jqxWidget = $('#grid');
    var offset = jqxWidget.offset();
    $('#editNameItemWindow').jqxWindow({
        position: { x: offset.left, y: offset.top },
        showCollapseButton: true, maxHeight: 1400, maxWidth: 1400, minHeight: 100, minWidth: 200, height: 136, width: 420,
        initContent: function () {
            $('#editNameItemWindow').jqxWindow('close');
        }
    });

    

    $('#editNameItemWindow').jqxWindow('close');
};

function createEditOrderIdWindow() {
    var jqxWidget = $('#grid');
    var offset = jqxWidget.offset();
    $('#editOrderIdItemWindow').jqxWindow({
        position: { x: offset.left, y: offset.top },
        showCollapseButton: true, maxHeight: 1400, maxWidth: 1400, minHeight: 100, minWidth: 200, height: 138, width: 391,
        initContent: function () {
            $('#editOrderIdItemWindow').jqxWindow('close');
        }
    });

    

    $('#editOrderIdItemWindow').jqxWindow('close');
};

//Creating the demo window
function createDeleteWindow() {
    $('#deleteWindow').jqxWindow({
        position: { x: 100, y: 100 },
        showCollapseButton: true, maxHeight: 600, maxWidth: 1024, minHeight: 600, minWidth: 1024, height: 600, width: 1024,
        isModal: true,
        initContent: function () {
            $('#deleteWindow').jqxWindow('focus');
        }
    });
    

    
    $('#deleteWindow').jqxWindow('close');
};

function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);
        if (viewItem.ViewItemType == 'DataSource' && viewItem.ViewItemClass == 'Grid') {
            console.info(viewItem.LastValue);
            //createGrid(viewItem.LastValue);

            var dataSource = JSON.parse(JSON.stringify(viewItem.LastValue))

            dataAdapter = createDataAdapter(dataSource)

            if (dataAdapter != undefined && columnConfig != undefined) {

                $('#grid').createGrid(dataAdapter, columnConfig, "Item");
                $('#grid').on('cellselect', function (event) {
                    // event arguments.
                    var args = event.args;
                    // column data field.
                    var datafield = event.args.datafield;
                    // row's bound index.
                    var rowBoundIndex = args.rowindex;
                    // new cell value.
                    var value = args.newvalue;
                    // old cell value.
                    var oldvalue = args.oldvalue;

                    var data = $('#' + viewItem.ViewItemId).jqxGrid('getrowdata', rowBoundIndex);

                    var sendData = JSON.stringify({ "Id": data.RowId, "ColumnName": datafield, "RowId": rowBoundIndex });
                    SendPropertyChangeWithValue(viewItem.ViewItemId, "SelectedIndex", "Grid", sendData);
                });
                $('#' + viewItem.ViewItemId).on('cellvaluechanged', function (event) {
                    // event arguments.
                    var args = event.args;
                    // column data field.
                    var datafield = event.args.datafield;
                    // row's bound index.
                    var rowBoundIndex = args.rowindex;
                    // new cell value.
                    var value = args.newvalue;
                    // old cell value.
                    var oldvalue = args.oldvalue;

                    var data = $('#' + viewItem.ViewItemId).jqxGrid('getrowdata', rowBoundIndex);

                    if (datafield == 'ItemApply') {
                        if (value == true) {
                            SendStringCommand("AppliedObjectRow", "Id", data.RowId);
                        }
                        else {
                            SendStringCommand("UnAppliedObjectRow", "Id", data.RowId);
                        }
                    }
                    ontoLog(args);
                });
            }

        }
        else if (viewItem.ViewItemId == 'isListen') {
            if (viewItem.LastValue) {
                $('#isListen').jqxToggleButton('check');
            }
            else {
                $('#isListen').jqxToggleButton('unCheck');
            }
        }
        else if (viewItem.ViewItemType == 'ColumnConfig' && viewItem.ViewItemClass == 'Grid') {
            columnConfig = JSON.parse(JSON.stringify(viewItem.LastValue.ColumnList))
        }
        else if (viewItem.ViewItemId == 'reloadGrid') {
            dataAdapter.dataBind();
        }
        else if (viewItem.ViewItemId == 'Url_NewItem') {
            if (viewItem.LastValue != undefined && viewItem.LastValue != '') {
                $('#newFrame').attr('src', viewItem.LastValue)
                $('#newItemWindow').jqxWindow('open');
            }

        }
        else if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemType == 'Content') {
            setViewItemContent(viewItem);
        }
        else if (viewItem.ViewItemClass == 'Grid' && viewItem.ViewItemType == 'Change') {
            changeCellOfGrid(viewItem);
        }
        else if (viewItem.ViewItemClass == 'Grid' && viewItem.ViewItemType == 'AddRow') {
            $('#' + viewItem.ViewItemId).addRow(viewItem.ViewItemValue);
        }
        else if (viewItem.ViewItemId == 'viewText') {
            document.title = viewItem.LastValue;
        }
    }

    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}