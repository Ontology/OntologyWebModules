﻿var dataAdapter;
var columnConfig;
var countdownSec;
var lockedPassword = true;
var windowHeader = "";

function init() {

    initControls();
    testWebSocket();

}

function initControls() {

    $("#inpUser").jqxInput({ placeHolder: "Choose a User", height: 25, width: 200, minLength: 1 });
    $('#inpUser').jqxInput({ disabled: true })
    $("#inpPassword").jqxInput({ placeHolder: "Enter a Password", height: 25, width: 200, minLength: 1 });
    $("#inpPasswordRepeat").jqxInput({ placeHolder: "Repeat the Password", height: 25, width: 200, minLength: 1 });

    $("#listenUser").jqxToggleButton({ width: 40, height: 27 });
    $("#removeUser").jqxButton({ width: 40, height: 27 });
    $("#showPassword").jqxButton({ width: 40, height: 27 });
    $("#showPassword").on('click', function () {
        if ($("#showPassword").jqxButton('disabled')) return;
        SendStringCommandWithoutParameter("ClickedShowPassword")
    });
    $("#cpyPassword").jqxButton({ width: 40, height: 27 });
    $("#cpyPassword").on('click', function () {
        if ($("#cpyPassword").jqxButton('disabled')) return;
        $("#inpPassword").copyValToClipboard();
    });

    _createWindow()
}

function _createWindow() {
    var jqxWidget = $('#grid');

    var offset = jqxWidget.offset();
    $('#window').jqxWindow({
        position: { x: offset.left, y: offset.top },
        showCollapseButton: true, maxHeight: 200, maxWidth: 600, minHeight: 100, minWidth: 200, height: 128, width: 320,
        initContent: function () {
            $('#window').jqxWindow('close');
        }
    });
};

function onMessage(evt) {

    var objJSON = eval("(function(){return " + evt.data + ";} )()");


    for (var ix in objJSON) {

        var viewItem = objJSON[ix];

        ontoLog(viewItem)

        if (viewItem.ViewItemId == 'IsSuccessful_Login') {
            if (!viewItem.LastValue) {

                return;
            }

        }
        else if (viewItem.ViewItemClass == 'ContextMenu' && viewItem.ViewItemType == 'Content') {
            $('#' + viewItem.ViewItemId).createMenu(viewItem.LastValue, true);
            $("#grid").on('mousedown', function (event) {

                var rightClick = isRightClick(event) || $.jqx.mobile.isTouchDevice();
                if (rightClick) {

                    var scrollTop = $(window).scrollTop();
                    var scrollLeft = $(window).scrollLeft();
                    $('#' + viewItem.ViewItemId).jqxMenu('open', parseInt(event.clientX) + 5 + scrollLeft, parseInt(event.clientY) + 5 + scrollTop);
                    ontoLog($('#' + viewItem.ViewItemId));
                    return false;
                }
            });
        }
        else if (viewItem.ViewItemClass == 'Grid' && viewItem.ViewItemType == 'DataSource') {
            var dataSource = JSON.parse(JSON.stringify(viewItem.LastValue))
            dataAdapter = createDataAdapter(dataSource)
            $('#' + viewItem.ViewItemId).createGrid(dataAdapter, columnConfig);

            $('#' + viewItem.ViewItemId).on('rowselect', function (event) {

                idInstances = new Array
                // event arguments.
                //alert('done');

                var selectedRows = $('#' + viewItem.ViewItemId).jqxGrid('getselectedrowindexes', args.rowindex);
                ontoLog(selectedRows);

                for (var ix in selectedRows) {
                    var rowId = selectedRows[ix]
                    var data = $('#grid').jqxGrid('getrowdata', rowId);
                    idInstances[ix] = data.Id
                }

                SendStringCommand("SelectedObjectRows", "IdItems", idInstances);
            });
        }
        else if (viewItem.ViewItemClass == 'Grid' && viewItem.ViewItemType == 'ColumnConfig') {
            columnConfig = JSON.parse(JSON.stringify(viewItem.LastValue.ColumnList))

        }
        else if (viewItem.ViewItemClass == 'ContextMenuEntry' && viewItem.ViewItemType == 'Change') {

            var contextMenuEntry = viewItem.LastValue;
            if (contextMenuEntry.IsVisible) {
                $('#' + contextMenuEntry.Id).show()
            }
            else {
                $('#' + contextMenuEntry.Id).hide()
            }
            $('#' + viewItem.ViewItemId).jqxMenu('disable', contextMenuEntry.Id, !contextMenuEntry.IsEnabled)

        }
        else if (viewItem.ViewItemId == 'passwordItemToEdit') {

            windowHeader = viewItem.LastValue.NameRef;
            $('#labelReference').html(windowHeader);

            if (viewItem.LastValue.PasswordMask != undefined) {
                $('#inpPassword').val(viewItem.LastValue.PasswordMask)
                if (lockedPassword) {
                    $("#inpPassword").copyValToClipboard();
                }
            }


            if (viewItem.LastValue.NameUser != undefined) {
                $('#inpUser').val(viewItem.LastValue.NameUser)
            }


            $('#window').jqxWindow('open');
        }
        else if (viewItem.ViewItemType == 'Enable') {
            ontoLog('Enable')
            setViewItemEnableState(viewItem)
        }
        else if (viewItem.ViewItemType == 'Visible') {
            ontoLog('Visible')
            setViewItemVisibility(viewItem)
        }
        else if (viewItem.ViewItemId == 'showPassword' && viewItem.ViewItemType == 'IconLabel') {
            $("#showPassword").jqxButton({ value: viewItem.LastValue });
        }
        else if (viewItem.ViewItemId == 'showPassword' && viewItem.ViewItemType == 'Checked') {
            lockedPassword = !viewItem.LastValue;
            if (viewItem.LastValue) {
                countdownSec = 20;
                setTimeout(countDown, 1000);
            }
            else {
                $('#labelReference').html(windowHeader);
            }
        }
    }




    //websocket.close();
}

function countDown() {
    countdownSec -= 1;


    if (lockedPassword) {
        $('#labelReference').html(windowHeader);
        return;
    }

    $('#labelReference').html(windowHeader + ' (' + countdownSec + ' s)');

    if (countdownSec == 0) {
        SendStringCommandWithoutParameter("ClickedShowPassword")
    }
    else {
        setTimeout(countDown, 1000);
    }
}

function directCopy(str) {

    //based on http://stackoverflow.com/a/12693636
    document.oncopy = function (event) {
        ontoLog(str)
        event.clipboardData.setData("Text", str);
        event.preventDefault();
    };
    ontoLog(str)
    document.execCommand("Copy");
    document.oncopy = undefined;
}

function clearClipboard() {

    //var isIe = (navigator.userAgent.toLowerCase().indexOf("msie") != -1
    //            || navigator.userAgent.toLowerCase().indexOf("trident") != -1);


    //if (isIe) {
    //    window.clipboardData.setData('Text', "");
    //} else {
    //    $('#password').val("")
    //    var passwordField = document.querySelector('#password');
    //    passwordField.select();

    //    document.execCommand('copy');
    //}
}

$.fn.createGrid = function (dataAdapter, columnConfig) {
    var elementId = this[0].id
    $("#" + elementId).jqxGrid(
    {
        source: dataAdapter,
        width: "100%",
        height: "100%",
        pageable: true,
        filterable: true,
        sortable: true,
        altrows: true,
        enabletooltips: true,
        editable: true,
        autoshowfiltericon: true,
        showfilterrow: true,
        columnsresize: true,
        selectionmode: 'multiplerowsextended',
        columns: columnConfig
    });

    $('#' + elementId).show();

    //$("#" + elementId).on('mousedown', function (event) {
    //    var rightClick = isRightClick(event) || $.jqx.mobile.isTouchDevice();
    //    if (rightClick) {
    //        var scrollTop = $(window).scrollTop();
    //        var scrollLeft = $(window).scrollLeft();
    //        $('#contextMenu').jqxMenu('open', parseInt(event.clientX) + 5 + scrollLeft, parseInt(event.clientY) + 5 + scrollTop);
    //        return false;
    //    }
    //});

}

function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams();

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}