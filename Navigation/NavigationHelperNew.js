﻿

function configureTabStrip() {
    var resizeAll = function () {
        expandContentDivs(tabStripElement.children(".k-content"));
    }

    var tabStripElement = $('#oareaTabs').kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        },
        select: function (e) {
            if (e.contentElement.id == 'oareaTabs-2') {
                //resizeGrid();
            }

        }
    });

    tabStripElement.parent().attr("id", "tabstrip-parent");

    var tabStrip = tabStripElement.data("kendoTabStrip");

    var expandContentDivs = function (divs) {
        var visibleDiv = divs.filter(":visible");
        divs.height(tabStripElement.innerHeight()
                    - tabStripElement.children(".k-tabstrip-items").outerHeight()
                    - parseFloat(visibleDiv.css("padding-top"))
                    - parseFloat(visibleDiv.css("padding-bottom"))
                    - parseFloat(visibleDiv.css("border-top-width"))
                    - parseFloat(visibleDiv.css("border-bottom-width"))
                    - parseFloat(visibleDiv.css("margin-bottom")));
        // all of the above padding/margin/border calculations can be replaced by a single hard-coded number for improved performance
    }

    resizeAll();

    $(window).resize(function () {
        resizeAll();
    });
}
function init() {
    ontoLog('init navigation');
    
    testWebSocket();

    configureTabStrip();
    $('#toolbarSideNav').kendoToolBar({
        items: [
            { type: "button", id: "openObjectEdit", spriteCssClass: "fa fa-share-alt" }
        ],
        click: function (e) {
            if (e.id == 'closeButton') {
                $('#isListen').hide();
                $('#sendSelect').hide();
                $('#sendApply').hide();
                $('.panel-default').hide();
                document.getElementById("mySidenav").style.width = "0";
            }
            else if (e.id == 'openObjectEdit') {
                window.open("../OntologyApps/ObjectEdit.html", "_blank")
            }
        }
    });

    $("#slide-in-handle").click(function (e) {
        if (visible) {
            slide.reverse();
        } else {
            slide.play();
        }
        visible = !visible;
        e.preventDefault();
    });
    

    $("#panelbar").kendoPanelBar({
        expandMode: "single"
    });

    
    

    $("#toolbar").kendoToolBar({
        items: [
            { type: "button", id: "btnNewWindow", spriteCssClass: "fa fa-desktop", togglable: true },
            { type: "separator" },
            {
                type: "buttonGroup",
                id: "btnGroupFilter",
                //ButtonGroup's items accept the same appearance configration optinos as the button control
                buttons: [
                    { text: "All", id: "btnFilterAll", togglable: true, group: "controlGroup" },
                    { type: "separator" },
                    { text: "A", id: "btnFilterA", togglable: true, group: "controlGroup" },
                    { text: "B", id: "btnFilterB", togglable: true, group: "controlGroup" },
                    { text: "C", id: "btnFilterC", togglable: true, group: "controlGroup" },
                    { text: "D", id: "btnFilterD", togglable: true, group: "controlGroup" },
                    { text: "E", id: "btnFilterE", togglable: true, group: "controlGroup" },
                    { text: "F", id: "btnFilterF", togglable: true, group: "controlGroup" },
                    { text: "G", id: "btnFilterG", togglable: true, group: "controlGroup" },
                    { text: "H", id: "btnFilterH", togglable: true, group: "controlGroup" },
                    { text: "I", id: "btnFilterI", togglable: true, group: "controlGroup" },
                    { text: "J", id: "btnFilterJ", togglable: true, group: "controlGroup" },
                    { text: "K", id: "btnFilterK", togglable: true, group: "controlGroup" },
                    { text: "L", id: "btnFilterL", togglable: true, group: "controlGroup" },
                    { text: "M", id: "btnFilterM", togglable: true, group: "controlGroup" },
                    { text: "N", id: "btnFilterN", togglable: true, group: "controlGroup" },
                    { text: "O", id: "btnFilterO", togglable: true, group: "controlGroup" },
                    { text: "P", id: "btnFilterP", togglable: true, group: "controlGroup" },
                    { text: "Q", id: "btnFilterQ", togglable: true, group: "controlGroup" },
                    { text: "R", id: "btnFilterR", togglable: true, group: "controlGroup" },
                    { text: "S", id: "btnFilterS", togglable: true, group: "controlGroup" },
                    { text: "T", id: "btnFilterT", togglable: true, group: "controlGroup" },
                    { text: "U", id: "btnFilterU", togglable: true, group: "controlGroup" },
                    { text: "V", id: "btnFilterV", togglable: true, group: "controlGroup" },
                    { text: "W", id: "btnFilterW", togglable: true, group: "controlGroup" },
                    { text: "X", id: "btnFilterX", togglable: true, group: "controlGroup" },
                    { text: "Y", id: "btnFilterY", togglable: true, group: "controlGroup" },
                    { text: "Z", id: "btnFilterZ", togglable: true, group: "controlGroup" },

                ]
            }
        ],
        toggle: function (e) {
            var btnFilter = 'btnFilter';
            if (e.id == 'btnNewWindow') {
                SendStringPropertyChangeWithValue("IsToggled_OpenWindow", e.checked);
            }
            else if (e.id.startsWith(btnFilter)) {
                var filter = e.id.replace(btnFilter, '');
                if (filter.length == 1) {
                    var dataSource = $("#treelist").data("kendoTreeList").dataSource;
                    dataSource.filter({ field: "text", operator: "startswith", value: filter });
                }
                else {
                    var dataSource = $("#treelist").data("kendoTreeList").dataSource;
                    dataSource.filter([]);
                }
                
            }
        },
        click: function (e) {
            if (e.id == 'btnOpenSideNav') {
                document.getElementById("mySidenav").style.width = "100%";
                $('#isListen').show();
                $('#sendSelect').show();
                $('#sendApply').show();
                $('.panel-default').show();
            }
        }
    });
    
    var tabStripElement = $("#tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
    });

}

function addEventListenerSearch() {
    $('#itemSearch').find('iframe').load(function () {
        var docEl = $($('#itemSearch').find('iframe')).contents()[0].documentElement;

        docEl.addEventListener('resizeParentElement', function (e)  { 
        }, false);
    });
}

function treeSelectionChanged(id) {
    SendStringPropertyChangeWithValue("Text_SelectedNodeId", id);

}

function onMessage(evt) {


    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);
        if (viewItem.ViewItemType == 'Command') {

        }
        else {
            if (viewItem.ViewItemId == 'Url_TreeData') {
                console.log(viewItem.LastValue)
                jQuery.getJSON(viewItem.LastValue, function (treeData) {
                    var dataSource = new kendo.data.TreeListDataSource({
                        data: treeData,
                        schema: {
                            model: {
                                id: "id",
                                expanded: true
                            }
                        }
                    });

                    $("#treelist").kendoTreeList({
                        dataSource: dataSource,
                        height: 800,
                        width: '100%',
                        filterable: true,
                        sortable: true,
                        selectable: "row",
                        columns: [
                            { field: "text", title: "View" }
                        ],
                        change: function (e) {
                            var selectedRows = this.select();
                            var selectedDataItems = [];
                            for (var i = 0; i < selectedRows.length; i++) {
                                var dataItem = this.dataItem(selectedRows[i]);
                                treeSelectionChanged(dataItem.id);
                            }
                            // selectedDataItems contains all selected data items
                        }
                    });
                });

                

            }
            else if (viewItem.ViewItemId == 'IsSuccessful_Login') {
               

            }
            else if (viewItem.ViewItemId == "NewNavigationItem") {
                console.info(viewItem.LastValue);
                if (viewItem.LastValue.ItemType == 1) {
                    window.open(viewItem.LastValue.src, '_blank');
                }
                else if (viewItem.LastValue.ItemType == 2) {
                    var treeList = $('#treelist').find('.k-link');
                    var treeListItem = $(treeList[0]);

                    var left = 200;
                    var top = treeListItem.offset().top;

                    var width = $(document).width() - left - 20;
                    var height = $(window).height() - top - 50;
                    
                    


                    var viewAres = $('.viewArea');
                    var windowContainer = $('#' + viewItem.LastValue.Id);
                    

                    if (windowContainer.length == 0) {
                        windowContainer = $('<div id="' + viewItem.LastValue.Id + '"><iframe class="k-content-frame" src="' + viewItem.LastValue.src + '"></div>"')
                        viewAres.append(windowContainer);
                        windowContainer.attr('data-name', viewItem.LastValue.Name);
                        windowContainer.find('iframe').load(function () {
                            
                            // select the target node
                            var titleEl = $(windowContainer.find('iframe')).contents().find("title")[0];
                            var docEl = $(windowContainer.find('iframe')).contents()[0].documentElement;

                            if (docEl && docEl.addEventListener) {
                                docEl.addEventListener("DOMSubtreeModified", function (evt) {
                                    var t = evt.target;
                                    if (t === titleEl || (t.parentNode && t.parentNode === titleEl)) {
                                        var window = windowContainer.closest('.k-window');
                                        var windowTitle = window.find('.k-window-title')
                                        windowTitle.text(titleEl.text);
                                        
                                    }
                                }, false);
                            } else {
                                document.onpropertychange = function () {
                                    if (window.event.propertyName == "title") {
                                        var title = windowContainer.attr('data-name');
                                        var window = windowContainer.closest('.k-window');
                                        var windowTitle = window.find('.k-window-title')
                                        windowTitle.text(titleEl.text);
                                    }
                                };
                            }
                        });
                        $('#pageContainer').append(windowContainer);

                        var kendoWindow = windowContainer.kendoWindow({
                            width: width,
                            height: height,
                            title: viewItem.LastValue.Name,
                            visible: false,
                            actions: [
                                "Refresh",
                                "Minimize",
                                "Maximize",
                                "Close"
                            ],
                            close: function (e) {

                            },
                            refresh: function () {
                                var iframe = windowContainer.find('iframe');
                                iframe.attr('src', iframe.attr('src'));
                            }
                        }).data("kendoWindow").center().open();
                        windowContainer.closest('.k-window').css({ left: left, top: top, width: width, height: height });
                    }
                    else {
                        if (windowContainer.data("kendoWindow").element.is(':hidden')) {
                            windowContainer.data("kendoWindow").open();
                        }
                        windowContainer.data("kendoWindow").toFront();
                    }
                }

                


            }
            else if (viewItem.ViewItemId == "SplitterMode") {
                
            }
            else if (viewItem.ViewItemId == 'IsToggled_Horizontal') {

            }
            else if (viewItem.ViewItemId == 'IsToggled_Vertical') {

            }
            else if (viewItem.ViewItemId == 'IsToggled_Four') {

            }
            else if (viewItem.ViewItemId == 'IsEnabled_Horizontal') {

            }
            else if (viewItem.ViewItemId == 'IsEnabled_Vertical') {

            }
            else if (viewItem.ViewItemId == 'IsEnabled_Four') {
                
            }
        }
    }


    //websocket.close();
}




function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams()


    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}