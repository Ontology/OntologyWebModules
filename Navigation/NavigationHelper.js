﻿

var docId;
var tabInit;
var selectedTab;
var myDocker;
var panelContainer
var DemoGrid;

function init() {
    ontoLog('init navigation');
    testWebSocket();

    $("#slide-in-handle").click(function (e) {
        if (visible) {
            slide.reverse();
        } else {
            slide.play();
        }
        visible = !visible;
        e.preventDefault();
    });
    openNav();
}

function openNav() {
    document.getElementById("mySidenav").style.width = "200px";
    $('#isListen').show();
    $('#sendSelect').show();
    $('#sendApply').show();
    $('.panel-default').show();
}

/* Set the width of the side navigation to 0 */
function closeNav() {
    $('#isListen').hide();
    $('#sendSelect').hide();
    $('#sendApply').hide();
    $('.panel-default').hide();
    document.getElementById("mySidenav").style.width = "0";

}

function treeSelectionChanged(id) {
    SendStringPropertyChangeWithValue("Text_SelectedNodeId", id);

}

function onMessage(evt) {


    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);
        if (viewItem.ViewItemType == 'Command') {

        }
        else {
            if (viewItem.ViewItemId == 'Url_TreeData') {
                console.log(viewItem.LastValue)
                jQuery.getJSON(viewItem.LastValue, function (data) {
                    initLayout(data);
                });



            }
            else if (viewItem.ViewItemId == 'IsSuccessful_Login') {
                if (!viewItem.LastValue) {

                    return;
                }

            }
            else if (viewItem.ViewItemId == "NewNavigationItem") {
                console.info(viewItem.LastValue);
                if (viewItem.LastValue.ItemType == 0) {
                    //var divParent = document.getElementById('parent');
                    //console.log('tab:' + selectedTab);

                    var divContainer;

                    divContainer = document.getElementById(viewItem.LastValue.ContainerId);

                    var divWindow = document.createElement('div');
                    divWindow.id = 'win_' + viewItem.LastValue.Id;
                    divWindow.setAttribute('class', 'fit98');
                    var divContent = document.createElement('div');
                    divContent.setAttribute('overflow', 'hidden');
                    divContent.setAttribute('class', 'fit98');
                    var iFrame = document.createElement('iframe');
                    iFrame.id = viewItem.LastValue.Id
                    iFrame.setAttribute('src', viewItem.LastValue.src);
                    iFrame.setAttribute('class', 'fit98');
                    divContent.appendChild(iFrame);


                    divWindow.appendChild(divContent);
                    divContainer.appendChild(divWindow);

                    //divParent.appendChild(divWindow);
                    //$('#docks').jqxDocking('addWindow', 'win_' + objJSON.NewNavigationItem.Id, 'default', panelId, 1);

                }
                else if (viewItem.LastValue.ItemType == 1) {
                    if (ctrlFlag) {
                        window.open('./NavigationJQ.html?Session=' + sessionId, '_blank');
                    }
                    else {
                        window.open(viewItem.LastValue.src, '_blank');
                        
                    }
                    
                }
                else if (viewItem.LastValue.ItemType == 2) {
                    var first = false;
                    var ulItem = document.getElementById("tabPageList");
                    var divContainer = document.getElementById(viewItem.LastValue.ContainerId);
                    if (ulItem == null || ulItem == undefined) {
                        console.log(ulItem);
                        var ulItem = document.createElement("ul");
                        ulItem.id = "tabPageList";

                        divContainer.appendChild(ulItem);
                        var li = document.createElement("li");
                        li.innerHTML = viewItem.LastValue.Name;
                        ulItem.appendChild(li);
                        var divItem = document.createElement("div");

                        var iFrame = document.createElement('iframe');
                        iFrame.id = viewItem.LastValue.Id
                        iFrame.setAttribute('src', viewItem.LastValue.src);
                        iFrame.setAttribute('class', 'fit98');

                        divItem.appendChild(iFrame);
                        divContainer.appendChild(divItem);
                        console.info(divContainer);
                        $('#' + divContainer.id).jqxTabs({ width: '100%', height: '100%', position: 'top', showCloseButtons: true });
                    } else {
                        var iFrame = document.createElement('iframe');
                        iFrame.id = viewItem.LastValue.Id
                        iFrame.setAttribute('src', viewItem.LastValue.src);
                        iFrame.setAttribute('class', 'fit98');

                        $('#' + divContainer.id).jqxTabs('addLast', viewItem.LastValue.Name, iFrame.outerHTML);
                    }

                }



            }
            else if (viewItem.ViewItemId == "SplitterMode") {
                createSplitter(viewItem.LastValue);
            }
            else if (viewItem.ViewItemId == 'IsToggled_Horizontal') {
                $("#splitHorizontal").jqxToggleButton({ toggled: viewItem.LastValue });
            }
            else if (viewItem.ViewItemId == 'IsToggled_Vertical') {
                $("#splitVertical").jqxToggleButton({ toggled: viewItem.LastValue });
            }
            else if (viewItem.ViewItemId == 'IsToggled_Four') {
                $("#splitFour").jqxToggleButton({ toggled: viewItem.LastValue });
            }
            else if (viewItem.ViewItemId == 'IsEnabled_Horizontal') {
                $("#splitHorizontal").jqxToggleButton({ disabled: !viewItem.LastValue });
            }
            else if (viewItem.ViewItemId == 'IsEnabled_Vertical') {
                $("#splitVertical").jqxToggleButton({ disabled: !viewItem.LastValue });
            }
            else if (viewItem.ViewItemId == 'IsEnabled_Four') {
                $("#splitFour").jqxToggleButton({ disabled: !viewItem.LastValue });
            }
        }
    }


    //websocket.close();
}

function initLayout(sourceJson) {
    var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'id' },
                    { name: 'parentid' },
                    { name: 'text' },
                    { name: 'subMenuWidth' }
                ],
                id: 'id',
                localdata: sourceJson
            };
    // create data adapter.
    console.log(sourceJson);
    var dataAdapter = new $.jqx.dataAdapter(source);
    // perform Data Binding.
    dataAdapter.dataBind();
    // get the menu items. The first parameter is the item's id. The second parameter is the parent item's id. The 'items' parameter represents 
    // the sub items collection name. Each jqxTree item has a 'label' property, but in the JSON data, we have a 'text' field. The last parameter 
    // specifies the mapping between the 'text' and 'label' fields.  
    var records = dataAdapter.getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label' }]);
    $('#jqxWidget').jqxMenu({ source: records, height: 30, width: '100%' });

    $("#jqxWidget").on('itemclick', function (event) {

        SendStringPropertyChangeWithValue('IsPressed_Ctrl', event.ctrlKey);
        treeSelectionChanged(event.args.id);
    });
    $("#jqxWidget").jqxMenu('minimize');

    $("#newWindow").jqxToggleButton({ width: '32px', height: '32px', toggled: false, value: "<i id='openInWindow' class='fa fa-desktop' aria-hidden='true'></i>" });
    $('#newWindow').on('click', function () {
        SendStringPropertyChangeWithValue("IsToggled_OpenWindow", $('#newWindow').jqxToggleButton('toggled'));
    });
    $("#newWindow").css('visibility', 'visible');
    $("#splitVertical").jqxToggleButton({ width: '32px', height: '32px', toggled: false, value: "<i class='fa fa-columns' aria-hidden='true'></i>" });
    $('#splitVertical').on('click', function () {
        SendStringPropertyChangeWithValue("IsToggled_Vertical", $("#splitVertical").jqxToggleButton('toggled'));
    });
    $("#splitVertical").css('visibility', 'visible');

    $("#splitHorizontal").jqxToggleButton({ width: '32px', height: '32px', toggled: false, value: "<i class='fa fa-columns fa-rotate-90' aria-hidden='true'></i>" });
    $('#splitHorizontal').on('click', function () {
        SendStringPropertyChangeWithValue("IsToggled_Horizontal", $("#splitHorizontal").jqxToggleButton('toggled'));
    });
    $("#splitHorizontal").css('visibility', 'visible');

    $("#splitFour").jqxToggleButton({ width: '32px', height: '32px', toggled: false, value: "<i class='fa fa-th-large' aria-hidden='true'></i>" });
    $('#splitFour').on('click', function () {
        SendStringPropertyChangeWithValue("IsToggled_Four", $("#splitFour").jqxToggleButton('toggled'));
    });
    $("#splitFour").css('visibility', 'visible');

    $("#minimizeMenu").jqxToggleButton({ width: '32px', height: '32px', toggled: false, value: "<i class='fa fa-bars' aria-hidden='true'></i>" });
    $('#minimizeMenu').on('click', function () {
        if ($("#minimizeMenu").jqxToggleButton('toggled')) {
            $("#jqxWidget").jqxMenu('minimize');
        }
        else {
            $("#jqxWidget").jqxMenu('restore');
        }
    });
    $("#minimizeMenu").css('visibility', 'visible');


    //$('#docks').jqxDocking({ orientation: 'horizontal', width: '100%', height: '100%' });

    var docs = document.getElementById('docks');


}

function createSplitter(splitterMode) {

    var containerWidth = $('#content').width() - 20;
    var containerHeight = $('#content').height() - 20;
    console.log('width: ' + containerWidth + ' / height:' + containerHeight);

    if (splitterMode == 1) {
        var divMainSplitt = document.createElement('div');
        divMainSplitt.setAttribute('class', 'fitfull');
        divMainSplitt.id = 'mainSplitter';

        var divLeft = document.createElement('div');
        divLeft.setAttribute('class', 'fitfull');
        divLeft.id = 'left';

        var divRight = document.createElement('div');
        divRight.setAttribute('class', 'fitfull');
        divRight.id = 'right';

        divMainSplitt.appendChild(divLeft);
        divMainSplitt.appendChild(divRight);
        $('#content').append(divMainSplitt);


        $('#mainSplitter').jqxSplitter({ width: containerWidth, height: containerHeight, panels: [{ size: 290, min: 100 }, { min: 290, size: 400 }] });

    } else if (splitterMode == 2) {
        var divMainSplitt = document.createElement('div');
        divMainSplitt.setAttribute('class', 'fitfull');
        divMainSplitt.id = 'mainSplitter';

        var divLeft = document.createElement('div');
        divLeft.setAttribute('class', 'fitfull');
        divLeft.id = 'left';

        var divRight = document.createElement('div');
        divRight.setAttribute('class', 'fitfull');
        divRight.id = 'right';

        divMainSplitt.appendChild(divLeft);
        divMainSplitt.appendChild(divRight);
        $('#content').append(divMainSplitt);

        $('#mainSplitter').jqxSplitter({ width: containerWidth, height: containerHeight, orientation: 'horizontal', panels: [{ size: 290 }, { size: 400 }] });
    } else if (splitterMode == 3) {
        var divSplitContainer = document.createElement('div');
        divSplitContainer.setAttribute('class', 'fitfull');
        divSplitContainer.id = 'splitContainer';

        var divLeft = document.createElement('div');
        divLeft.setAttribute('class', 'fitfull');
        divLeft.id = 'leftSplitter';

        var divOne = document.createElement('div');
        divOne.setAttribute('class', 'fitfull');
        divOne.id = 'one';
        divLeft.appendChild(divOne);

        var divTwo = document.createElement('div');
        divTwo.setAttribute('class', 'fitfull');
        divTwo.id = 'two';
        divLeft.appendChild(divTwo);

        var divRight = document.createElement('div');
        divRight.setAttribute('class', 'fitfull');
        divRight.id = 'rightSplitter';

        var divThree = document.createElement('div');
        divThree.setAttribute('class', 'fitfull');
        divThree.id = 'three';
        divRight.appendChild(divThree);

        var divFour = document.createElement('div');
        divFour.setAttribute('class', 'fitfull');
        divFour.id = 'four';
        divRight.appendChild(divFour);

        var div1 = document.createElement('div');
        var div2 = document.createElement('div');

        div1.appendChild(divLeft);
        div2.appendChild(divRight);
        divSplitContainer.appendChild(div1);
        divSplitContainer.appendChild(div2);

        console.log(divSplitContainer);
        $('#content').append(divSplitContainer);


        $('#splitContainer').jqxSplitter({ width: containerWidth, height: containerHeight, orientation: 'horizontal', panels: [{ size: '50%' }, { size: '50%' }] });
        $('#leftSplitter').jqxSplitter({ height: '100%', width: '100%', orientation: 'vertical', panels: [{ size: '50%' }, { size: '50%' }] });
        $('#rightSplitter').jqxSplitter({ height: '100%', width: '100%', orientation: 'vertical', panels: [{ size: '50%' }, { size: '50%' }] });
    }



}

function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams()


    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}