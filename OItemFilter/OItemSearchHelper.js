﻿
(function ($) {
    $.fn.createSearchArea = function (removable, id) {
        var divItem = $(this);

        if (id == undefined) {
            id = "_" + guid();
        }
        

        var rowItem = $('<div class="row"></div>');
        divItem.append(rowItem);

        var colItem = $('<div class="col-sm-12"></div>');
        rowItem.append(colItem);

        var wellItem = $('<div class="well well-sm fullWidth"></div>');
        colItem.append(wellItem);

        var rowItem2 = $('<div class="row"></div>')
        wellItem.append(rowItem2);

        var colItemSearchButtons = $('<div class="col-sm-2 searchButtons"></div>');

        rowItem2.append(colItemSearchButtons);

        var colItemSearchText = $('<div class="col-sm-8 searchText"></div>');

        rowItem2.append(colItemSearchText);

        var colItemSearchActions = $('<div class="col-sm-2 searchActions"></div>');

        rowItem2.append(colItemSearchActions);

        var buttonGroup = $('<div class="btn-group pull-right" role="group" >');
        colItemSearchActions.append(buttonGroup);

        var removeButton = $('<button class="removeButton"><i class="fa fa-times" aria-hidden="true"></i></button>');
        if (removable) {
            buttonGroup.append(removeButton);
        }
        
        var addButton = $('<button class="addButton"><i class="fa fa-plus" aria-hidden="true"></i></button>')
        buttonGroup.append(addButton);

        wellItem.attr('id', id);

        var classButton = $('<button class="classButton searchButton"><i class="fa fa-puzzle-piece" aria-hidden="true"></i></button>');
        var objectButton = $('<button class="objectButton searchButton"><i class="fa fa-cubes" aria-hidden="true"></i></button>');
        var relationTypeButton = $('<button class="relationTypeButton searchButton"><i class="fa fa-share-alt-square" aria-hidden="true"></i></button>');
        var attributeTypeButton = $('<button class="attributeTypeButton searchButton"><i class="fa fa-sitemap" aria-hidden="true"></i></button>');
        var leftRightTypeButton = $('<button class="leftRightButton searchButton"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></button>');
        var rightLeftTypeButton = $('<button class="rightLeftButton searchButton"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i></button>');


        var table = $('<table></table>');
        colItemSearchButtons.append(table);

        var row1 = $('<tr></tr>');
        var row2 = $('<tr></tr>');

        table.append(row1);
        table.append(row2);
        
        var classCell = $('<td></td>');
        classCell.append(classButton);

        var objectCell = $('<td></td>');
        objectCell.append(objectButton);

        var relationTypeCell = $('<td></td>');
        relationTypeCell.append(relationTypeButton);

        var attributeTypeCell = $('<td></td>');
        attributeTypeCell.append(attributeTypeButton);

        var leftRightTypeCell = $('<td></td>');
        leftRightTypeCell.append(leftRightTypeButton);

        var rightLeftTypeCell = $('<td></td>');
        rightLeftTypeCell.append(rightLeftTypeButton);

        row1.append(classCell);
        row2.append(objectCell);

        row1.append(attributeTypeCell);
        row2.append(relationTypeCell);

        row1.append(leftRightTypeCell);
        row2.append(rightLeftTypeCell);

        wellItem.find('.classButton').on('click', function () {
            var wellItem = $(this).closest('.well');
            var item = {
                id: wellItem.attr('id'),
                type: 'classButton'
            };

            console.log(item);

            SendPropertyChangeWithValue("searchItem", "Other", "Other", item);
        });

        wellItem.find('.objectButton').on('click', function () {
            var wellItem = $(this).closest('.well');
            var item = {
                id: wellItem.attr('id'),
                type: 'objectButton'
            };

            console.log(item);

            SendPropertyChangeWithValue("searchItem", "Other", "Other", item);
        });

        wellItem.find('.relationTypeButton').on('click', function () {
            var wellItem = $(this).closest('.well');
            var item = {
                id: wellItem.attr('id'),
                type: 'relationTypeButton'
            };

            console.log(item);

            SendPropertyChangeWithValue("searchItem", "Other", "Other", item);
        });

        wellItem.find('.attributeTypeButton').on('click', function () {
            var wellItem = $(this).closest('.well');
            var item = {
                id: wellItem.attr('id'),
                type: 'attributeTypeButton'
            };

            console.log(item);

            SendPropertyChangeWithValue("searchItem", "Other", "Other", item);
        });

        wellItem.find('.leftRightButton').on('click', function () {
            var wellItem = $(this).closest('.well');
            var item = {
                id: wellItem.attr('id'),
                type: 'leftRightButton'
            };

            console.log(item);

            SendPropertyChangeWithValue("searchItem", "Other", "Other", item);
        });

        wellItem.find('.rightLeftButton').on('click', function () {
            var wellItem = $(this).closest('.well');
            var item = {
                id: wellItem.attr('id'),
                type: 'rightLeftButton'
            };

            console.log(item);

            SendPropertyChangeWithValue("searchItem", "Other", "Other", item);
        });

        wellItem.find('.removeButton').on('click', function () {
            var wellItem = $(this).closest('.well');
            var item = {
                id: wellItem.attr('id')
            };
            SendPropertyChangeWithValue("searchItemToDel", "Other", "Other", item);

        });

        wellItem.find('.addButton').on('click', function () {
            var item = {
                id: guid()
            };

            SendPropertyChangeWithValue("searchItemToAdd", "Other", "Other", item);
        });

       
    };
})(jQuery);

function init() {

    testWebSocket();

    

    $("#panelbar").kendoPanelBar({
        expandMode: "single"
    });

    $('#resultToolbar').kendoToolBar({
        items: [
            {
                template: '<button id="applyItem"><i class="fa fa-check-circle" aria-hidden="true"></i></button>'
            }
        ]
    });

    $("#applyItem").kendoButton({
        enable: false,
        click: function (e) {
            SendStringCommandWithoutParameter('clicked.apply');
        }
    });
}

function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemId == 'searchItemChange') {
            if (!viewItem.LastValue.remove && !viewItem.LastValue.clear && !viewItem.LastValue.add && !viewItem.LastValue.addAtttypeInput) {
                var searchButtonArea = $('#' + viewItem.LastValue.id).find('.searchButtons');
                var button = searchButtonArea.find('.' + viewItem.LastValue.type);
                var icon = $(button.html());
                icon.addClass('fa-2x');
                searchButtonArea.html('');
                searchButtonArea.append(icon);

                if (viewItem.LastValue.type == 'classButton') {
                    var searchTextArea = $('#' + viewItem.LastValue.id).find('.searchText');
                    var dropDownInput = $("<input class='classDropDown fullWidth' />");
                    
                    searchTextArea.append(dropDownInput);
                    jQuery.extend(viewItem.LastValue.dropDownConfig,{
                        filter: "contains",
                        suggest: true
                    });

                    searchTextArea.find('.classDropDown').kendoComboBox(viewItem.LastValue.dropDownConfig);
                    searchTextArea.find('.classDropDown').change(function (e) {
                        var wellItem = $(this).closest('.well');
                        var value = $(this).val();
                        SendPropertyChangeWithValue(wellItem.attr('id'), 'SelectedIndex', 'KendoDropDownList', value)
                    });
                   
                    
                }
                else if (viewItem.LastValue.type == 'objectButton') {
                    var searchTextArea = $('#' + viewItem.LastValue.id).find('.searchText');
                    var input = $('<input class="objectIdOrName fullWidth" placeholder="GUID or Name" /input>');
                    input.on("input", function () {
                        var wellItem = $(this).closest('.well');
                        var item = {
                            id: wellItem.attr('id'),
                            type: 'objectButton',
                            searchText: $(this).val()
                        };

                        console.log(item);

                        SendPropertyChangeWithValue("searchItemTextChanged", "Other", "Other", item);
                    });
                    searchTextArea.append(input);
                }
                else if (viewItem.LastValue.type == 'attributeTypeButton') {
                    var searchTextArea = $('#' + viewItem.LastValue.id).find('.searchText');
                    var table = $('<table class="fullWidth"></table>');
                    searchTextArea.append(table);

                    var tr1 = $('<tr></tr>');
                    table.append(tr1);

                    var td1 = $('<td></td>');
                    tr1.append(td1);

                    var tr2 = $('<tr></tr>');
                    table.append(tr2);

                    var td2 = $('<td class="valueCol"></td>');
                    tr2.append(td2);

                    var dropDownInput = $("<input class='attributeTypeDropDown fullWidth' />");
                    
                    td1.append(dropDownInput);

                    jQuery.extend(viewItem.LastValue.dropDownConfig, {
                        filter: "contains",
                        suggest: true
                    });

                    searchTextArea.find('.attributeTypeDropDown').kendoComboBox(viewItem.LastValue.dropDownConfig);
                    searchTextArea.find('.attributeTypeDropDown').change(function (e) {
                        var wellItem = $(this).closest('.well');
                        var value = $(this).val();
                        SendPropertyChangeWithValue(wellItem.attr('id'), 'SelectedIndex', 'KendoDropDownList', value)
                    });
                    
                }
                else if (viewItem.LastValue.type == 'relationTypeButton') {
                    var searchTextArea = $('#' + viewItem.LastValue.id).find('.searchText');
                    var table = $('<table class="fullWidth"></table>');
                    searchTextArea.append(table);

                    var tr1 = $('<tr></tr>');
                    table.append(tr1);

                    var td1 = $('<td></td>');
                    tr1.append(td1);

                    var tr2 = $('<tr></tr>');
                    table.append(tr2);

                    var td2 = $('<td class="valueCol"></td>');
                    tr2.append(td2);

                    var dropDownInput = $("<input class='relationTypeDropDown fullWidth' />");

                    td1.append(dropDownInput);

                    jQuery.extend(viewItem.LastValue.dropDownConfig, {
                        filter: "contains",
                        suggest: true
                    });

                    searchTextArea.find('.relationTypeDropDown').kendoComboBox(viewItem.LastValue.dropDownConfig);
                    searchTextArea.find('.relationTypeDropDown').change(function (e) {
                        var wellItem = $(this).closest('.well');
                        var value = $(this).val();
                        SendPropertyChangeWithValue(wellItem.attr('id'), 'SelectedIndex', 'KendoDropDownList', value)
                    });

                }
                else if (viewItem.LastValue.type == 'leftRightButton' || viewItem.LastValue.type == 'rightLeftButton') {
                    var searchTextArea = $('#' + viewItem.LastValue.id).find('.searchText');
                    var input = $('<input class="idRelOrName fullWidth" placeholder="GUID or Name" /input>');
                    input.on("input", function () {
                        var wellItem = $(this).closest('.well');
                        var item = {
                            id: wellItem.attr('id'),
                            type: viewItem.LastValue.type,
                            searchText: $(this).val()
                        };

                        console.log(item);

                        SendPropertyChangeWithValue("searchItemTextChanged", "Other", "Other", item);
                    });
                    searchTextArea.append(input);
                }
            }
            else if (viewItem.LastValue.remove) {
                var wellItem = $('#' + viewItem.LastValue.id);
                var rowItem = wellItem.closest('.row');
                rowItem.remove();
            }
            else if (viewItem.LastValue.clear) {
                var wellItem = $('#' + viewItem.LastValue.id);
                var rowItem = wellItem.closest('.row');
                rowItem.remove();

                $('#searchCriteriaRow').createSearchArea(true, viewItem.LastValue.id);
            }
            else if (viewItem.LastValue.add) {
                $('#searchCriteriaRow').createSearchArea(true, viewItem.LastValue.id);
            }
            else if (viewItem.LastValue.addAtttypeInput) {
                var searchTextArea = $('#' + viewItem.LastValue.id).find('.searchText');
                var valueCol = searchTextArea.find('.valueCol');

                valueCol.html('');

                if (viewItem.LastValue.inputType == "Bool") {
                    var checkBoxItem = $('<input type="checkbox" class="attributeValue">');
                    checkBoxItem.on('change', function () {
                        var wellItem = $(this).closest('.well');
                        var checked = $(this).prop('checked');
                        var item = {
                            id: wellItem.attr('id'),
                            boolValue: checked
                        };
                        SendPropertyChangeWithValue("searchItemAttValue", "Other", "Other", item);
                    })
                    valueCol.append(checkBoxItem);

                    var wellItem = checkBoxItem.closest('.well');
                    var checked = checkBoxItem.prop('checked');
                    var item = {
                        id: wellItem.attr('id'),
                        boolValue: checked
                    };
                    SendPropertyChangeWithValue("searchItemAttValue", "Other", "Other", item);
                }
                else if (viewItem.LastValue.inputType == "Datetime") {
                    var dateTimePicker = $('<input class="attributeValue">');
                    dateTimePicker.on('change', function () {
                        var wellItem = $(this).closest('.well');
                        var value = $(this).val();
                        var item = {
                            id: wellItem.attr('id'),
                            dateTimeValue: value
                        };
                        SendPropertyChangeWithValue("searchItemAttValue", "Other", "Other", item);
                    })
                    valueCol.append(dateTimePicker);

                    var wellItem = dateTimePicker.closest('.well');
                    var value = dateTimePicker.val();
                    var item = {
                        id: wellItem.attr('id'),
                        dateTimeValue: value
                    };
                    SendPropertyChangeWithValue("searchItemAttValue", "Other", "Other", item);

                    dateTimePicker.kendoDateTimePicker();
                }
                else if (viewItem.LastValue.inputType == "Real") {
                    var dblInp = $('<input type="number" class="attributeValue">');
                    dblInp.on('change', function () {
                        var wellItem = $(this).closest('.well');
                        var value = $(this).val();
                        var item = {
                            id: wellItem.attr('id'),
                            dblValue: value
                        };
                        SendPropertyChangeWithValue("searchItemAttValue", "Other", "Other", item);
                    })
                    valueCol.append(dblInp);

                    var wellItem = dblInp.closest('.well');
                    var value = dblInp.val();
                    var item = {
                        id: wellItem.attr('id'),
                        dblValue: value
                    };
                    SendPropertyChangeWithValue("searchItemAttValue", "Other", "Other", item);


                }
                else if (viewItem.LastValue.inputType == "Int") {
                    var intInp = $('<input type="number" class="attributeValue">');
                    intInp.on('change', function () {
                        var wellItem = $(this).closest('.well');
                        var value = $(this).val();
                        var item = {
                            id: wellItem.attr('id'),
                            intValue: value
                        };
                        SendPropertyChangeWithValue("searchItemAttValue", "Other", "Other", item);
                    })
                    valueCol.append(intInp);

                    var wellItem = intInp.closest('.well');
                    var value = intInp.val();
                    var item = {
                        id: wellItem.attr('id'),
                        intValue: value
                    };
                    SendPropertyChangeWithValue("searchItemAttValue", "Other", "Other", item);


                }
                else if (viewItem.LastValue.inputType == "String") {
                    var intInp = $('<input type="text" class="attributeValue">');
                    intInp.on('change', function () {
                        var wellItem = $(this).closest('.well');
                        var value = $(this).val();
                        var item = {
                            id: wellItem.attr('id'),
                            strValue: value
                        };
                        SendPropertyChangeWithValue("searchItemAttValue", "Other", "Other", item);
                    })
                    valueCol.append(intInp);

                    var wellItem = intInp.closest('.well');
                    var value = intInp.val();
                    var item = {
                        id: wellItem.attr('id'),
                        strValue: value
                    };
                    SendPropertyChangeWithValue("searchItemAttValue", "Other", "Other", item);


                }
            }
            
            
        }
        else if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemType == 'DataSource' && viewItem.ViewItemClass == 'Grid') {
            console.info(viewItem.LastValue);
            //createGrid(viewItem.LastValue);

            var gridConfig = {
                gridConfig: viewItem.LastValue,
                dataBoundFunc: function (e) {
                    var grid = $("#grid").data("kendoGrid");
                    $('#resultCount').text(grid.dataSource.total());

                    $('.applyCellContent').on("change", function () {
                        var grid = $("#grid").data("kendoGrid");
                        var dataItem = grid.dataItem($(this).closest('tr'));
                        dataItem.ApplyItem = $(this).is(':checked');
                        SendPropertyChangeWithValue("grid", "Content", "grid", dataItem);
                    });
                },
                changeFunc: function (e) {
                    var selectedRows = this.select();
                    var selectedDataItems = [];
                    for (var i = 0; i < selectedRows.length; i++) {
                        var dataItem = this.dataItem(selectedRows[i]);
                        SendPropertyChangeWithValue("resultItemSelected", "Other", "Other", dataItem);
                    }

                },
                dataSourceChnageFunc: function (e) {
                    console.log(e);
                }
            }
            $('#grid').createKendoGrid(gridConfig);

        }
    }

    //websocket.close();
}

function setButtonIcon(viewItem) {
    if (viewItem.LastValue == true) {
        $('#' + viewItem.ViewItemId).html('<i class="fa fa-assistive-listening-systems" aria-hidden="true"></i>')
        $('#' + viewItem.ViewItemId).removeClass('k-default');
        $('#' + viewItem.ViewItemId).addClass('k-primary');
    }
    else {
        $('#' + viewItem.ViewItemId).html('<i class="fa fa-circle-thin" aria-hidden="true"></i>')
        $('#' + viewItem.ViewItemId).removeClass('k-primary');
        $('#' + viewItem.ViewItemId).addClass('k-default');
        
    }
    
}

function onOpen(evt) {
    ontoLog("CONNECTED");
    var item = {
        id: '_' + guid()
    };

    SendPropertyChangeWithValue("searchItemToAdd", "Other", "Other", item);
    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}