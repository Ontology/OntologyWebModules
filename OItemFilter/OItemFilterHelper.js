﻿



function init() {


    output = document.getElementById("output");
    testWebSocket();
    initSplitter();
    initControls();
}

function initControls() {
    createFilterObjectWindow();

    $('#inpObject').jqxInput({ height: 25, width: "100%", minLength: 1 });
    $('#inpClass').jqxInput({ height: 25, width: "100%", minLength: 1 });
    $('#inpRelationType').jqxInput({ height: 25, width: "100%", minLength: 1 });

    $('#btnAddObject').jqxButton({ width: 30, height: 30 });
    $('#btnAddObject').on('click', function () {
        SendStringCommandWithoutParameter('OpenObjectList')
    });
    $('#btnRemoveObject').jqxButton({ width: 30, height: 30 });
    $('#btnNullItem').jqxToggleButton({ width: 30, height: 30 });
    $('#btnNullItem').on('click', function () {
        var toggled = $("#btnNullItem").jqxToggleButton('toggled');
        SendPropertyChangeWithValue('btnNullItem', 'Checked', 'ToggleButton', toggled);
    });

    $('#applyItem').jqxButton({ width: 30, height: 30 })
    $('#applyItem').on('click', function () {
        SendStringCommandWithoutParameter('ApplyFilter')
    });

    $('#isListen').jqxToggleButton({ width: 30, height: 30 });
    $('#isListen').on('click', function () {
        SendPropertyChangeWithValue('isListen', 'Other', 'Other', $("#isListen").jqxToggleButton('toggled'));
    });

}

//Creating the demo window
function createFilterObjectWindow() {
    $('#filterObjectWindow').jqxWindow({
        position: { x: 100, y: 100 },
        showCollapseButton: true, maxHeight: 600, maxWidth: 1024, minHeight: 600, minWidth: 1024, height: 600, width: 1024,
        isModal: true,
        initContent: function () {
            $('#filterObjectWindow').jqxWindow('focus');
        }
    });


    $('#filterObjectWindow').jqxWindow('close');
};

function initSplitter() {
    $('#mainSplitter').jqxSplitter({ width: "100%", height: "100%", panels: [{ size: 300, min: 100 }] });
}

function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);
 
        if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemType == 'Visible') {
            setViewItemVisibility(viewItem);
        }
        else if (viewItem.ViewItemType == 'Content') {
            setViewItemContent(viewItem);
            if (viewItem.ViewItemId == 'ifrmFilterObject' && viewItem.LastValue != undefined) {
                $('#filterObjectWindow').jqxWindow('open');
            }
        }
        else if (viewItem.ViewItemType == 'Checked') {
            setViewItemChecked(viewItem);
        }
        else if (viewItem.ViewItemId == 'isListen') {
            if (viewItem.LastValue) {
                $('#isListen').jqxToggleButton('check');
            }
            else {
                $('#isListen').jqxToggleButton('unCheck');
            }
        }
        else if (viewItem.ViewItemId == 'isDirectionLeftRight') {
            if (viewItem.LastValue) {
                $('#directionCell').html('<i class="fa fa-long-arrow-right fa-5x" aria-hidden="true">');
            }
            else {
                $('#directionCell').html('<i class="fa fa-long-arrow-left fa-5x" aria-hidden="true">');
            }
            
        }
        else if (viewItem.ViewItemType == 'Command' && viewItem.ViewItemId == 'CloseFilterObjectWindow') {
            $('#filterObjectWindow').jqxWindow('close');
        }
        else if (viewItem.ViewItemId == 'viewText') {
            document.title = viewItem.LastValue;
        }
        
    }

    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");

   
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}