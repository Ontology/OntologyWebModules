﻿var dataSource;



function init() {
    $('body').openPageLoader();
    testWebSocket();
    initControls();
}

function initControls() {
    kendo.culture("de-DE");
}

function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);
        if (viewItem.ViewItemId == 'loginSuccess') {
            if (viewItem.LastValue) {
                $('body').closePageLoader();
                $('#example').show();
            }
            else {
                $('body').closePageLoader();
                
            }

        }
        else if (viewItem.ViewItemId == 'schedulerDataSource') {
            var scheduler = $("#scheduler").kendoScheduler({
                date: new Date(),
                startTime: new Date("2000/1/1 07:00 AM"),
                majorTimeHeaderTemplate: kendo.template("<strong>#=kendo.toString(date, 'HH')#</strong><sup>00</sup>"),
                height: '100%',
                views: [
                    { type: "day", dateHeaderTemplate: kendo.template("<strong>#=kendo.toString(date, 'dd.MM')#</strong>") },
                    { type: "workWeek", selected: true, dateHeaderTemplate: kendo.template("<strong>#=kendo.toString(date, 'ddd dd.MM')#</strong>") },
                    { type: "week", dateHeaderTemplate: kendo.template("<strong>#=kendo.toString(date, 'ddd dd.MM')#</strong>") },
                    { type: "month", dateHeaderTemplate: kendo.template("<strong>#=kendo.toString(date, 'ddd dd.MM')#</strong>") }
                ],
                dataSource: viewItem.LastValue,
                    filter: {
                        logic: "or",
                        filters: [
                            { field: "ownerId", operator: "eq", value: 1 },
                            { field: "ownerId", operator: "eq", value: 2 }
                        ]
                    },
                add: function (e) {
                        console.log("Add", e.event.title);
                },
                save: function (e) {
                    console.log("Saving", e.event.title);
                    SendPropertyChangeWithValue("eventToSave", "other", "other", e.event);
                    e.preventDefault();
                },
                change: function (e) {
                    var id = e.event.taskId;

                    SendPropertyChangeWithValue("selectId", "other", "other", id);
                },
                edit: function (e) {
                    var id = e.event.taskId;

                    SendPropertyChangeWithValue("selectId", "other", "other", id);
                }
            });

            console.log(scheduler);
        }
        else if (viewItem.ViewItemId == 'closeEventWindow') {
            $('.k-window').hide();
        }
        else if (viewItem.ViewItemId == 'eventToSave') {
            $("#scheduler").data("kendoScheduler").addEvent(viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'selectId') {
            var scheduler = $("#scheduler").data("kendoScheduler");
            scheduler.select([viewItem.LastValue]);
        }
    }

    //websocket.close();
}


function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams();

   
}


function onClose(evt) {
    $('body').closePageLoader();
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}