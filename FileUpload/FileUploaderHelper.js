﻿var acceptString;

function init() {
    
    $('#writeState').kendoProgressBar({
        type: "percent",
        animation: {
            duration: 100
        }
    })
    $('#uploadButton').kendoButton();
    testWebSocket();
    $('#uploadButton').click(function () {
        sendFiles();
    });
    $('#filename').change(function () {
        printFiles();
    });

    acceptString = getUrlParameter("mimeAccept")
    if (acceptString != undefined) {
        $('#filename').attr('accept', acceptString);
    }
}

function onMessage(evt) {

    ontoLog('<span style="color: blue;">Data: ' + evt.data + '</span>');
    var objJSON = eval("(function(){return " + evt.data + ";} )()");



    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemId == 'IsSuccessful_Login') {
            if (!objJSON.LastValue) {

                return;
            }

        }
        else if (viewItem.ViewItemId == 'UploadResult') {
            WriteResultToScreen(objJSON.LastValue);
        }
        else if (viewItem.ViewItemId == 'writeState') {
            var percent = 100 / viewItem.LastValue.ByteCount * viewItem.LastValue.BytesDone;
            if (percent % 20 == 0) {
                $('#' + viewItem.ViewItemId).data('kendoProgressBar').value(percent);
            }
            
        }
        else if (viewItem.ViewItemId == 'filename' && viewItem.ViewItemType == 'Enable') {
            document.getElementById(viewItem.ViewItemId).disabled = !viewItem.LastValue;
        }
        else if (viewItem.ViewItemId == 'uploadButton' && viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
            
        }

    }


    //websocket.close();
}

function SendCommandUpload(enable, count) {
    var sendJson = {
        "MessageType": "Command",
        "Command": "UploadFile",
        "Switch": enable,
        "Count": count
    };
    websocket.send(JSON.stringify(sendJson));

}

function SendCommandFilesSelected(files) {
    var sendJson = {
        "MessageType": "Command",
        "Command": "FilesSelected",
        "FileItems": files
    };
    websocket.send(JSON.stringify(sendJson));

}

function SendCommandUploadFinished() {
    SendStringCommandWithoutParameter("upload.finished")
}

function onOpen(evt) {
    ontoLog("CONNECTED");


    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

function printFiles() {
    var tableItem = document.getElementById('resultTable');
    var files = new Array(document.getElementById('filename').files.length);
    tableItem.innerHTML = "";
    console.log(document.getElementById('filename').files);
    for (ix = 0; ix < document.getElementById('filename').files.length; ix++) {
        var file = document.getElementById('filename').files[ix];
        files[ix] = {
            originalName: file.name,
            fileType: file.type
        }

        var trItem = document.createElement('tr');

        tableItem.appendChild(trItem);

        var tdItemName = document.createElement('td');
        tdItemName.innerHTML = file.name;
        trItem.appendChild(tdItemName);

        var tdItemSuccess = document.createElement('td');
        tdItemSuccess.id = 'uploadSuccess' + ix;
        console.log(tdItemSuccess);
        trItem.appendChild(tdItemSuccess);
    }
    SendCommandFilesSelected(files);
}

function sendFiles() {

    SendCommandUpload(true, document.getElementById('filename').files.length);
    for (var ix = 0; ix < document.getElementById('filename').files.length; ix++) {
        var file = document.getElementById('filename').files[ix];

        console.info('file ' + ix + ' ' + file);
        var reader = new FileReader();
        var rawData = new ArrayBuffer();

        reader.loadend = function () {

        }
        reader.onload = function (e) {
            console.log(e);
            rawData = e.target.result;


            websocket.send(rawData);
        }

        reader.readAsArrayBuffer(file);
        sleepFor(500);
    }
    
}

function sleepFor(sleepDuration) {
    var now = new Date().getTime();
    while (new Date().getTime() < now + sleepDuration) { /* do nothing */ }
}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}