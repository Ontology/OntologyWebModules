﻿function init() {


    testWebSocket();

    $('#partnerTabs').kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
    });

    $('#isListenPlz').click(function () {
        SendStringCommand('clicked.isListenPlz');
    });

    $('#isListenOrt').click(function () {
        SendStringCommand('clicked.isListenOrt');
    });

    $('#isListenLand').click(function () {
        SendStringCommand('clicked.isListenLand');
    });

    $('#isListenGeschlecht').click(function () {
        SendStringCommand('clicked.isListenGeschlecht');
    });

    $('#isListenFamilienstand').click(function () {
        SendStringCommand('clicked.isListenFamilienstand');
    });

    $('#isListenGebursort').click(function () {
        SendStringCommand('clicked.isListenGebursort');
    });

    $('#isListenSozialsversicherungsnummer').click(function () {
        SendStringCommand('clicked.isListenSozialsversicherungsnummer');
    });

    $('#isListenETin').click(function () {
        SendStringCommand('clicked.isListenETin');
    });

    $('#isListenIndentifikationsNr').click(function () {
        SendStringCommand('clicked.isListenIndentifikationsNr');
    });

    $('#isListenSteuernummer').click(function () {
        SendStringCommand('clicked.isListenSteuernummer');
    });

    $('#inpName').on('input', function () {
        SendPropertyChangeWithValue('inpNameSave', 'Content', 'Input', $('#inpName').val());
    });

    $('#inpStrasse').on('input', function () {
        SendPropertyChangeWithValue('inpStrasseSave', 'Content', 'Input', $('#inpStrasse').val());
    });

    $('#inpPostfach').on('input', function () {
        SendPropertyChangeWithValue('inpPostfachSave', 'Content', 'Input', $('#inpPostfach').val());
    });

    $('#inpVorname').on('input', function () {
        SendPropertyChangeWithValue('inpVornameSave', 'Content', 'Input', $('#inpVorname').val());
    });

    $('#inpNachname').on('input', function () {
        SendPropertyChangeWithValue('inpNachnameSave', 'Content', 'Input', $('#inpNachname').val());
    });

    $("#datepGeburtsdatum").kendoDatePicker({
        format: 'dd.MM.yyyy',
        change: function () {
            var value = this.value();
            console.log('Geburtsdatum', value);
            SendPropertyChangeWithValue('datepGeburtsdatum', 'Content', 'DateTimeInput', value);
        }
    });
    $("#datepTodesdatum").kendoDatePicker({
        format: 'dd.MM.yyyy',
        change: function () {
            var value = this.value();
            console.log('Todesdatum', value);
            SendPropertyChangeWithValue('datepTodesdatum', 'Content', 'DateTimeInput', value);
        }
    });

    $("#comToolBar").kendoToolBar({
        items: [
            {
                template: '<label>Telefon:</label>'
            },
            {
                template: '<button id="isListenTelephone"><i class="fa fa-assistive-listening-systems" aria-hidden="true"></i></button>'
            },
            { type: "separator" },
            {
                template: '<label>Fax:</label>'
            },
            {
                template: '<button id="isListenFax"><i class="fa fa-assistive-listening-systems" aria-hidden="true"></i></button>'
            },
            { type: "separator" },
            {
                template: '<label>Email:</label>'
            },
            {
                template: '<button id="isListenEmail"><i class="fa fa-assistive-listening-systems" aria-hidden="true"></i></button>'
            },
            { type: "separator" },
            {
                template: '<label>Url:</label>'
            },
            {
                template: '<button id="isListenUrl"><i class="fa fa-assistive-listening-systems" aria-hidden="true"></i></button>'
            },
            { type: "separator" },
        ]
    });

    $('#isListenTelephone').click(function () {
        SendStringCommand('clicked.isListenTelephone');
    });

    $('#isListenFax').click(function () {
        SendStringCommand('clicked.isListenFax');
    });

    $('#isListenEmail').click(function () {
        SendStringCommand('clicked.isListenEmail');
    });

    $('#isListenUrl').click(function () {
        SendStringCommand('clicked.isListenUrl');
    });
}

function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);

        if (viewItem.ViewItemClass == 'Input') {
            setViewItemContent(viewItem);
        }
        if (viewItem.ViewItemClass == 'DateTimeInput' && viewItem.ViewItemType == 'Content') {
            $('#' + viewItem.ViewItemId).data("kendoDatePicker").value(viewItem.LastValue);
                
        }
        if (viewItem.ViewItemId == 'viewText') {
            document.title = viewItem.LastValue;
        }
        if (viewItem.ViewItemId == 'isListenLand' ||
            viewItem.ViewItemId == 'isListenOrt' ||
            viewItem.ViewItemId == 'isListenPlz' || 
            viewItem.ViewItemId == 'isListenGeschlecht' || 
            viewItem.ViewItemId == 'isListenFamilienstand' || 
            viewItem.ViewItemId == 'isListenGebursort' || 
            viewItem.ViewItemId == 'isListenSozialsversicherungsnummer' || 
            viewItem.ViewItemId == 'isListenETin' || 
            viewItem.ViewItemId == 'isListenIndentifikationsNr' ||
            viewItem.ViewItemId == 'isListenSteuernummer') {
            setButtonIcon(viewItem);
        }
        else if (viewItem.ViewItemType == 'DataSource' && viewItem.ViewItemClass == 'Grid') {
            console.info(viewItem.LastValue);
            //createGrid(viewItem.LastValue);

            var dataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: viewItem.LastValue.url,
                        dataType: viewItem.LastValue.datatype
                    }
                },
                pageSize: viewItem.LastValue.pagesize
            });

            $("#comGrid").kendoGrid({
                dataSource: dataSource,
                height: '100%',
                width: '100%',
                groupable: false,
                sortable: true,
                autoBind: false,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 20
                },
                columns: [
                    { field: "Type", width: 100, filterable: true },
                    { field: "Value", filterable: true }
                ],
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to"
                        }
                    }
                }
            });

            setTimeout(function () {
                dataSource.read();
            }, 500);

        }
        else if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemId == 'isListenTelephone') {
            if (viewItem.LastValue) {
                $('#isListenTelephone').addClass('k-primary');
            }
            else {
                $('#isListenTelephone').removeClass('k-primary');
            }
        }
        else if (viewItem.ViewItemId == 'isListenFax') {
            if (viewItem.LastValue) {
                $('#isListenFax').addClass('k-primary');
            }
            else {
                $('#isListenFax').removeClass('k-primary');
            }
        }
        else if (viewItem.ViewItemId == 'isListenEmail') {
            if (viewItem.LastValue) {
                $('#isListenEmail').addClass('k-primary');
            }
            else {
                $('#isListenEmail').removeClass('k-primary');
            }
        }
        else if (viewItem.ViewItemId == 'isListenUrl') {
            if (viewItem.LastValue) {
                $('#isListenUrl').addClass('k-primary');
            }
            else {
                $('#isListenUrl').removeClass('k-primary');
            }
        }
        else if (viewItem.ViewItemId == 'comGrid' && viewItem.ViewItemType == 'AddRow') {
            var grid = $("#comGrid").data("kendoGrid");
            grid.dataSource.add(viewItem.LastValue);
        }
    }

    //websocket.close();
}

function setButtonIcon(viewItem) {
    if (viewItem.LastValue == true) {
        $('#' + viewItem.ViewItemId).html('<i class="fa fa-assistive-listening-systems" aria-hidden="true"></i>')
        $('#' + viewItem.ViewItemId).removeClass('k-default');
        $('#' + viewItem.ViewItemId).addClass('k-primary');
    }
    else {
        $('#' + viewItem.ViewItemId).html('<i class="fa fa-circle-thin" aria-hidden="true"></i>')
        $('#' + viewItem.ViewItemId).removeClass('k-primary');
        $('#' + viewItem.ViewItemId).addClass('k-default');
        
    }
    
}

function onOpen(evt) {
    ontoLog("CONNECTED");

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}