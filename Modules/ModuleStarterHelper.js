﻿var dataAdapter;
var columnConfig;

function resizeGrid() {
    $('#grid').jqxGrid('autoresizecolumns');
}
function init() {


    testWebSocket();
}

function onMessage(evt) {

    ontoLog('<span style="color: blue;">Data: ' + evt.data + '</span>');
    var objJSON = eval("(function(){return " + evt.data + ";} )()");
    console.info(evt.data);


    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemType == 'DataSource' && viewItem.ViewItemClass == 'Grid') {
            console.info(viewItem.LastValue);
            //createGrid(viewItem.LastValue);

            var dataSource = JSON.parse(JSON.stringify(viewItem.LastValue))

            dataAdapter = createDataAdapter(dataSource)

            if (dataAdapter != undefined && columnConfig != undefined) {
                GetColumnConfig();

                $('#grid').createGrid(dataAdapter, columnConfig, "Module");
                $('#grid').on('cellselect', function (event) {
                    // event arguments.
                    var args = event.args;
                    // column data field.
                    var datafield = event.args.datafield;
                    // row's bound index.
                    var rowBoundIndex = args.rowindex;
                    // new cell value.
                    var value = args.newvalue;
                    // old cell value.
                    var oldvalue = args.oldvalue;

                    var data = $('#' + viewItem.ViewItemId).jqxGrid('getrowdata', rowBoundIndex);

                    var sendData = JSON.stringify({ "Id": data.IdRow, "ColumnName": datafield, "RowId": rowBoundIndex });
                    SendPropertyChangeWithValue(viewItem.ViewItemId, "SelectedIndex", "Grid", sendData);
                });
            }

        }
        else if (viewItem.ViewItemType == 'ColumnConfig' && viewItem.ViewItemClass == 'Grid') {
            dataAdapter = null;
            columnConfig = JSON.parse(JSON.stringify(viewItem.LastValue.ColumnList))
        }
        else if (viewItem.ViewItemId == 'reloadGrid') {
            dataAdapter.dataBind();
        }
        else if (viewItem.ViewItemClass == 'Grid' && viewItem.ViewItemType == 'Change') {
            changeCellOfGrid(viewItem);
        }
        else if (viewItem.ViewItemId == 'viewText') {
            document.title = viewItem.LastValue;
        }
    }



    //websocket.close();
}

function GetColumnConfig() {
    for (var i in columnConfig) {
        if (columnConfig[i].datafield == 'Url') {
            columnConfig[i].cellsrenderer = linkrenderer;
        }
    }
}

function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams();

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}