﻿function init() {

    testWebSocket();
    initNavigationBar();
    initToolbar();
    _createWindow();
}

function initNavigationBar() {
    // Create jqxNavigationBar.
    $("#jqxNavigationBar").jqxNavigationBar({ width: '100%', expandedIndexes: ['0'], expandMode: 'multiple' });
    // Focus jqxNavigationBar.
    $("#jqxNavigationBar").jqxNavigationBar('focus');
}
function initToolbar() {
    
    $("#relationBar").jqxToolBar({
        width: '100%', height: 40, tools: 'custom custom custom',
        initTools: function (type, index, tool, menuToolIninitialization) {
            if (type == "toggleButton") {
                var icon = $("<div class='jqx-editor-toolbar-icon jqx-editor-toolbar-icon-" + theme + " buttonIcon'></div>");
            }
            switch (index) {
                case 0:
                    var button = $("<div id='isListen'>" + '<i class="fa fa-assistive-listening-systems" aria-hidden="true"></i>' + "</div>");
                    tool.append(button);
                    button.jqxToggleButton({ width: 19, height: 20 });
                    $('#isListen').on('click', function () {
                        SendPropertyChangeWithValue('isListen', 'Other', 'Other', $("#isListen").jqxToggleButton('toggled'));
                    });
                    break;
                case 1:
                    var numericUpDown = $("<div id='orderId'></div>");
                    tool.append(numericUpDown);
                    tool.jqxNumberInput({ width: '100px', height: '25px', symbol: "", min: 0, spinButtons: true, decimalDigits: 0, digits: 8 });
                    tool.on('change', function (event) {
                        var value = event.args.value;
                        SendPropertyChangeWithValue("orderId", "Content", "NumericInput", value);
                    });
                    break;
                case 2:
                    var button = $("<div id='nextOrderId'>" + '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>' + "</div>");
                    tool.append(button);
                    button.jqxToggleButton({ height: 19, width: 20 });
                    $("#nextOrderId").on('click', function () {
                        var toggled = $("#nextOrderId").jqxToggleButton('toggled');
                        SendPropertyChangeWithValue('nextOrderId', 'Checked', 'ToggleButton', toggled);
                    });
                    break;


            }
        }
    });
}

function _createWindow() {
    var jqxWidget = $('#jqxNavigationBar');

    var offset = jqxWidget.offset();
    $('#window').jqxWindow({
        rtl: true,
        position: { x: offset.left, y: offset.top },
        showCollapseButton: true, maxHeight: 200, maxWidth: 600, minHeight: 100, minWidth: 200, height: 210, width: 344,
        initContent: function () {
            $('#window').jqxWindow('close');
        }
    });
};

function onMessage(evt) {

    console.info(evt.data);
    var objJSON = eval("(function(){return " + evt.data + ";} )()");



    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemId == 'isListen') {
            if (viewItem.LastValue) {
                $('#isListen').jqxToggleButton('check');
            }
            else {
                $('#isListen').jqxToggleButton('unCheck');
            }
        }
        else if (viewItem.ViewItemId == 'Url_ClassAttributeNodes') {
            jQuery.getJSON(viewItem.LastValue, function (data) {
                createNode('attributeList', data.items, 0, true);
            });

        }
        else if (viewItem.ViewItemId == 'Url_LeftRightNodes') {
            jQuery.getJSON(viewItem.LastValue, function (data) {
                createNode('leftRightList', data.items, 1, true);
            });

        }
        else if (viewItem.ViewItemId == 'Url_LeftRightInformalNodes') {
            jQuery.getJSON(viewItem.LastValue, function (data) {
                createNode('leftRightInformalList', data.items, 3, false);
            });

        }
        else if (viewItem.ViewItemId == 'Url_RightLeftNodes') {
            jQuery.getJSON(viewItem.LastValue, function (data) {
                createNode('rightLeftList', data.items, 2, false);
            });

        }
        else if (viewItem.ViewItemId == 'Url_Others') {
            jQuery.getJSON(viewItem.LastValue, function (data) {
                createNode('otherBackwardList', data.items, 4, false);
            });

        }
        else if (viewItem.ViewItemId == 'Text_NodeAttributes') {
            $('#nodeAttribute').text(viewItem.LastValue);

        }
        else if (viewItem.ViewItemId == 'Text_NodeLeftRight') {
            $('#nodeLeftRight').text(viewItem.LastValue);

        }
        else if (viewItem.ViewItemId == 'Text_NodeRightLeft') {
            $('#nodeRightLeft').text(viewItem.LastValue);

        }
        else if (viewItem.ViewItemId == 'Text_NodeOtherBackward') {
            $('#nodeOtherBackward').text(viewItem.LastValue);

        }
        else if (viewItem.ViewItemId == 'Text_NodeLeftRightInformal') {
            $('#nodeLeftRightInformal').text(viewItem.LastValue);

        }
        else if (viewItem.ViewItemId == 'listenToggleNode' && viewItem.LastValue != undefined) {
            $('#' + viewItem.LastValue.Id).toggleListenMode(viewItem.LastValue.Text, viewItem.LastValue.ListenMode);
            $('#' + viewItem.LastValue.Id).on('click', function () {
                SendRelationNode(this);
            });
            $('#icon' + viewItem.LastValue.Id).html('<i class="' + viewItem.LastValue.Class + '" aria-hidden="true"></i>');
            $('#icon' + viewItem.LastValue.Id).css('backgroundColor', viewItem.LastValue.Color);
        }
        else if (viewItem.ViewItemId == 'jqxNavigationBar' && viewItem.ViewItemType == 'Change') {
            $('#' + viewItem.LastValue.Id).toggleListenMode(viewItem.LastValue.Text, viewItem.LastValue.ListenMode);
            $('#' + viewItem.LastValue.Id).on('click', function () {
                SendRelationNode(this);
            });
            $('#icon' + viewItem.LastValue.Id).html('<i class="' + viewItem.LastValue.Class + '" aria-hidden="true"></i>');
            $('#icon' + viewItem.LastValue.Id).css('backgroundColor', viewItem.LastValue.Color);
        }
        else if (viewItem.ViewItemClass == 'ContextMenu' && viewItem.ViewItemType == 'Content') {
            $('#' + viewItem.ViewItemId).createMenu(viewItem.LastValue, true, 3);
            $("#jqxNavigationBar").on('mousedown', function (event) {

                var rightClick = isRightClick(event) || $.jqx.mobile.isTouchDevice();
                if (rightClick) {

                    var scrollTop = $(window).scrollTop();
                    var scrollLeft = $(window).scrollLeft();
                    $('#' + viewItem.ViewItemId).jqxMenu('open', parseInt(event.clientX) + 5 + scrollLeft, parseInt(event.clientY) + 5 + scrollTop);
                    ontoLog($('#' + viewItem.ViewItemId));
                    return false;
                }
            });
        }
        else if (viewItem.ViewItemClass == 'ContextMenuEntry' && viewItem.ViewItemType == 'Change') {
            if (viewItem != null && viewItem != undefined) {
                $('#contextMenu').jqxMenu('disable', viewItem.LastValue.Id, !viewItem.LastValue.IsEnabled);
                if (viewItem.LastValue.IsVisible) {
                    $('#' + viewItem.LastValue.Id).show();
                }
                else {
                    $('#' + viewItem.LastValue.Id).hide();
                }

            }
        }
        else if (viewItem.ViewItemType == 'Enable') {
            //setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemType == 'Checked') {
            setViewItemChecked(viewItem);
        }
        else if (viewItem.ViewItemClass == 'IFrame' && viewItem.ViewItemType == 'Content') {
            $('#urlAttributeAdd').attr('src', viewItem.LastValue);
            $('#window').jqxWindow('open');
        }
        else if (viewItem.ViewItemId == 'CloseNewAttributeWindow') {
            $('#window').jqxWindow('close');
        }
        else if (viewItem.ViewItemId == 'viewText') {
            document.title = viewItem.LastValue;
        }

    }

    //websocket.close();
}

function createNode(idContainer, data, navBarIx, colorNode) {
    console.info(data);
    var nodeTable = document.getElementById(idContainer);

    nodeTable.innerHTML = '';
    if (data.length > 0) {
        $('#jqxNavigationBar').jqxNavigationBar('expandAt', navBarIx);
    }

    for (var ix in data) {
        var classAttributeNode = data[ix];

        var trItem = document.createElement('tr');
        nodeTable.appendChild(trItem);
        var tdItemIcon = document.createElement('td');
        tdItemIcon.style.width = '25px';

        var html = '';
        if (classAttributeNode.Icon == undefined ||
            classAttributeNode.Icon == '') {
            html += '';
        } else {
            html += '<i class="' + classAttributeNode.Icon + '" aria-hidden="true"></i>';
        }
        tdItemIcon.innerHTML = html;
        tdItemIcon.id = 'icon' + classAttributeNode.Id;
        trItem.appendChild(tdItemIcon);

        var tdItemNode = document.createElement('td');
        var buttonItem = document.createElement('button');
        buttonItem.id = classAttributeNode.Id;
        if (classAttributeNode.Color != 'none' && colorNode) {
            tdItemIcon.style.backgroundColor = classAttributeNode.Color;
        }
        buttonItem.setAttribute('NodeType', classAttributeNode.NodeType);

        buttonItem.innerHTML = classAttributeNode.Label;

        tdItemNode.appendChild(buttonItem);

        trItem.appendChild(tdItemNode);
        ontoLog('Label:' + classAttributeNode.Label);
        $('#' + buttonItem.id).jqxButton({ template: "link" });
        $('#' + buttonItem.id).on('click', function () {
            SendRelationNode(this);
        });
        ontoLog('Label-Done:' + classAttributeNode.Label)






    }
    //var spanItem 
    //<span id="000">Attributes</span>

}

function onOpen(evt) {
    ontoLog("CONNECTED");


    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

function SendRelationNode(node) {
    console.log(node);
    SendStringCommand('RelationNode_Selected', 'SelectedNode', node.id)

}


window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}