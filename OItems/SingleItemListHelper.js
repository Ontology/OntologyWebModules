﻿var itemTable;
var urlColumns;
var initialized = false;
var dataAdapter
var columnConfig
var timeout;
var idInstances = [];
var selectedRows = [];
var filterId;

var pagerrenderer = function () {
    var element = $("<div style='margin-top: 5px; width: 100%; height: 100%;'></div>");
    var datainfo = $("#grid").jqxGrid('getdatainformation');
    var paginginfo = datainfo.paginginformation;

    var guidLabel = $("<div style='padding: 1px; float: left;'><label id='guidLabel'>Guid:</label></div>")
    guidLabel.appendTo(element);

    var guidInput = $("<div style='padding: 1px; float: left;'><input id='inpCellGuid' /></div>")
    guidInput.jqxInput({ disabled: true });
    guidInput.appendTo(element);
    

    var btnCopy = $("<div style='padding: 1px; float: left;'><div id='copyButton' style='margin-left: 9px; width: 16px; height: 18px;'><i class='fa fa-files-o' aria-hidden='true'></i></div></div>");
    btnCopy.width(36);
    btnCopy.jqxButton();
    
    btnCopy.appendTo(element);
    btnCopy.click(function () {
        $("#inpCellGuid").copyValToClipboard();
    });
    
    var seperator = $("<div style='padding: 1px; float: left;'>|</div>")
    seperator.appendTo(element);

    // create navigation buttons.
    var leftButton = $("<div style='padding: 1px; float: left;'><div style='margin-left: 9px; width: 16px; height: 18px;'><i class='fa fa-chevron-circle-left' aria-hidden='true'></i></div></div>");
    leftButton.find('div').addClass('icon-arrow-left');
    leftButton.width(36);
    leftButton.jqxButton();

    var rightButton = $("<div style='padding: 1px; margin: 0px 3px; float: left;'><div style='margin-left: 9px; width: 16px; height: 18px;'><i class='fa fa-chevron-circle-right' aria-hidden='true'></i></div></div>");
    rightButton.find('div').addClass('icon-arrow-right');
    rightButton.width(36);
    rightButton.jqxButton();

    // append the navigation buttons to the container DIV tag.
    leftButton.appendTo(element);
    rightButton.appendTo(element);

    // create a page information label and append it to the container DIV tag.
    var label = $("<div style='font-size: 11px; margin: 2px 3px; font-weight: bold; float: left;'></div>");
    label.text("1-" + paginginfo.pagesize + ' of ' + datainfo.rowscount);
    label.appendTo(element);

    // navigate to the next page when the right navigation button is clicked.
    rightButton.click(function () {
        $("#grid").jqxGrid('gotonextpage');
        var datainfo = $("#grid").jqxGrid('getdatainformation');
        var paginginfo = datainfo.paginginformation;
        label.text(1 + paginginfo.pagenum * paginginfo.pagesize + "-" + Math.min(datainfo.rowscount, (paginginfo.pagenum + 1) * paginginfo.pagesize) + ' of ' + datainfo.rowscount);
    });

    // navigate to the previous page when the right navigation button is clicked.           
    leftButton.click(function () {
        $("#grid").jqxGrid('gotoprevpage');
        var datainfo = $("#grid").jqxGrid('getdatainformation');
        var paginginfo = datainfo.paginginformation;
        label.text(1 + paginginfo.pagenum * paginginfo.pagesize + "-" + Math.min(datainfo.rowscount, (paginginfo.pagenum + 1) * paginginfo.pagesize) + ' of ' + datainfo.rowscount);
    });
    // return the new pager element.

    return element;
}

var toggle = false;
function init() {

    $('body').openPageLoader();
    output = document.getElementById("output");
    testWebSocket();
    
    if (isActiveToolbar) {
        $('#applyItem').jqxButton({ width: 30, height: 30 })
        $('#applyItem').on('click', function () {
            if (!$('#applyItem').jqxButton('disabled')) {
                SendStringCommandWithoutParameter('AppliedObjectRows')
            }
            
        });

        $('#newItem').jqxButton({ width: 30, height: 30 })
        $('#newItem').on('click', function () {
            if (!$('#newItem').jqxButton('disabled')) {
                SendStringCommandWithoutParameter('NewItem')
            }
        });

        $('#deleteItem').jqxButton({ width: 30, height: 30 })
        $('#deleteItem').on('click', function () {
            if (!$('#deleteItem').jqxButton('disabled')) {
                SendStringCommandWithoutParameter('NewItem')
            }
        });

        $('#isListen').jqxToggleButton({ width: 30, height: 30 });
        $('#isListen').on('click', function () {
            if (!$('#isListen').jqxToggleButton('disabled')) {
                SendPropertyChangeWithValue('isListen', 'Other', 'Other', $("#isListen").jqxToggleButton('toggled'));
            }
        });

        $('#addToClipboard').jqxButton({ width: 30, height: 30 })
        $('#addToClipboard').on('click', function () {
            if (!$('#addToClipboard').jqxButton('disabled')) {
                SendStringCommandWithoutParameter('AddToClipboard')
            }
            
        });

        
    }

    createNewItemWindow()
    createEditNameWindow();
    createEditOrderIdWindow();
    createMessageWindow()

    $("#inpValueEditOrderId").jqxNumberInput({ width: '250px', height: '25px', symbol: "", min: 0, spinButtons: true, decimalDigits: 0, digits: 10 });
    $("#inpValueEditOrderId").on('change', function (event) {
        var value = event.args.value;
        SendPropertyChangeWithValue("inpValueEditOrderId", "Content", "NumericInput", value);
    });

    $("#inpValueEditName").jqxInput({ height: 25, width: 250, minLength: 1 });
    $("#txtGuid").jqxInput({ height: 25, width: 250, minLength: 1 });
    $("#txtGuid").jqxInput({ disabled: true });
    //$("#inpValueEditName").on('change', function (event) {
    //    var value = event.args.value;
    //    SendPropertyChangeWithValue("inpValueEditName", "Content", "Input", value);
    //});

    $("#inpValueEditName").bind("propertychange change keyup input paste", function(event){

        var value = $("#inpValueEditName").val();
        SendPropertyChangeWithValue("inpValueEditName", "Content", "Input", value);
    });

    $('#cpyGuid').jqxButton({ width: 30, height: 30 });
    $('#cpyGuid').on('click', function () {
        
        $("#txtGuid").copyValToClipboard();
        
    });

    $('#saveName').jqxButton({ width: 100, height: 30 });
    $('#saveName').on('click', function () {
        SendStringCommandWithoutParameter('SaveName')
    });

    $('#saveOrderId').jqxButton({ width: 100, height: 30 });
    $('#saveOrderId').on('click', function () {
        SendStringCommandWithoutParameter('SaveOrderId')
    });

    
}

//Creating the demo window
function createNewItemWindow() {
    var jqxWidget = $('#grid');
    var offset = jqxWidget.offset();
    $('#newItemWindow').jqxWindow({
        rtl: true,
        position: { x: 100, y: 100 },
        showCollapseButton: true, maxHeight: 700, maxWidth: 1224, minHeight: 600, minWidth: 1024, height: 700, width: 1224,
        isModal: true,
        initContent: function () {
            $('#newItemWindow').jqxWindow('focus');
        }
    });

   

    $('#newItemWindow').jqxWindow('close');
};

function createEditNameWindow() {
    var jqxWidget = $('#grid');
    var offset = jqxWidget.offset();
    $('#editNameItemWindow').jqxWindow({
        rtl: true,
        position: { x: offset.left, y: offset.top },
        showCollapseButton: true, maxHeight: 1400, maxWidth: 1400, minHeight: 100, minWidth: 200, height: 136, width: 420,
        initContent: function () {
            $('#editNameItemWindow').jqxWindow('close');
        }
    });

    

    $('#editNameItemWindow').jqxWindow('close');
};

function createEditOrderIdWindow() {
    var jqxWidget = $('#grid');
    var offset = jqxWidget.offset();
    $('#editOrderIdItemWindow').jqxWindow({
        rtl: true,
        position: { x: offset.left, y: offset.top },
        showCollapseButton: true, maxHeight: 1400, maxWidth: 1400, minHeight: 100, minWidth: 200, height: 138, width: 391,
        initContent: function () {
            $('#editOrderIdItemWindow').jqxWindow('close');
        }
    });

    

    $('#editOrderIdItemWindow').jqxWindow('close');
};

//Creating the demo window
function createMessageWindow() {
    $('#deleteWindowMessage').jqxWindow({
        position: { x: 100, y: 100 },
        showCollapseButton: true, maxHeight: 600, maxWidth: 1024, minHeight: 600, minWidth: 1024, height: 600, width: 1024,
        isModal: true,
        initContent: function () {
            $('#deleteWindowMessage').jqxWindow('focus');
        }
    });
    

    
    $('#deleteWindowMessage').jqxWindow('close');
};



function createContextMenu(dataSource) {

    //var dataAdapter = new $.jqx.dataAdapter(dataSource);
    //dataAdapter.dataBind();
    //var records = dataAdapter.getRecordsHierarchy('Id', 'ParentId', 'items', [{ name: 'Html', map: 'label' }]);

    var contextMenu = document.getElementById("contextMenu");
    var ul = document.createElement("ul");
    for (var ix in dataSource.localdata) {
        var contextMenuEntry = dataSource.localdata[ix];
        var li = document.createElement("li");
        li.id = contextMenuEntry.Id;
        ul.appendChild(li);

        li.innerHTML = contextMenuEntry.Html;
    }

    contextMenu.appendChild(ul);


    $("#contextMenu").jqxMenu({ width: '120px', height: '60px', autoOpenPopup: false, mode: 'popup' });
    $("#grid").on('mousedown', function (event) {
        var rightClick = isRightClick(event) || $.jqx.mobile.isTouchDevice();
        if (rightClick) {
            var scrollTop = $(window).scrollTop();
            var scrollLeft = $(window).scrollLeft();
            $('#contextMenu').jqxMenu('open', parseInt(event.clientX) + 5 + scrollLeft, parseInt(event.clientY) + 5 + scrollTop);
            return false;
        }
    });

    $('#contextMenu').on('itemclick', function (event) {
        // get the clicked LI element.
        var element = event.args;
        var id = element.id;
        SendStringCommand('ClickedMenuItem', 'Id', id);
    });

    // disable the default browser's context menu.
    $(document).on('contextmenu', function (e) {
        return false;
    });
    function isRightClick(event) {
        var rightclick;
        if (!event) var event = window.event;
        if (event.which) rightclick = (event.which == 3);
        else if (event.button) rightclick = (event.button == 2);
        return rightclick;
    }

    for (var ix in dataSource.localdata) {
        var contextMenuEntry = dataSource.localdata[ix];
        if (contextMenuEntry.IsVisible) {
            $('#' + contextMenuEntry.Id).show();
        }
        else {
            $('#' + contextMenuEntry.Id).hide();
        }

        $('#contextMenu').jqxMenu('disable', contextMenuEntry.Id, !contextMenuEntry.IsEnabled);
    }
}

function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);
        if (viewItem.ViewItemType == 'DataSource' && viewItem.ViewItemClass == 'Grid') {
            console.info(viewItem.LastValue);
            //createGrid(viewItem.LastValue);

            
            var dataSource = JSON.parse(JSON.stringify(viewItem.LastValue))

            dataAdapter = createDataAdapter(dataSource)

            testCreateGrid(viewItem, dataAdapter, columnConfig, "IdItem", "NameItem", pagerrenderer, function () {
                if (filterId != undefined && filterId != null) {
                    addGridFilter('grid', 'IdInstance', filterId);
                }
                else {
                    $('#grid').jqxGrid('clearfilters');
                }
                
            });
            
        }
        else if (viewItem.ViewItemId == 'isListen') {
            if (viewItem.LastValue) {
                $('#isListen').jqxToggleButton('check');
            }
            else {
                $('#isListen').jqxToggleButton('unCheck');
            }
        }
        else if (viewItem.ViewItemType == 'ColumnConfig' && viewItem.ViewItemClass == 'Grid') {
            columnConfig = JSON.parse(JSON.stringify(viewItem.LastValue.ColumnList))

            testCreateGrid(viewItem, dataAdapter, columnConfig, "IdItem", "NameItem", pagerrenderer, function () {
                if (filterId != undefined && filterId != null) {
                    addGridFilter('grid', 'IdInstance', filterId);
                }
                else {
                    $('#grid').jqxGrid('clearfilters');
                }

            });
        }
        else if (viewItem.ViewItemId == 'IsSuccessful_Login') {
            if (!viewItem.LastValue) {

                return;
            }

        }
        else if (viewItem.ViewItemId == 'Url_ContextMenu') {
            $.getJSON(viewItem.LastValue, function (data) {
                console.log('data:', data);
                createContextMenu(data);
            });
        }
        else if (viewItem.ViewItemId == 'ContextMenuEntry_ContextMenuEntryChange') {
            if (viewItem != null && viewItem != undefined) {
                $('#contextMenu').jqxMenu('disable', viewItem.LastValue.Id, !viewItem.LastValue.IsEnabled);
                if (viewItem.LastValue.IsVisible) {
                    $('#' + viewItem.LastValue.Id).show();
                }
                else {
                    $('#' + viewItem.LastValue.Id).hide();
                }

            }
        }
        else if (viewItem.ViewItemId == 'refreshGrid') {
            
            if (dataAdapter != undefined && columnConfig != undefined) {
                dataAdapter.dataBind();
            }
            
        }
        else if (viewItem.ViewItemId == 'Url_NewItem') {
            if (viewItem.LastValue != undefined && viewItem.LastValue != '') {
                $('#newFrame').attr('src', viewItem.LastValue)
                $('#newItemWindow').jqxWindow('open');
            }

        }
        else if (viewItem.ViewItemType == 'Command') {
            if (viewItem.ViewItemId == 'OpenEditOrderId') {
                $('#editOrderIdItemWindow').jqxWindow('open');
            }
            else if (viewItem.ViewItemId == 'CloseEditOrderId') {
                $('#editOrderIdItemWindow').jqxWindow('close');
            }
            else if (viewItem.ViewItemId == 'OpenEditName') {
                $('#editNameItemWindow').jqxWindow('open');
            }
            else if (viewItem.ViewItemId == 'CloseEditName') {
                $('#editNameItemWindow').jqxWindow('close');
            }
            else if (viewItem.ViewItemId == commandShowLoader) {
                $('body').openPageLoader();
            }
            else if (viewItem.ViewItemId == commandHideLoader) {
                $('body').closePageLoader();
            }
        }
        else if (viewItem.ViewItemType == 'Enable') {
            setViewItemEnableState(viewItem);
        }
        else if (viewItem.ViewItemType == 'Content') {
            setViewItemContent(viewItem);
        }
        else if (viewItem.ViewItemClass == 'Grid' && viewItem.ViewItemType == 'Change') {
            changeCellOfGrid(viewItem);
        }
        else if (viewItem.ViewItemId == 'editMessageName') {
            $('#' + viewItem.ViewItemId).html(viewItem.LastValue);
        }
        else if (viewItem.ViewItemClass == 'Grid' && viewItem.ViewItemType == 'AddRow') {
            $('#' + viewItem.ViewItemId).addRow(viewItem.ViewItemValue);
        }
        else if (viewItem.ViewItemId == 'viewText') {
            document.title = viewItem.LastValue;
        }
        else if (viewItem.ViewItemId == 'grid' && viewItem.ViewItemType == 'Filter') {
            filterId = viewItem.LastValue;
            addGridFilter('grid', 'IdInstance', filterId);
        }
        
    }

    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");


    $('#newItemWindow').on('open', function (event) {
        SendStringCommandWithoutParameter('OpenedNewItemWindow');
    });

    $('#newItemWindow').on('close', function (event) {
        SendStringCommandWithoutParameter('ClosedNewItemWindow');
    });

    $('#editNameItemWindow').on('open', function (event) {
        SendStringCommandWithoutParameter('OpenedEditNameItemWindow');
    });
    $('#editNameItemWindow').on('close', function (event) {
        SendStringCommandWithoutParameter('ClosedEditNameItemWindow');
    });

    $('#editOrderIdItemWindow').on('open', function (event) {
        SendStringCommandWithoutParameter('OpenedEditOrderIdItemWindow');
    });
    $('#editOrderIdItemWindow').on('close', function (event) {
        SendStringCommandWithoutParameter('ClosedEditOrderIdItemWindow');
    });

    $('#deleteWindowMessage').on('open', function (event) {
        SendStringCommandWithoutParameter('OpenedDeleteWindowMessage');
    });
    $('#deleteWindowMessage').on('close', function (event) {
        SendStringCommandWithoutParameter('ClosedDeleteWindowMessage');
    });
    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}