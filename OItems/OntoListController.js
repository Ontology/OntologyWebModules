﻿var dataAdapter
var columnConfig

function init() {


    output = document.getElementById("output");
    testWebSocket();
    initControls();


}


function initControls() {
    $('#addToClipboard').jqxButton({ width: 30, height: 30 })
    $('#addToClipboard').on('click', function () {
        SendStringCommandWithoutParameter('AddtoClipboard')
    });
}

function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);
        
        if (viewItem.ViewItemType == 'DataSource' && viewItem.ViewItemClass == 'Grid') {
            console.info(viewItem.LastValue);
            //createGrid(viewItem.LastValue);

            var dataSource = JSON.parse(JSON.stringify(viewItem.LastValue))

            dataAdapter = createDataAdapter(dataSource)

            $('#grid').createGrid(dataAdapter, columnConfig, "Name");
            $('#grid').on('cellselect', function (event) {
                // event arguments.
                var args = event.args;
                // column data field.
                var datafield = event.args.datafield;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
                // new cell value.
                var value = args.newvalue;
                // old cell value.
                var oldvalue = args.oldvalue;

                var data = $('#' + viewItem.ViewItemId).jqxGrid('getrowdata', rowBoundIndex);

                var sendData = JSON.stringify({ "Id": data.RowId, "ColumnName": datafield, "GUID": rowBoundIndex });
                SendPropertyChangeWithValue(viewItem.ViewItemId, "SelectedIndex", "Grid", sendData);
            });
            $('#' + viewItem.ViewItemId).on('cellvaluechanged', function (event) {
                // event arguments.
                var args = event.args;
                // column data field.
                var datafield = event.args.datafield;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
                // new cell value.
                var value = args.newvalue;
                // old cell value.
                var oldvalue = args.oldvalue;

                var data = $('#' + viewItem.ViewItemId).jqxGrid('getrowdata', rowBoundIndex);

                if (datafield == 'ItemApply') {
                    if (value == true) {
                        SendStringCommand("AppliedObjectRow", "Id", data.GUID);
                    }
                    else {
                        SendStringCommand("UnAppliedObjectRow", "Id", data.GUID);
                    }
                }
                ontoLog(args);
            });

        }
        else if (viewItem.ViewItemType == 'ColumnConfig' && viewItem.ViewItemClass == 'Grid') {
            columnConfig = JSON.parse(JSON.stringify(viewItem.LastValue.ColumnList))

           
        }
        else if (viewItem.ViewItemId == 'refreshGrid') {
            dataAdapter.dataBind();
        }
        else if (viewItem.ViewItemClass == 'Grid' && viewItem.ViewItemType == 'RemoveItem') {
            $('#' + viewItem.ViewItemId).removeRow(viewItem);
        }
        else if (viewItem.ViewItemClass == 'Grid' && viewItem.ViewItemType == "AddOItem") {
            $('#' + viewItem.ViewItemId).addORow(viewItem);
        }
    }

    //websocket.close();
}



function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams();
    
    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}