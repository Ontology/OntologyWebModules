﻿function init() {


    output = document.getElementById("output");
    testWebSocket();
    initControls();
}

function initControls() {
    $('.k-primary').kendoButton();

    $('#newItemWindow').kendoWindow({
        width: '600px',
        height: '600px',
        title: 'New Item',
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ]
    }).data("kendoWindow").center();

    $('#editNameItemWindow').kendoWindow({
        width: '350px',
        height: '260px',
        title: 'Edit Name',
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ]
    }).data("kendoWindow").center();
    //$('#editNameItemWindow').data("kendoWindow").open();

    $("#inpValueEditOrderId").kendoNumericTextBox({ format: 'n0' });

    $('#editOrderIdItemWindow').kendoWindow({
        width: '160px',
        height: '160px',
        title: 'OrderId',
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ]
    }).data("kendoWindow").center();

    
    $('#deleteWindowMessage').kendoWindow({
        width: '160px',
        height: '160px',
        title: 'Delete',
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ]
    }).data("kendoWindow").center();
   
}

function onMessage(evt) {

    ontoLog(evt.data)
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];
        console.log(viewItem);
        if (viewItem.ViewItemType == 'DataSource' && viewItem.ViewItemClass == 'Grid') {
            console.info(viewItem.LastValue);
            //createGrid(viewItem.LastValue);

            var dataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: viewItem.LastValue.url,
                        dataType: "json"
                    }
                },
                pageSize: viewItem.LastValue.pagesize
            });
            
            $("#grid").kendoGrid({
                dataSource: dataSource,
                height: '100%',
                width: '100%',
                groupable: false,
                sortable: true,
                autoBind: false,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 20
                },
                columns: [
                    { field: "NameInstance"},
                    { field: "NameClass", width:100, filterable: false }
                ],
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to"
                        }
                    }
                }
            });

            setTimeout(function () {
                dataSource.read();
            }, 500);
            

            var slide = kendo.fx($("#slide-in-share")).slideIn("left");
            var slideVisible = true;

            $("#slide-in-handle").click(function (e) {
                if (slideVisible) {
                    slide.reverse();
                } else {
                    slide.play();
                }
                slideVisible = !slideVisible;
                e.preventDefault();
            });
            

        }
        else if (viewItem.ViewItemId == 'isListen') {
            
        }
        else if (viewItem.ViewItemType == 'ColumnConfig' && viewItem.ViewItemClass == 'Grid') {
            
        }
        else if (viewItem.ViewItemId == 'IsSuccessful_Login') {
            

        }
        else if (viewItem.ViewItemId == 'Url_ContextMenu') {
            
        }
        else if (viewItem.ViewItemId == 'ContextMenuEntry_ContextMenuEntryChange') {
            
        }
        else if (viewItem.ViewItemId == 'refreshGrid') {
            var grid = $('#grid').data('kendoGrid');
            grid.dataSource.read();
        }
        else if (viewItem.ViewItemId == 'Url_NewItem') {
            
        }
        else if (viewItem.ViewItemType == 'Command') {
            if (viewItem.ViewItemId == 'OpenEditOrderId') {
             
            }
            else if (viewItem.ViewItemId == 'CloseEditOrderId') {
             
            }
            else if (viewItem.ViewItemId == 'OpenEditName') {
             
            }
            else if (viewItem.ViewItemId == 'CloseEditName') {
             
            }
        }
        else if (viewItem.ViewItemType == 'Enable') {

        }
        else if (viewItem.ViewItemType == 'Content') {

        }
        else if (viewItem.ViewItemClass == 'Grid' && viewItem.ViewItemType == 'Change') {
            
        }
        else if (viewItem.ViewItemId == 'editMessageName') {
            $('#' + viewItem.ViewItemId).html(viewItem.LastValue);
        }
        else if (viewItem.ViewItemClass == 'Grid' && viewItem.ViewItemType == 'AddRow') {
            
        }
        else if (viewItem.ViewItemId == 'viewText') {
            document.title = viewItem.LastValue;
        }
    }

    //websocket.close();
}

function onOpen(evt) {
    ontoLog("CONNECTED");


}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}