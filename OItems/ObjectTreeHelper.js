﻿
kendo.ui.TreeView.prototype.getCheckedItems = (function () {

    function getCheckedItems() {
        var nodes = this.dataSource.view();
        return getCheckedNodes(nodes);
    }

    function getCheckedNodes(nodes) {
        var node, childCheckedNodes;
        var checkedNodes = [];

        for (var i = 0; i < nodes.length; i++) {
            node = nodes[i];
            if (node.checked) {
                checkedNodes.push(node.id);
            }

            // to understand recursion, first
            // you must understand recursion
            if (node.hasChildren) {
                childCheckedNodes = getCheckedNodes(node.children.view());
                if (childCheckedNodes.length > 0) {
                    checkedNodes = checkedNodes.concat(childCheckedNodes);
                }
            }

        }

        return checkedNodes;
    }

    return getCheckedItems;
})();

function init() {

    
    ontoLog('init classTree');
    testWebSocket();
    initTree();
    initContextMenu();
}

function initContextMenu() {
    $("#menu").kendoContextMenu({
        orientation: "vertical",
        target: "#objectTreeView",
        animation: {
            open: { effects: "fadeIn" },
            duration: 500
        },
        select: function (e) {
            // Do something on select
            if (e.item.id == 'addSubItem') {
                SendStringCommandWithoutParameter('addSubItem');
                
            }
        }
    });
}

function initTree() {
    $("#treeToolbar").kendoToolBar({
        items: [
            {
                template: "<input id='dropdRelationType' style='width: 150px;' />",
                overflow: "never"
            },
            {
                template: '<input id="inpFilter" class="k-textbox" placeholder="filter..." />'
            },
            {
                type: "button",
                id: "isToggledNextId",
                togglable: true,
                spriteCssClass: "k-icon k-i-sort-asc",
                toggle: function (e) {
                    var checked = e.checked;
                    SendPropertyChangeWithValue("isToggledNextId", 'Checked', 'ToggleButton', checked);
                }
            },
            {
                type: "button",
                id: "applyItems",
                attributes: { "class": "fa fa-check-circle" },
                click: function (e) {
                    SendStringCommandWithoutParameter("applyItems");
                }
            }

        ],
        click: function (e) {

        },
        toggle: function (e) {

        },
        overflowOpen: function (e) {

        },
        overflowClose: function (e) {
        },
        open: function (e) {
        },
        close: function (e) {
        }
    });

    $("#inpFilter").on("input", function () {
        var query = this.value.toLowerCase();
        var dataSource = $("#objectTreeView").data("kendoTreeView").dataSource;

        filter(dataSource, query);
    });
    
}

// sets "hidden" field on items matching query
function filter(dataSource, query) {
    var hasVisibleChildren = false;
    var data = dataSource instanceof kendo.data.HierarchicalDataSource && dataSource.data();

    for (var i = 0; i < data.length; i++) {
        var item = data[i];
        var text = item.NodeName.toLowerCase();
        var itemVisible =
            query === true // parent already matches
            || query === "" // query is empty
            || text.indexOf(query) >= 0; // item text matches query

        var anyVisibleChildren = filter(item.SubNodes, itemVisible || query); // pass true if parent matches

        hasVisibleChildren = hasVisibleChildren || anyVisibleChildren || itemVisible;

        item.hidden = !itemVisible && !anyVisibleChildren;
    }

    if (data) {
        // re-apply filter on children
        dataSource.filter({ field: "hidden", operator: "neq", value: true });
    }

    return hasVisibleChildren;
}

function createAddWindow(addUrl) {
    windowContainer = $('<div id="addWindow"><iframe class="k-content-frame" src="' + addUrl + '"></div>"')

    var kendoWindow = windowContainer.kendoWindow({
        width: 800,
        height: 800,
        title: "New Node",
        visible: false,
        actions: [
            "Refresh",
            "Minimize",
            "Maximize",
            "Close"
        ],
        close: function (e) {
            var dialog = $("#addWindow").data("kendoWindow");
            dialog.destroy();
        },
        refresh: function () {
            var iframe = windowContainer.find('iframe');
            iframe.attr('src', iframe.attr('src'));
        }
    }).data("kendoWindow").center().open();
    
}

function onMessage(evt) {

    console.log(evt.data);
    var objJSON = eval("(function(){return " + evt.data + ";} )()");

    for (var ix in objJSON) {
        var viewItem = objJSON[ix];

        if (viewItem.ViewItemId == 'Url_TreeData') {
            


        }
        else if (viewItem.ViewItemId == 'IsSuccessful_Login') {
            if (!viewItem.LastValue) {

                return;
            }

        }
        else if (viewItem.ViewItemId == 'objectTreeView') {
            var kendoTreeViewConfig = viewItem;
            jQuery.getJSON(viewItem.LastValue.dataSource.transport.read.url, function (treeData) {
                var data = treeData.data;
                if (data == undefined) {
                    data = treeData;
                }
                var dataSource = new kendo.data.HierarchicalDataSource({
                    data: data,
                    schema: kendoTreeViewConfig.LastValue.dataSource.schema
                });

                $('#' + kendoTreeViewConfig.ViewItemId).kendoTreeView({
                    dataSource: dataSource,
                    dataTextField: viewItem.LastValue.dataTextField,
                    checkboxes: { checkChildren: false },
                    check: function(e) {
                        var treeView = $("#objectTreeView").data("kendoTreeView");
                        var checkedNodes = treeView.getCheckedItems();

                        
                        SendPropertyChangeWithValue("objectTreeView", 'checkedItems', 'TreeNode', checkedNodes);
                    },
                    select: function (e) {
                        
                        var uId = $(e.node).attr('data-uid');
                        var data = $('#objectTreeView').data('kendoTreeView').dataItem(e.node);
                        var node = {
                            NodeId: data.NodeId,
                            NodeName: data.NodeName,
                            NodeUId: uId
                        }
                        var nodeId = data.NodeId;
                        var nodeName = data.NodeName;
                        SendPropertyChangeWithValue("objectTreeView", 'SelectedIndex', 'TreeNode', node)
                    }
                });
            });
        }
        else if (viewItem.ViewItemId == 'dropdRelationType' && viewItem.ViewItemType == 'SelectedIndex') {
            var dropdownlist = $("#dropdRelationType").data("kendoDropDownList");
            dropdownlist.value(viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'isToggledNextId' && viewItem.ViewItemType == 'Checked') {
            var toolbar = $("#treeToolbar").data("kendoToolBar");
            toolbar.toggle("#isToggledNextId", viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'addUrl') {
            createAddWindow(viewItem.LastValue);
        }
        else if (viewItem.ViewItemId == 'kendoTreeNodesNew') {
            var treeview = $("#objectTreeView").data("kendoTreeView");
            for (var i in viewItem.LastValue) {
                var treeNode = viewItem.LastValue[i];

                
                var parentNode = treeview.findByUid(treeNode.NodeUId);
                treeview.append(treeNode.SubNodes, parentNode);
            }
        }
        else if (viewItem.ViewItemId == 'isToggledCheck') {
            var treeview = $("#objectTreeView").data("kendoTreeView");
            treeview.checkboxes = { checkChildren: true };
        }
        else if (viewItem.ViewItemType == 'Content') {
            setViewItemContent(viewItem);
            if (viewItem.ViewItemClass == 'KendoDropDownList') {
                $('#' + viewItem.ViewItemId).change(function (e) {
                    var value = $("#dropdRelationType").val();
                    SendPropertyChangeWithValue(viewItem.ViewItemId, 'SelectedIndex', 'KendoDropDownList', value)
                });
            }
        }
        
        

    }



    //websocket.close();
}


function onOpen(evt) {
    ontoLog("CONNECTED");
    sendStandardParams();

    //myTree.load("tree.xml");
}


function onClose(evt) {
    openDisconnectedWindow();
    ontoLog("DISCONNECTED");
}

function onError(evt) {
    ontoLog('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function errorOccured(error, url, line) {

}

function errorOccured(error, url, line) {

}

window.addEventListener("load", init, false);
window.onerror = function (error, url, line) {
    errorOccured(error, url, line);
}